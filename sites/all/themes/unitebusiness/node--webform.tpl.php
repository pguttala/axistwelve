<?php

if (module_exists('taxonomy')) {
//	$taxonomy = taxonomy_link('taxonomy terms', $node);

	$out = array();
	if (!empty($content['body']['#object']->field_tags))
	if (is_array($content['body']['#object']->field_tags['und']))
	foreach ($content['body']['#object']->field_tags['und'] as $k => $d) {
		$t = taxonomy_term_load($d['tid']);
		$out[] = l($t->name, 'taxonomy/term/'.$t->tid);
	}
	$output = '';
	if (count($out) > 0) {
		$output = ' '.t("in").' '.implode(', ', $out);
	}
}

?>
<div class="blogPostSummary">
	<?php if (!$page): ?><h1><a href="<?php print $node_url ?>" title="<?php print $title ?>"><?php print $title ?></a></h1><?php endif; ?>
	<?php if ($content['body']['#object']->field_image['und'][0]) { ?>
	<div class="blogPostImage">
	<a href="<?php print file_create_url($content['body']['#object']->field_image['und'][0]['uri']); ?>" title="<?php print $content['body']['#object']->field_image['und'][0]['title'] ?>" class="img zoom" rel="blogpostimage"><?php print theme('image_style', array('style_name' => 'blog_teaser', 'path' => $content['body']['#object']->field_image['und'][0]['uri'], 'alt' => $content['body']['#object']->field_image['und'][0]['alt'], 'title' => $content['body']['#object']->field_image['und'][0]['title'], 'attributes' => array('class' => 'img'),'getsize' => false) );?></a>
	</div>
	<?php } ?>
	<div class="m5"><?php //print $picture ?><?php print str_replace('rel="tag"','',render($content)); ?></div>
	<?php //print $links; ?>
</div>
<?php //print '<pre>'. check_plain(print_r($name, 1)) .'</pre>'; ?>
<?php if ($bar_flag) {
           print '<div class="webform_pager">Page ' . $current_page. ' of '. $total_page.'</div>'; 
           print '<div class="form_completion"><div id="perHolder">
                  <div class="message"> <span> '.$bar.'% </span> Complete</div>
                  <div id="perHolderbar" class="bar" style="width: '.$bar.'px;"></div>
                  </div></div>';
    }
?>