<div id="a12_recommends" class="block">
<?php if ($block->subject): ?>
  <div class="block-title">
    <h2><?php print $block->subject ?></h2>
  </div>
<?php endif;?>
<div class="recommendations">
<?php print $content ?>
</div>
</div>
