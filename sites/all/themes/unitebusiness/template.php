<?php
// $Id$

//if (arg(2) != 'block' and arg(0) != 'admin') drupal_add_js(path_to_theme() . '/js/jquery-1.4.min.js', array('weight' => -15));

$style = theme_get_setting('skin');
switch ($style) {
	case 0:
		drupal_add_css(path_to_theme().'/css/skins/skin-1.css', array('group' => CSS_THEME));
		break;
	case 1:
		drupal_add_css(path_to_theme().'/css/skins/skin-2.css', array('group' => CSS_THEME));
		break;
	case 2:
		drupal_add_css(path_to_theme().'/css/skins/skin-3.css', array('group' => CSS_THEME));
		break;
	case 3:
		drupal_add_css(path_to_theme().'/css/skins/skin-4.css', array('group' => CSS_THEME));
		break;
	case 4:
		drupal_add_css(path_to_theme().'/css/skins/skin-5.css', array('group' => CSS_THEME));
		break;
	default:
		drupal_add_css(path_to_theme().'/css/skins/skin-1.css', array('group' => CSS_THEME));
}


function unitebusiness_add_conditional_styles() {
  $themes = $GLOBALS['base_theme_info'];
  $themes = $GLOBALS['theme_info'];
  
  $default_weight = 10;
  if(isset($themes->stylesheets) && isset($themes->stylesheets['responsive'])) {
    $responsive_stylesheets = $themes->stylesheets['responsive'];
    foreach($responsive_stylesheets as $media => $css) {
      if('responsive-ie' == $media) {
        foreach ( $css as $ie_media => $ie_css ) {
          drupal_add_css((string)array_pop($ie_css),
          array('group' => CSS_THEME, 'browsers' => array('IE' => ' IE ', '!IE' => FALSE), 'media' => $ie_media, 'weight' => $default_weight++));
        }
      }
      drupal_add_css((string)array_pop($css), array('group' => CSS_THEME, 'media' => $media, 'weight' => $default_weight++));
    }
  }
}


function unitebusiness_preprocess_html(&$variables) {
  
  //Add fonts from the google web font.
  drupal_add_css('http://fonts.googleapis.com/css?family=Questrial', array('type' => 'external')); //font-family: 'Questrial'
  drupal_add_css('http://fonts.googleapis.com/css?family=Lato:700', array('type' => 'external'));  //'font-family: Lato'
	drupal_add_css('http://fonts.googleapis.com/css?family=Montserrat', array('type' => 'external'));//font-family: 'Montserrat'
  
  //Add  stylesheet for handling hack for IE.
  drupal_add_css(path_to_theme() . '/css/ie-only-all-versions.css', array('group' => CSS_THEME, 'browsers' => array('IE' => 'lte IE 9', '!IE' => FALSE), 'weight' => 10, 'preprocess' => FALSE));
  unitebusiness_add_conditional_styles();
  
   $vp = array(
    '#type' => 'html_tag',
    '#tag' => 'meta',
    '#attributes' => array(
      'name' =>  'viewport',
      'content' =>  'width=device-width, initial-scale=1, maximum-scale=1, minimum-scale=1, user-scalable=no'
    )
  );
  drupal_add_html_head($vp, 'viewport');
}


/**
 * Implementation of theme_preprocess_views_view function
 */
function unitebusiness_preprocess_views_view(&$vars) {
  if (isset($vars['view']->name)) {
    $function = 'unitebusiness_preprocess_views_view__'.$vars['view']->name;
    if (function_exists($function)) {
      $function($vars);
    }
    $function = 'unitebusiness_preprocess_views_view_field__'.$vars['view']->name;
    if (function_exists($function)) {
      $function($vars);
    }
    $function = 'unitebusiness_preprocess_views_view_table__'.$vars['view']->name;
    if (function_exists($function)) {
      $function($vars);
    }
  }
}

/**
 * Implementation of THEME_preprocess_views_view_table__VIEW_NAME
 * create variable for my subscription status class.
 */

function unitebusiness_preprocess_views_view_table__my_subscriptions(&$vars) {
  $my_subscriptions_view = array();
  $my_subscriptions_view = $vars['view']->result;
  foreach($my_subscriptions_view as $num => $row){
    global $user;

		// get subscription status from a12_product_subscriptions_history table
		// show status as per the status value in database table
		$qry_get_history_status = db_query("SELECT status FROM {a12_product_subscriptions_history}
																				WHERE uid = :uid AND product_id = :product_id
																				ORDER BY a12_product_subscriptions_history_id DESC
																				LIMIT 1 ",
																			 array(	':uid' => $user->uid,
																			 				':product_id' => $row->commerce_product_field_data_commerce_product_product_id
																			 			)
																			 );

		if ($qry_get_history_status -> rowCount() > 0){
			$data_get_history_status = $qry_get_history_status -> fetchObject();
	
			switch($data_get_history_status->status) {
		
				/*********  status = OFFLINE ********/
				case 0:
				  $subscription_status[$num] = 'offline';
					$output = "<span>Offline </span>";
			
					$output .= "<span>(";
					$output .= l("Make Online",'my_subscriptions_order_status/ajax/'.$row->order_id.'/'.$row->commerce_line_item_field_data_commerce_line_items_line_item_.'/1' , array('attributes' => array('class' => array('use-ajax'))) );
					$output .= ")</span>";
					$output .= " <div> " . l('Cancel My Subscription' , 'my_subscriptions_order_delete_commerce_line_item_reference/'.$row->order_id.'/'.$row->commerce_line_item_field_data_commerce_line_items_line_item_, array('query' => array('op'=>'delete', 'destination' => 'user/'.$user->uid.'/my_subscriptions'))) . '</div>';
					break;
		
				/********* status = ONLINE *********/
				case 1:
				  $subscription_status[$num] = 'online';
					$output = "<span>Online </span>";
			
					$output .= "<span>(";
					$output .= l("Make Offline",'my_subscriptions_order_status/ajax/'.$row->order_id.'/'.$row->commerce_line_item_field_data_commerce_line_items_line_item_.'/0' , array('attributes' => array('class' => array('use-ajax'))) );
					$output .= ")</span";
					break;
		
				/********* status = CANCELLED ********/
				case 3:
				  $subscription_status[$num] = 'cancelled';
					$output = 'Cancelled';
					break;
		
				/********* status = UPGRADED ********/
				case 4:
				  $subscription_status[$num] = 'upgraded';
					$output = 'Upgraded';
					break;
		
				/********* status = BLOCKED ********/
				case 5:
				 $subscription_status[$num] = 'blocked';
					$output = 'Blocked';
					break;
		
				/********* status = EXPIRED ********/
				case 6:
				  $subscription_status[$num] = 'expired';
					$output = 'Expired';
					break;
			}

		}
   $vars['subscription_status'] = $output;
   $vars['subscription_status_class'][$num] = $subscription_status[$num];
  }
}
/**
 * Themes an empty shopping cart page (Custom overiding).
 */
function unitebusiness_commerce_cart_empty_page() {
  $breadcrumb = array();
  $breadcrumb[] = l('Home', '<front>');
  $breadcrumb[] = drupal_get_title();
  drupal_set_breadcrumb($breadcrumb);
  $output = '<div class="cart-empty-block">' . t('Your shopping cart is empty.') . '</div>';
  $output .= "<a href='product_store'><input type='button' name='goto_product_store' value='GoTo Product Store' /></a>";
  return $output;
}

/**
 * Themes an empty shopping cart page (Custom overiding).


function unitebusiness_webform_page(&$breadcrumb) {
  $breadcrumb = array();
     $breadcrumb[] = l('Home', '<front>');
     $breadcrumb[] = l(drupal_get_title(), base_path() . request_uri()); ;
     //print_r($breadcrumb);
     $vars['my_breadcrumb'] = drupal_set_breadcrumb($breadcrumb);
}
*/

/**
 * Implementation of theme_preprocess_page
 */

function unitebusiness_preprocess_page(&$vars) {
  // hide primary Tab - Check Access from 'Configure Key Access' page
  drupal_add_js(path_to_theme() . '/js/jquery.watermark.js');
  drupal_add_js(path_to_theme() . '/js/jquery.watermark.min.js');
  drupal_add_js(path_to_theme() . '/js/loginShowHide.js');
  drupal_add_js ('http://maps.googleapis.com/maps/api/js?sensor=false');


  drupal_add_js(path_to_theme() . '/js/slidingBoxs.js');

  if(drupal_is_front_page()) {
    unset($vars['page']['content']['system_main']['default_message']);
    /*drupal_add_css(path_to_theme() . '/css/page_front.css', array('group' => CSS_THEME, 'weight' => 115, 'preprocess' => TRUE));*/
  }

  if(isset($vars['tabs']['#primary']) && !empty($vars['tabs']['#primary'])) {
    foreach ($vars['tabs']['#primary'] as $id => $tab) {
      if( isset($tab['#link']['path']) && $tab['#link']['path'] == 'a12_indexes/%/access_key_settings/check_access') {
        $vars['tabs']['#primary'][$id] = '';
      }
    }
  }
 // About Us Breadcrumb
 if (request_path() == 'about-us') {
    $breadcrumb = array();
    $breadcrumb[] = l('Home', '<front>');
    $breadcrumb[] = l('About Us');     
    $breadcrumb[] = l(drupal_get_title(), current_path()); 
     //print_r($breadcrumb);
    drupal_set_breadcrumb($breadcrumb);
  } 
 // Blog Breadcrumb
  else if ($vars['node']->type == 'blog') {
    $breadcrumb = array();
    $breadcrumb[] = l('Home', '<front>');
    $breadcrumb[] = l('About Us', 'about-us');     
    $breadcrumb[] = l(drupal_get_title(), current_path()); 
     //print_r($breadcrumb);
    drupal_set_breadcrumb($breadcrumb);
  }
  else if (request_path() == 'blog') {
    $breadcrumb = array();
    $breadcrumb[] = l('Home', '<front>');
    $breadcrumb[] = l('About Us','about-us');
    $breadcrumb[] = l(drupal_get_title(), current_path()); 
     //print_r($breadcrumb);
    drupal_set_breadcrumb($breadcrumb);
  }
 // Our Resources 
  else if (request_path() == 'resources') {
    $breadcrumb = array();
    $breadcrumb[] = l('Home', '<front>');
    $breadcrumb[] = l('About Us', 'about-us');     
    $breadcrumb[] = l(drupal_get_title(), current_path()); 
     //print_r($breadcrumb);
    drupal_set_breadcrumb($breadcrumb);
  }
 // Our Team Breadcrumb  
  else if (request_path() == 'our-team') {
    $breadcrumb = array();
    $breadcrumb[] = l('Home', '<front>');
    $breadcrumb[] = l('About Us','about-us');     
    $breadcrumb[] = l(drupal_get_title(), current_path()); 
     //print_r($breadcrumb);
    drupal_set_breadcrumb($breadcrumb);
  }
// Services Breadcrumb    
  else if (request_path() == 'services') {
    $breadcrumb = array();
    $breadcrumb[] = l('Home', '<front>');
    $breadcrumb[] = l('Services');     
    $breadcrumb[] = l(drupal_get_title(), current_path()); 
     //print_r($breadcrumb);
    drupal_set_breadcrumb($breadcrumb);
  }
  else if ($vars['node']->type == 'services_landing') {
    $breadcrumb = array();
    $breadcrumb[] = l('Home', '<front>');
    $breadcrumb[] = l('Services', 'services');     
    $breadcrumb[] = l(drupal_get_title(), current_path()); 
     //print_r($breadcrumb);
    drupal_set_breadcrumb($breadcrumb);
  }
  else if (request_path() == 'our-gcloud-services') {
    $breadcrumb = array();
    $breadcrumb[] = l('Home', '<front>');
    $breadcrumb[] = l('Services','services');     
    $breadcrumb[] = l(drupal_get_title(), current_path()); 
     //print_r($breadcrumb);
    drupal_set_breadcrumb($breadcrumb);
  }  
 // Government Breadcrumb
  else if (request_path() == 'services/government-digital-frameworks') {
    $breadcrumb = array();
    $breadcrumb[] = l('Home', '<front>');
    $breadcrumb[] = l('Government','services/government-digital-frameworks');     
    $breadcrumb[] = l(drupal_get_title(), current_path()); 
     //print_r($breadcrumb);
    drupal_set_breadcrumb($breadcrumb);
  }
  else if (request_path() == 'services/government') {
    $breadcrumb = array();
    $breadcrumb[] = l('Home', '<front>');
    $breadcrumb[] = l('Government','services/government');     
    $breadcrumb[] = l(drupal_get_title(), current_path()); 
     //print_r($breadcrumb);
    drupal_set_breadcrumb($breadcrumb);
  }
 // Our Work Breadcrumb  
  else if (request_path() == 'portfolio') {
    $breadcrumb = array();
    $breadcrumb[] = l('Home', '<front>');
//  $breadcrumb[] = l('Our Work','portfolio');     
    $breadcrumb[] = l(drupal_get_title(), current_path()); 
     //print_r($breadcrumb);
    drupal_set_breadcrumb($breadcrumb);
  }
  else if ($vars['node']->type == 'sample_sites') {
    $breadcrumb = array();
    $breadcrumb[] = l('Home', '<front>');
    $breadcrumb[] = l('Our Work');     
    $breadcrumb[] = l(drupal_get_title(), current_path()); 
     //print_r($breadcrumb);
    drupal_set_breadcrumb($breadcrumb);
  }
// Contact Us Breadcrumb
  else if (request_path() == 'contact') {
    $breadcrumb = array();
    $breadcrumb[] = l('Home', '<front>');
    $breadcrumb[] = l(drupal_get_title(), current_path()); 
     //print_r($breadcrumb);
    drupal_set_breadcrumb($breadcrumb);
  }
// A12 Find Breadcrumb
  else if (request_path() == 'find') {
    $breadcrumb = array();
    $breadcrumb[] = l('Home', '<front>');
    $breadcrumb[] = l(drupal_get_title(), current_path()); 
     //print_r($breadcrumb);
    drupal_set_breadcrumb($breadcrumb);
  }    
  else if($vars['node']->type == 'webform' && $vars['node']->nid == 88){
    $breadcrumb = array();
    $breadcrumb[] = l('Home', '<front>');
    $breadcrumb[] = l('Services', 'services');
    $breadcrumb[] = l('Support', 'content/support-and-hosting-questionnaire');
    drupal_set_breadcrumb($breadcrumb);
  } 
  
// FND-199: Please remove Drupal status message from Step2 of free trial register
  $a12_free_dest = array();
  if($_GET['a12_free_dest']) {
    $a12_free_dest = parse_url(urldecode($_GET['a12_free_dest']));
  }
  if(arg(0) == 'user' && arg(1) == 'create_secret_access_key' && $a12_free_dest['path'] == '/user/create_secret_access_key' ) {
    // remove drupal message to not display registration confirmation message
    drupal_get_messages('status');
  }
}

/**
 * Implementation of theme_form_alter
 */
function unitebusiness_form_alter(&$form, &$form_state, $form_id){
	// change label of submit button 'Create new account' to 'Register' on user registration form
	if($form_id == 'user_register_form') {
		$form['actions']['submit']['#value'] = 'Register';
	}
}

 /**
 * Override or insert variables into the field template.
 */
function unitebusiness_preprocess_node(&$vars) {
     
   global $base_url;
   if($vars['type'] == 'training_course') {
       $selected_course_id = $base_url."/book-training?nid=".$vars['nid'];
       $vars['book_now'] = '<a href="'.$selected_course_id.'"><button class="btn"><span>Book now!</span></button></a>';
   }
	
if($vars['type'] == 'webform') {
  $breadcrumb = array();
  $breadcrumb[] = l('Home', '<front>');
  $breadcrumb[] = drupal_get_title();
  //print_r($breadcrumb);
  $vars['my_breadcrumb'] = drupal_set_breadcrumb($breadcrumb);
     
    $current_page          = $total_page  = $vars['elements']['webform']['#form']['details']['page_num']['#value'];
    $vars['current_page']  = $current_page;
    
    $current_page -=1;
    $total_page         = $vars['elements']['webform']['#form']['details']['page_count']['#value'];
    $vars['total_page'] = $total_page;
    
if($total_page > 1) {
      $bar              = ($current_page*100)/$total_page;
      $vars['bar']      = floor($bar);
      $vars['bar_flag'] = TRUE;
    } else {
      $vars['bar']       = 0;
      $vars['bar_flag']  = FALSE;
    }
  }
}

/**
 * Create the calendar date box.
 */
function unitebusiness_preprocess_calendar_datebox(&$vars) {
  $date = $vars['date'];
  $view = $vars['view'];
  $vars['day'] = intval(substr($date, 8, 2));
  $force_view_url = !empty($view->date_info->block) ? TRUE : FALSE;
  $month_path = calendar_granularity_path($view, 'month');
  $year_path = calendar_granularity_path($view, 'year');
  $day_path = calendar_granularity_path($view, 'day');
  $vars['url'] = str_replace(array($month_path, $year_path), 'training_course_events', date_pager_url($view, NULL, $date, $force_view_url));
  $vars['link'] = !empty($day_path) ? l($vars['day'], $vars['url']) : $vars['day'];
  $vars['granularity'] = $view->date_info->granularity;
  $vars['mini'] = !empty($view->date_info->mini);
  if ($vars['mini']) {
    if (!empty($vars['selected'])) {
      $vars['class'] = 'mini-day-on';

      foreach ($vars['items'][$date] as $time => $results_at_that_time)
        foreach ($results_at_that_time as $num => $result)
          $bt_text .= "<li class='tipsy_event_title'>" . $result->row->node_title . "</li>";

        $vars['link'] = l($vars['day'], $vars['url'], array('attributes' => array('class' => 'tipsy', 'id' => $bt_text)));

    }
    else {
      $vars['class'] = 'mini-day-off';
    }
  }
  else {
    $vars['class'] = 'day';
  }
}
