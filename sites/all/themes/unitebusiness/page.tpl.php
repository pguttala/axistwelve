<!-- Site Container -->
<!-- Login slide start -->

<?php if (!($user->uid > 0)) { ?>
<div id="ContentPanel">
  <!-- close button -->
  <a href="#" class="topReveal closeBtn"><?php print t("Close") ?></a>
  <div class="contentArea">
    <!-- New member registration -->
    <div class="right m7">
      <h4><?php print t("Not a member yet?") ?><span><?php print t("Register now and get started.") ?></span></h4>
        <button type="button" onclick="parent.location='<?php print url("user/register") ?>'"><?php print t("Register for an account") ?></button>
    </div>
    <!-- Alternate Login -->
    <div>
      <form class="loginForm" method="post" action="<?php print url("user") ?>" style="height:auto;">
        <div id="loginBg"><img src="<?php print base_path().drupal_get_path('theme', 'unitebusiness'); ?>/images/icon_login.png" width="90" height="90" alt="lock and key" />
          <div id="stack" class="noMargin">
            <a href="<?php print url("user/password") ?>"><?php print t("Forgot your password?") ?></a>
          </div>
        </div>
        <h2 class="clientFormTitle"><?php print t("Client login") ?></h2>
        <fieldset>
          <legend><?php print t("Account Login") ?></legend>
            <div class="left m1">
              <label for="RevealUsername" class="overlabel"><?php print t("Username") ?></label>
              <input type="text" id="RevealUsername" name="name"  class="loginInput textInput rounded"/>
            </div>
            <div class="left m2">
              <label for="RevealPassword" class="overlabel"><?php print t("Password") ?></label>
              <input type="password" id="RevealPassword" name="pass" class="loginInput textInput rounded">
            </div>
            <div class="left m3">
              <input type="submit" name="op" id="edit-submit1" value="<?php print t("Login >") ?>"  class="form-submit" />
            </div>
        </fieldset>
          <div class="left noMargin">
            <a href="<?php print url("user/password") ?>"><?php print t("Forgot your password?") ?></a>
          </div>
          <fieldset>
            <input type="hidden" name="form_id" id="edit-user-login" value="user_login" /></fieldset>
       </form>
     </div>
     <!-- End of Content -->
     <div class="clear"></div>
  </div>
</div>
<?php } ?>
<!-- BRANDING START -->
<div id="branding-wrapper">
  <div id="Branding">
    <!-- Logo -->
    <div id="Logo">
    <?php if ($logo): ?>
      <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home" id="logo">
        <img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" />
      </a>
    <?php endif; ?>
    </div>
    <!-- LOIGN LOGOUT links-->
    <div class="mmLoginText">Websites that mean business.</div>
    <ul id="MmOtherLinks" >
      <?php if ($user->uid > 0) { ?>
      <li><a href="<?php print url('user/logout') ?>"><span class="mmLogin"><?php print t("Logout >") ?></span></a></li>
      <?php } else { ?>
      <li><a href="#ContentPanel" class="topReveal"><div id = "LoginButton"class="mmLogin"><?php print t("Login >") ?></div></a></li> <?php } ?>
    </ul>
    
    <!-- End of Content -->
    <div class="clear"></div>
  </div>
</div>
<!-- BRANDING END -->

<div id="header-wrapper">
  <!-- Main Menu -->
  <div id="MenuWrapper">
  <div id="MenuWrapper_full_width">
   <div id="MainMenu_box">
    <div id="MainMenu">
      <!-- Main Menu Links -->
      <?php print str_replace(array('class="menu"'), array('class="sf-menu"'), drupal_render(menu_tree('main-menu'))) ?>
      <!-- Extra Menu Links -->
    </div>
  </div>
  </div>
    <?php $block = module_invoke('block_plus', 'block_view', 'primary_menu_stack');?>
    <div id="MainMenuStack">
      <?php print render($block['content']); ?>
    </div>
    <div class="header_slider" >
        <?php if ($page['slide_shows']): ?><?php print render($page['slide_shows']); ?><?php endif;?>
    </div>
  </div>
  <div class="clear"></div>
</div>

<!--  HEADER WRAPPER ENDS  -->

<!-- CONTENT WRAPPER STARTS -->
<div id="content-wrapper">
<div id="content-wrapper-shadow">

  <!-- CONTENT START -->
  <div id="Content" class="extraTop">
          <div class="breadcrumbs"><?php print $breadcrumb ?></div>
					<?php if ($title): ?>
            <div class="page-title">
              <h1 class="headline"><?php echo $title ?></h1>
            </div>
          <?php endif;?>
          
          <?php if (!empty($messages) && (!($is_front))): print $messages; endif; ?>
          <?php if(in_array('a12_admin', $user->roles) || in_array('administrator', $user->roles)): ?>
            <?php if ($tabs): print ''. render($tabs) .''; endif; ?>
          <?php endif;?>
          <?php if (!empty($help)): print $help; endif; ?>
    <div class="content-one-third-left">
    <?php if ($page['sidebar_left']): ?><?php print render($page['sidebar_left']); ?><?php endif;?>
    </div>
    
    <div class="content-two-thirds">
      <?php if ($page['content']): ?><?php print render($page['content']); ?><?php endif;?>
    </div>

    <div class="content-one-third-right">
      <?php if ($page['sidebar_right']): ?><?php print render($page['sidebar_right']);?><?php endif;?>
    </div>

  </div>
  <div class="clear"></div>
</div>
</div>
<!-- CONTENT WRAPEER ENDS -->
<!--  FOOTER TOP STARTS -->
<div id="footer-top-wrapper">
  <!-- CONTENT START -->
  <div id="Footer_Top">
    <?php print render($page['footer_top']); ?>
  </div>
  <div class="clear"></div>
</div>

<!-- FOOTER TOP ENDS -->

<!-- FOOTER WRAPPER STARTS -->
<div id="footer-wrapper">
  <div id="footer-zigzag">&nbsp;</div>
  <!-- CONTENT START -->
  <div id="Footer">
    <!-- Column 1 -->
    <div class="footer-one-third first">
      <?php if ($page['footer_1']): ?><?php print render($page['footer_1']); ?><?php endif;?>
    </div>
      <!-- Column 2 -->
      <div class="footer-one-third second">
         <?php if ($page['footer_2']): ?><?php print render($page['footer_2']); ?><?php endif;?>
      </div>
      <!-- Column 3 -->
      <div class="footer-one-third third">
         <?php if ($page['footer_3']): ?><?php print render($page['footer_3']); ?><?php endif;?>
      </div>
      <!-- End of Content -->
      <div class="clear"></div>
  </div>
</div>

<!-- FOOTER WRAPPER ENDS -->
<?php if($page['footer_bottom']):?>
  <div id="footer-bottom-wrapper">
    <?php print render($page['footer_bottom']); ?>
  </div>
<?php endif; ?>
