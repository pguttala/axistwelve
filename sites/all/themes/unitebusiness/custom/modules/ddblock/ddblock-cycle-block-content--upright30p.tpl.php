<?php
// $Id$

/*!
 * Dynamic display block module template (Drupal 7): upright40p - content template
 * (c) Copyright Phelsa Information Technology, 2011. All rights reserved.
 * Version 1.3 ( 30-JUL-2011 )
 * Licenced under GPL license
 * http://www.gnu.org/licenses/gpl.html
 */

/**
 * @file
 * Dynamic display block module template (Drupal 7): upright40p - content template
 *
 * Available variables:
 * - $delta: Block number of the block.
 *
 * - $template: template name
 * - $output_type: type of content
 *
 * - $slider_items: array with slidecontent
 * - $slide_text_position of the text in the slider (top | right | bottom | left)
 * - $slide_direction: direction of the text in the slider (horizontal | vertical )
 * -
 * - $pager_content: Themed pager content
 * - $pager_position: position of the pager (top | bottom)
 *
 * notes: don't change the ID names, they are used by the jQuery script.
 */

$settings = $ddblock_cycle_slider_settings;
 
// add Cascading style sheet
drupal_add_css($directory . '/custom/modules/ddblock/' . $settings['template'] . '/ddblock-cycle-' . $settings['template'] . '.css', array('group' => CSS_THEME, 'every_page' => FALSE));
global $base_url;
?>
<!-- dynamic display block slideshow -->
<div id="ddblock-<?php print $settings['delta'] ?>" class="ddblock-cycle-<?php print $settings['template'] ?> clearfix">
 <div class="container clearfix">
 
  <div class="container-inner clearfix">
  
   <?php if ($settings['pager_toggle'] && $settings['pager_position'] == "top") : ?>
    <?php print $pager_content ?>
   <?php endif; ?>
   <!-- slider content -->
   <div class="background-blue clearfix">
   <div class="slider clearfix">
    <div class="slider-inner clearfix">
     <?php if ($settings['output_type'] == 'view_fields') : ?>
     <?php  ksort($content);
      $next = end(array_keys($content));  $prev = $next - 1 ; $my_counter = 1;
     ?>
      <?php foreach ($content as $key => $slider_item):  ?>
       <div class="slide clearfix">
        <div class="slide-inner clearfix">
         <?php if (isset($slider_item['slide_image'])) :?>
         <div class="main_slide_text">
         <div class="slider_text_content">
         <?php if ($settings['slide_text'] == 1) :?>
             <div class="current_slider_title"><?php print sprintf("%02d", $my_counter++);  //print $my_counter++;?><a href="<?php print $slider_item['slide_read_more'] ;?>"><?php print_r(strtoupper($slider_item['slide_title'])); ?></a></div>
             <?php /*?><div class="current_slider_body"><?php print $slider_item['slide_text']; ?></div><?php */?>
             <?php /*?><a href="<?php print $slider_item['slide_read_more'] ;?>"><div class="current_slider_read_more">READ MORE</div></a><?php */?>
             <div class="current_slider_img"><?php print $slider_item['slide_image']; ?></div>
         <?php else: ?>
            <?php print $slider_item['slide_text']; ?>
         <?php endif; ?></div></div>
         
         <?php endif; ?>
         <div class="next_prev">
             <div class="text_prev">
               <a href="<?php print $content[$prev]['slide_read_more'] ;?>"><div class="navigation_block_image"><?php print_r($content[$prev]['slide_image']); ?></div></a>
               <a href="<?php print $content[$prev]['slide_read_more'] ;?>"><div class="navigation_block_title"> <?php print_r($content[$prev]['slide_title']); ?></div></a>
               <div class="navigation_block_body"><?php print_r($content[$prev]['secondary_desc']); ?></div>
             </div>
             <div class="text_next">
               <a href="<?php print $content[$next]['slide_read_more'] ;?>"><div class="navigation_block_image"><?php print_r($content[$next]['slide_image']); ?></div></a>
               <a href="<?php print $content[$next]['slide_read_more'] ;?>"><div class="navigation_block_title"> <?php print_r($content[$next]['slide_title']); ?></div></a>
               <div class="navigation_block_body"><?php print_r($content[$next]['secondary_desc']); ?></div>
             </div>
         </div>
         <?php $prev = $next; ?>
         <?php $next = $key; ?>
        </div> <!-- slide-inner-->
       </div>  <!-- slide-->
      <?php endforeach; ?>
     <?php endif; ?>
    </div> <!-- slider-inner-->
    </div>  <!-- background-blue-->
    
   </div>  <!-- slider-->
   
   <!-- prev/next pager on slide -->
   <?php if ($settings['pager2'] == 1): ?>
    <div class="pager-slide prev-container prev-container-<?php print $settings['pager_position'] ?>">
     <a class="prev" href="#"><?php print $settings['pager2_slide_prev']?></a>
    </div>
    <div class="pager-slide next-container next-container-<?php print $settings['pager_position'] ?>">
     <a class="next" href="#"><?php print $settings['pager2_slide_next'] ?></a>
    </div>
    
    <!--<div class="round_block_box">
      <div class="round-block round-block-center" >
        <div class="our_products_slider_title">OUR PRODUCTS</div>
        <div class="our_products_slider_body">Visit our product store to see the great range of Drupal products available</div>
        <a class="" href="./product_store"><span class="play"><img src="sites/all/themes/unitebusiness/images/play.png"></span></a>
      </div>
     </div>-->
    <div class="clear"></div>
   <?php endif; ?>
  </div> <!-- container-inner-->
  
 </div> <!--container-->
</div> <!--  template -->