<?php if ($content['comments'] or $content['comment_form']) { ?>
<div id="comments">
  <?php print render($content['comments']); ?>
	<div class="box">
	  <h2><?php print t('Post new comment'); ?></h2>
	  <?php print render($content['comment_form']); ?>
	</div>
</div>
<?php } ?>
