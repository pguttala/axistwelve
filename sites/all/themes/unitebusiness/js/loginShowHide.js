(function ($) {
  Drupal.behaviors.colorBlockHeaderByTaxonomy = {		  
    attach: function (context) {
			$('#RevealUsername').watermark('Username');
			$('#RevealPassword').watermark('Password');
			$('#edit-picture-upload').attr("size", 27);
	    $('input[type=submit]').not('.no-replace-btn').each(function(index) {
	    	var id = $(this).attr("id");var name = $(this).attr("name"); var value = $(this).val();	    	
	    	$(this).replaceWith('<button value="'+value+'" name="'+name+'" type="submit" class="form-submit btn" id="'+id+'"><span style="">'+value+'</span></button>');
	    });
  		$('.closeBtn').click(function(){
			$('#ContentPanel').slideToggle("slow");				  
		  });
		$('#LoginButton').click(function(){
			$('#ContentPanel').slideToggle("slow");				  
		  });
    }
  }
})(jQuery);
  
