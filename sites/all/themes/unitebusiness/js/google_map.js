/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


/*
 * UK Google Map
 */

function initialize() {
  var uk_Latlng = new google.maps.LatLng(51.541531,-0.095294);  
  var uk_mapOptions = {
    zoom: 16,
    center: uk_Latlng,
  //  center: new google.maps.LatLng(51.541531,-0.095294),
    disableDefaultUI: true,
    mapTypeId: google.maps.MapTypeId.ROADMAP
  }
  var uk_map = new google.maps.Map(document.getElementById('map-canvas-uk'),
                                uk_mapOptions);

  var contentString = '<div id="content">'+
      '<div id="siteNotice">'+
      '</div>'+
      '<h2 id="firstHeading" class="firstHeading">Axis 12 Ltd.</h2>'+
      '<div id="bodyContent">'+
      '<p>Unit 14, 6-18 Northampton St, London, N1 2HY</p>'+
      '</div>'+
      '</div>';

  var uk_infowindow = new google.maps.InfoWindow({
      content: contentString,
      maxWidth: 140
  });
  
  var uk_marker = new google.maps.Marker({
      position: uk_Latlng,
      map: uk_map,
      title: 'Axis 12 Limited'
  });

  google.maps.event.addListener(uk_marker, 'click', function() {
    uk_infowindow.open(uk_map,uk_marker);
  });
  

  
/*
 * Austrailian Google Map
 */  
  var aus_Latlng= new google.maps.LatLng(-33.881261,151.214494);  
  var aus_mapOptions = {
    zoom: 16,
    center: aus_Latlng,
   // center: new google.maps.LatLng(-33.841771,151.202967),
    disableDefaultUI: true,
    mapTypeId: google.maps.MapTypeId.ROADMAP
  }
  var aus_map = new google.maps.Map(document.getElementById('map-canvas-aus'),
                                aus_mapOptions);

  var contentString = '<div id="content">'+
      '<div id="siteNotice">'+
      '</div>'+
      '<h2 id="firstHeading" class="firstHeading">Axis 12 Pty Ltd.</h2>'+
      '<div id="bodyContent">'+
      '<p>285A Crown Street, Sydney, New South Wales 2010, Austrailia</p>'+
      '</div>'+
      '</div>';

  var aus_infowindow = new google.maps.InfoWindow({
      content: contentString,
      maxWidth: 140
  });
  
  var aus_marker = new google.maps.Marker({
      position: aus_Latlng,
      map: aus_map,
      title: 'Axis 12 PTY Ltd'
  });
    
   google.maps.event.addListener(aus_marker, 'click', function() {
    aus_infowindow.open(aus_map,aus_marker);
  });
  
/*
 * Indian Google Map
 */  
  var idn_Latlng= new google.maps.LatLng(18.563598,73.91711);  
  var idn_mapOptions = {
    zoom: 12,
    center: idn_Latlng,
   // center: new google.maps.LatLng(18.563598,73.91711),
    disableDefaultUI: true,
    mapTypeId: google.maps.MapTypeId.ROADMAP
  }
  var idn_map = new google.maps.Map(document.getElementById('map-canvas-idn'),
                                idn_mapOptions);

  var contentString = '<div id="content">'+
      '<div id="siteNotice">'+
      '</div>'+
/*    '<h2 id="firstHeading" class="firstHeading">Axis 12 Pty Ltd.</h2>'+ */
      '<div id="bodyContent">'+
      '<p>OZ Paza, Viman Nagar, Maharastra,India</p>'+
      '</div>'+
      '</div>';

  var idn_infowindow = new google.maps.InfoWindow({
      content: contentString,
      maxWidth: 140
  });
  
  var idn_marker = new google.maps.Marker({
      position: idn_Latlng,
      map: idn_map,
      title: 'Axis 12 PTY Ltd'
  });
    
   google.maps.event.addListener(idn_marker, 'click', function() {
    idn_infowindow.open(idn_map,idn_marker);
  });
}
google.maps.event.addDomListener(window, 'load', initialize);
