<?php

/**
 * Admin forms for a12_find_server_management module
 */
define('DEV', 1);
define('TEST', 2);
define('LIVE', 3);


/**
 * Background to the functions
 *
 * Implemented:
 * RELOAD: Load a new core from the same configuration as an existing registered core.
 * 		   While the "new" core is initalizing, the "old" one will continue to accept requests.
 *         Once it has finished, all new request will go to the "new" core, and the "old" core will be unloaded.
 * 	       This can be useful when (backwards compatible) changes have been made to your solrconfig.xml or schema.xml
 *         files (ie: new <field> declarations, changed default params for a <requestHandler>, etc...) and you want to start
 *         using them without stopping and restarting your whole Servlet Container.
 *
 * DIH FULL Import:  http://<host>:<port>/solr/dataimport?command=full-import
 *  It takes in extra parameters:
 *    entity : Name of an entity directly under the <document> tag. Use this to execute one or more entities 
 *              selectively. Multiple 'entity' parameters can be passed on to run multiple entities at once. 
 *              If nothing is passed, all entities are executed.
 *    clean : (default 'true'). Tells whether to clean up the index before the indexing is started.
 *    commit : (default 'true'). Tells whether to commit after the operation.
 *    optimize : (default 'true'). Tells whether to optimize after the operation.
 *    debug : (default 'false'). Runs in debug mode. It is used by the interactive development mode (see here). 
 *            Please note that in debug mode, documents are never committed automatically. 
 *            If you want to run debug mode and commit the results too, add 'commit=true' as a request parameter.
 *    delta-import : For incremental imports and change detection run the command http://<host>:<port>/solr/dataimport?command=delta-import . 
 *            It supports the same clean, commit, optimize and debug parameters as full-import command.
 *    status : To know the status of the current command, hit the URL http://<host>:<port>/solr/dataimport . 
 *             It gives an elaborate statistics on no. of docs created, deleted, queries run, rows fetched, status etc.
 *    reload-config : If the data-config is changed and you wish to reload the file without restarting Solr. 
 *                    Run the command http://<host>:<port>/solr/dataimport?command=reload-config .
 *    abort : Abort an ongoing operation by hitting the URL http://<host>:<port>/solr/dataimport?command=abort .
 *
 * To be Implemented:
 * STATUS: Get the status for a given core or all cores if no core is specified:
 * 			e.g. http://<host>:<port>/solr/admin/cores?action=STATUS&core=core0
 * 			http://<host>:<port>/solr/admin/cores?action=STATUS
 *
 * CREATE: Creates a new core based on preexisting instanceDir/solrconfig.xml/schema.xml, and registers it.
 *          If persistence is enabled (persist=true), the configuration for this new core will be saved in 'solr.xml'.
 *          If a core with the same name exists, while the "new" created core is initalizing, the "old" one will continue to accept requests.
 *          Once it has finished, all new request will go to the "new" core, and the "old" core will be unloaded.
 *          e.g. http://<host>:<port>/solr/admin/cores?action=CREATE&name=coreX&instanceDir=path_to_instance_directory&config=config_file_name.xml&schema=schem_file_name.xml&dataDir=data
 *          instanceDir is a required parameter. config, schema & dataDir parameters are optional.
 *          (Default is to look for solrconfig.xml/schema.xml inside instanceDir. Default place to look for dataDir depends on solrconfig.xml.)
 *
 * RENAME: Change the names used to access a core. The example below changes the name of the core from "core0" to "core5".
 * 			e.g. http://<host>:<port>/solr/admin/cores?action=RENAME&core=core0&other=core5
 *
 * ALIAS: (Experimental) Adds an additional name for a core. The example below allows access to the same core via the names "core0"
 * 			and "corefoo".
 * 			e.g. http://<host>:<port>/solr/admin/cores?action=ALIAS&core=core0&other=corefoo
 *
 * SWAP: Atomically swaps the names used to access two existing cores.
 * 		This can be useful for replacing a "live" core with an "ondeck" core, and keeping the old "live" core running in case you
 * 		decide to roll-back.
 * 		e.g. http://<host>:<port>/solr/admin/cores?action=SWAP&core=core1&other=core0
 *
 * UNLOAD: Removes a core from solr. Existing requests will continue to be processed, but no new requests can be sent to this core by the name.
 * 			If a core is registered under more than one name, only that specific mapping is removed.
 * 			e.g. http://<host>:<port>/solr/admin/cores?action=UNLOAD&core=core0
 */
function a12_find_server_management_settings() {
  $headers = array(
    t('Build'),
    t('Name'),
    t('Host'),
    t('Port'),
    t('Path'),
    array('data' => t('Operations'), 'colspan' => 3),
  );
  $servers = a12_find_server_management_get_server();
  $build_ops = array(0 => t('None'), 1 => t('DEV'), 2 => t('TEST'), 3 => t('LIVE'));
  $_options = variable_get('a12_find_server_management_build_ops');
  $indices = a12_find_server_management_format_indicies();
  if($servers) {
    foreach($servers as $server) {
      $op_s = array();
     
      isset($_options[$server->sid])?$op_s[$_options[$server->sid]] = ' SELECTED="SELECTED" ':$op_s[$_options[$server->sid]]=''; 
      // @TODO: Stupid in table form rendering you figure it out!!
      $bo = sprintf('<select name="build_ops[%d]"/><option value="0" %s>None</option><option value="1" %s>DEV</option><option value="2" %s>TEST</option><option value="3" %s>LIVE</option>', $server->sid, $op_s[0], $op_s[1], $op_s[2], $op_s[3]);
      $rows[] = array('data' =>
          array(
            // Cells
//            ,
//            drupal_render($build_form),
            $bo,
            $server->apachesolr_title,
            $server->apachesolr_host,
            $server->apachesolr_port,
            $server->apachesolr_path,
            array('data' => l(t('Edit'), sprintf('admin/config/a12/find_server_management/%s/edit', $server->sid), array('query' => array('destination' => 'admin/config/a12/find_server_management')))),
            array('data' => l(t('Delete'), sprintf('admin/config/a12/find_server_management/%s/delete', $server->sid)), 'colspan' => 2),
          ),
        );
      if($cores = a12_find_server_management_get_cores($server->apachesolr_scheme, $server->apachesolr_host, $server->apachesolr_port, $server->apachesolr_path)) {
        foreach($cores as $core){
          $details = print_r($core, 1);
          $client = '!Not bound!';
          if($indices[$server->sid]) {
            foreach($indices[$server->sid] as $path => $index)
            if(stristr(sprintf('%s/%s/', $server->apachesolr_path, $core->name), $path)) {
              $client = sprintf('Client index: %s', $index->name);
            }
          }
          $tablerow = array(
            array('data' => '', colspan => 1),
            array('data' => $client, colspan => 1),
            array('data' => '', colspan => 2),
            array('data' => t(sprintf('%s/%s (online)', $server->apachesolr_path, $core->name))),
            array('data' => l(t('Reload Core'), sprintf('admin/config/a12/find_server_management/core/reload/%s/%s', $core->name, $server->sid))),
            array('data' => l(t('Full Import'), sprintf('admin/config/a12/find_server_management/reindex/full/%s/%s', $core->name, $server->sid))),
            // @TODO add Details option
            array('data' => l(t('Details Not yet implemented'), 'admin/config/a12/find_server_management', array('attributes' => array('title' => $details)))),
          );
          $rows[] = $tablerow;
        }
      }
    }
      $form['a12_find_server_management']['actions'] = array(
      '#markup' => '<ul class="action-links">' . l(t('Add new search environment'), 'admin/config/a12/find_server_management/add', array('query' => array('destination' => $_GET['q']))) . '</ul>',
    );
  }
  $form['a12_find_server_management']['table'] = array(
    '#theme' => 'table',
    '#header' => $headers,
    '#rows' => $rows
  );
  return system_settings_form($form);
}

function a12_find_server_management_settings_validate($form, $form_state) {
  variable_set('a12_find_server_management_build_ops', $form_state['input']['build_ops']);
}

/**
 * This method will create all the set cores for a index  
 * @staticvar type $server_sids
 * @param type $current_cores
 * @param string $synonym
 * @param string $language
 * @return type
 * 
 * @todo On update this function should allow the creation of addition cores
 */
function a12_find_server_management_create_cores($current_cores=NULL, $synonym='default', $language = 'default') {
  static $server_sids;
  $core_data = array();
  $build_servers = variable_get('a12_find_server_management_build_ops');
  
  foreach($build_servers as $sid => $type) {
    switch($type) {
      case DEV:
        if(!isset($server_sids[DEV]) && !isset($current_cores->DEV)) {
          $server_sids[DEV] = $sid;
          $core_data['DEV'] = a12_find_server_management_create_core($server_sids[DEV], 'DEV', $synonym, $language);
        }
        elseif (isset($current_cores->DEV)) {
          $core_data['DEV'] = (array)$current_cores->DEV;
        }
        break;
      case TEST:
        if(!isset($server_sids[TEST])  && !isset($current_cores->TEST)) {
          $server_sids[TEST] = $sid;
          $core_data['TEST'] = a12_find_server_management_create_core($server_sids[TEST], 'TEST', $synonym, $language);
        }
        elseif (isset($current_cores->TEST)) {
          $core_data['TEST'] = (array)$current_cores->TEST;
        }        
        break;
      case LIVE:
        if(!isset($server_sids[LIVE])  && !isset($current_cores->LIVE)) {
          $server_sids[LIVE] = $sid;
          $core_data['LIVE'] = a12_find_server_management_create_core($server_sids[LIVE], 'LIVE', $synonym, $language);
        }
        elseif (isset($current_cores->LIVE)) {
          $core_data['LIVE'] = (array)$current_cores->LIVE;
        }         
        break;
    }
  }
  
  return $core_data;
}
############## Core Manipulation Functions

/**
 * Allows you to create a new core on a Solr instance
 * 
 * @param type $sid
 * @param type $type
 * @param type $synonym
 * @param type $language
 * @return type
 */
function a12_find_server_management_create_core($sid, $type='DEV', $synonym='default', $language = 'default') {
   $server = a12_find_server_management_get_server($sid);
   $scheme = $server[0]->apachesolr_scheme;
   $host = $server[0]->apachesolr_host;
   $port = $server[0]->apachesolr_port;
   $path = $server[0]->apachesolr_path;
   
   $core = a12_find_server_management_get_cores($scheme, $host, $port, $path);
   $core_name = sprintf('%s%s', $type, $server[0]->apachesolr_is_multicore);
   $core_instanceDir = sprintf('%s/', $core_name);
   $server[0]->apachesolr_is_multicore++;
   $serv['values'] = (array)$server[0];
   
   if ($language != 'default') {
     $synonym = sprintf('%s_%s', $synonym, $language);
   }
   
   $config_path = '/data/solr_conf/' . $synonym . '/';
   $solrconfig = $config_path . 'solrconfig.xml';
   $schema = $config_path . 'schema.xml';
   a12_find_server_management_server_form_submit(array(), $serv);
   // Create return array
   $rtn = array();
   $rtn['path']  =  $serv['values']['apachesolr_path'] . '/' . $core_instanceDir;
   $rtn['scheme']  = str_replace('://', '', $serv['values']['apachesolr_scheme']);
   $rtn['host']  =  $serv['values']['apachesolr_host'];
   $rtn['port']  =  $serv['values']['apachesolr_port'];
   $rtn['sid']  =  $serv['values']['sid'];
//   @todo instanceDir is hardcoded to core0 at the moment, we should look at setting up a gold server
//   
   $create_core_url = sprintf('http://%s:%s%s/admin/cores?action=CREATE&name=%s&instanceDir=%s&persist=true&wt=json&config=%s&schema=%s', $host, $port, $path, $core_name, $core_instanceDir, $solrconfig, $schema);

//   @todo This might be usefull if we are moving data &dataDir=data
   $response = file_get_contents($create_core_url);
   watchdog('Find Create Core - Notice', print_r($response, 1));
   return $rtn;
}


/**
 *
 * Allow you to reload the configuration in an existing core
 * @param $core Name of the core
 */
function a12_find_server_management_reload_core($core, $sid=NULL) {
  if (is_null($sid)) {
   $host = variable_get('apachesolr_host', 'localhost');
   $port = variable_get('apachesolr_port', '8983');
   $path = variable_get('apachesolr_path', '/solr');
  }
  else{
   $server = a12_find_server_management_get_server($sid);
   $host = $server[0]->apachesolr_host;
   $port = $server[0]->apachesolr_port;
   $path = $server[0]->apachesolr_path;
  }
   $reload_url = sprintf('http://%s:%s%s/admin/cores?action=RELOAD&core=%s', $host, $port, $path, $core);
   $response = file_get_contents($reload_url);
}

/**
 * Allows you to permentantly  remove a core
 * @param $core Name of the core
 * 
 */
function a12_find_server_management_delete_core($core, $sid=NULL) {
  if (is_null($sid)) {
   $host = variable_get('apachesolr_host', 'localhost');
   $port = variable_get('apachesolr_port', '8983');
   $path = variable_get('apachesolr_path', '/solr');
  }
  else{
   $server = a12_find_server_management_get_server($sid);
   $host = $server[0]->apachesolr_host;
   $port = $server[0]->apachesolr_port;
   $path = $server[0]->apachesolr_path;
  }
  $reload_url = sprintf('http://%s:%s%s/admin/cores?action=UNLOAD&core=%s', $host, $port, $path, $core);
  $response = file_get_contents($reload_url);
}

/**
 * Get server object(s)
 *
 * @param $sid: Server id, if serverid not provided returns list of all servers registerd in the system
 * @return $find_server/s
 */
function a12_find_server_management_get_server($sid=NULL) {
  $rows = NULL;
  $result = db_select('a12_find_apachesolr_servers', 'a')
       ->fields('a', array('sid', 'apachesolr_scheme', 'apachesolr_title', 'apachesolr_host', 'apachesolr_port', 'apachesolr_path', 'apachesolr_is_multicore'));
       
  if (!is_null($sid)) {
    $result->condition('sid', $sid);
  }

  $results = $result->execute();
  if($results) {
    $rows = $results->fetchAll();
  }
  return $rows;
}

/**
 *
 * Grab the current avaible apache solr cores
 * @TODO Extended to get cores from different registered servers
 * @parma $scheme
 * @param $host
 * @param $port
 * @param $path
 */
function  a12_find_server_management_get_cores($scheme, $host, $port, $path) {
  static $cores = array();
  static $mc = array();
  $url = sprintf('%s%s:%s%s/%s%s', $scheme, $host, $port, $path, 'admin/cores', '?wt=json');
  if(!$cores[$url]) {
    if($result = file_get_contents($url)) {
      $data = json_decode($result);
      $cores[$url] = $data->status;
    }
  }
  return $cores[$url];
}


/**
 * Form for creating or editing solr servers
 * @param type $form_state
 * @param type $form_type
 * @param type $action
 * @param type $sid
 * @return int 
 * @todo We need to look at labelling cores with their configuration values (meaning what synonym file etc is loaded)
 *       Alternatively we could look at having VM dedicated to synonyns (but thats a little dumb)
 */
function a12_find_server_management_server_form($form_state, $form_type, $action, $sid=NULL) {
  switch($action) {
    case 'edit':
      if($sid) {
        $server = a12_find_server_management_get_server($sid);
        if(isset($server[0])) $solr_server = $server[0];
      }
      
//        print_r($form_type);
//        print_r($sid);
        $form['sid'] = array(
          '#type' => 'hidden',
          '#value' => $solr_server->sid,
        );
    case 'add':
        $form['apachesolr_title'] = array(
          '#title' => t('Server Name'),
          '#type' => 'textfield',
          '#description' => t('Identifier for a solr server.'),
          '#default_value' => isset($solr_server->apachesolr_title) ? $solr_server->apachesolr_title : '',
          '#weight' => -10,
          '#required' => TRUE,
        );
       $form['apachesolr_scheme'] = array(
          '#type' => 'select',
          '#title' => t('Protocol Scheme for Solr'),
          '#options' => array('http://' => 'http://', 'https://' => 'https://'),
          '#default_value' => isset($solr_server->apachesolr_scheme) ? $solr_server->apachesolr_scheme : 'http://',
          '#description' => t('Protocol scheme which the Solr server listens on.'),
          '#required' => TRUE,
        );
       $form['apachesolr_host'] = array(
          '#type' => 'textfield',
          '#title' => t('Solr host name'),
          '#default_value' => isset($solr_server->apachesolr_host) ? $solr_server->apachesolr_host : '',
          '#description' => t('Host name of your Solr server, e.g. <code>localhost</code> or <code>example.com</code>.'),
          '#required' => TRUE,
        );
        $form['apachesolr_port'] = array(
          '#type' => 'textfield',
          '#title' => t('Solr port'),
          '#default_value' => isset($solr_server->apachesolr_port) ? $solr_server->apachesolr_port : '',
          '#description' => t('Port on which the Solr server listens. The Jetty example server is 8983, while Tomcat is 8080 by default.'),
          '#required' => TRUE,
        );
        $form['apachesolr_path'] = array(
          '#type' => 'textfield',
          '#title' => t('Solr path'),
          '#default_value' => isset($solr_server->apachesolr_path) ? $solr_server->apachesolr_path : '',
          '#description' => t('Path that identifies the Solr request handler to be used Please include leading slash.'),
          '#required' => TRUE,
        );
        $form['apachesolr_is_multicore'] = array(
          '#type' => 'textfield',
          '#title' => t('Current Core count'),
          '#default_value' => isset($solr_server->apachesolr_is_multicore) ? $solr_server->apachesolr_is_multicore : '',
          '#description' => t('Please indicate the current number of cores the server has.'),
          '#required' => TRUE,
        );        
      break;
      
  }
  $form['submit_form'] = array(
    '#type' => 'submit',
    '#weight' => 10,
    '#value' => t('Save'),
  );
  return $form;
}

function a12_find_server_management_server_form_submit($form, $form_state) {
  unset($form_state['values']['submit_form']);
  unset($form_state['values']['form_build_id']);
  unset($form_state['values']['form_token']);
  unset($form_state['values']['form_id']);
  unset($form_state['values']['op']);
  if(isset($form_state['values']['sid'])) {
    db_update('a12_find_apachesolr_servers')
      ->condition('sid', $form_state['values']['sid'])
      ->fields($form_state['values'])
      ->execute();    
  }
  else {
    db_insert('a12_find_apachesolr_servers')
    ->fields($form_state['values'])
    ->execute();    
  }
}

/**
 * This function will format the client indicies data the way we need it by solr server
 * @staticvar type $indices
 * @param type $reset
 * @return type 
 * @example
 * s_data exampe
 * array(
 *  'DEV' => array('scheme' => 'http', 'host' => 'lily-02', 'port' => '8080', 'path' => 'solr/core1/', 'sid' => 1),
 *  'TEST' => array('scheme' => '', 'host' => '192.168.1.133', 'port' => '8080', 'path' => 'solr3/TEST/', 'sid' => 2),
 *  'LIVE' => array('scheme' => '', 'host' => '192.168.1.133', 'port' => '', 'path' => 'solr3/LIVE/', 'sid' => 3),
 * )
 */
function a12_find_server_management_format_indicies($reset=false) {
  static $indices;
  if(!$indices || $reset) {
    $_indices = a12_indexes_get_index();
    //Organise data in sid groups then key data to match against cores everything else is metadata
    foreach($_indices as $index) {
      if(!empty($index->s_data)) {
        $s_data = json_decode($index->s_data);
        foreach($s_data as $core_name => $core) {
          $core->name = sprintf('%s (%s)', $index->name, $core_name);
          $indices[$core->sid][$core->path] = $core;
        }
      }
    }
  }
  return $indices;
}

