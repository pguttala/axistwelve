<?php

/**
 * Views for the My Products.
 */

/**
 * Implements hook_views_default_views().
 */
function user_account_views_default_views() {
	$view = new view;
	$view->name = 'commerce_user_orders';
	$view->description = 'Display a list of completed orders for a user.';
	$view->tag = 'commerce';
	$view->base_table = 'commerce_order';
	$view->human_name = 'User orders';
	$view->core = 7;
	$view->api_version = '3.0-alpha1';
	$view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

	/* Display: Defaults */
	$handler = $view->new_display('default', 'Defaults', 'default');
	$handler->display->display_options['title'] = 'Orders';
	$handler->display->display_options['access']['type'] = 'perm';
	$handler->display->display_options['access']['perm'] = 'view own orders';
	$handler->display->display_options['cache']['type'] = 'none';
	$handler->display->display_options['query']['type'] = 'views_query';
	$handler->display->display_options['exposed_form']['type'] = 'basic';
	$handler->display->display_options['pager']['type'] = 'full';
	$handler->display->display_options['pager']['options']['items_per_page'] = 25;
	$handler->display->display_options['style_plugin'] = 'table';
	$handler->display->display_options['style_options']['columns'] = array(
		'order_number' => 'order_number',
		'created' => 'created',
		'changed' => 'changed',
		'commerce_order_total' => 'commerce_order_total',
		'status' => 'status',
	);
	$handler->display->display_options['style_options']['default'] = 'created';
	$handler->display->display_options['style_options']['info'] = array(
		'order_number' => array(
		  'sortable' => 1,
		  'default_sort_order' => 'asc',
		  'align' => '',
		  'separator' => '',
		),
		'created' => array(
		  'sortable' => 1,
		  'default_sort_order' => 'desc',
		  'align' => '',
		  'separator' => '',
		),
		'changed' => array(
		  'sortable' => 1,
		  'default_sort_order' => 'desc',
		  'align' => '',
		  'separator' => '',
		),
		'commerce_order_total' => array(
		  'sortable' => 0,
		  'default_sort_order' => 'asc',
		  'align' => '',
		  'separator' => '',
		),
		'status' => array(
		  'sortable' => 1,
		  'default_sort_order' => 'asc',
		  'align' => '',
		  'separator' => '',
		),
	);
	$handler->display->display_options['style_options']['override'] = 1;
	$handler->display->display_options['style_options']['sticky'] = 0;
	$handler->display->display_options['style_options']['order'] = 'desc';
	/* No results behavior: Global: Text area */
	$handler->display->display_options['empty']['text']['id'] = 'text';
	$handler->display->display_options['empty']['text']['table'] = 'views';
	$handler->display->display_options['empty']['text']['field'] = 'area';
	$handler->display->display_options['empty']['text']['empty'] = FALSE;
	$handler->display->display_options['empty']['text']['content'] = 'You have not placed any orders with us yet.';
	/* Relationship: Order: Referenced line item */
	$handler->display->display_options['relationships']['commerce_line_items_line_item_id']['id'] = 'commerce_line_items_line_item_id';
	$handler->display->display_options['relationships']['commerce_line_items_line_item_id']['table'] = 'field_data_commerce_line_items';
	$handler->display->display_options['relationships']['commerce_line_items_line_item_id']['field'] = 'commerce_line_items_line_item_id';
	/* Field: Commerce Order: Order number */
	$handler->display->display_options['fields']['order_number']['id'] = 'order_number';
	$handler->display->display_options['fields']['order_number']['table'] = 'commerce_order';
	$handler->display->display_options['fields']['order_number']['field'] = 'order_number';
	$handler->display->display_options['fields']['order_number']['alter']['alter_text'] = 0;
	$handler->display->display_options['fields']['order_number']['alter']['make_link'] = 0;
	$handler->display->display_options['fields']['order_number']['alter']['absolute'] = 0;
	$handler->display->display_options['fields']['order_number']['alter']['external'] = 0;
	$handler->display->display_options['fields']['order_number']['alter']['trim'] = 0;
	$handler->display->display_options['fields']['order_number']['alter']['nl2br'] = 0;
	$handler->display->display_options['fields']['order_number']['alter']['word_boundary'] = 1;
	$handler->display->display_options['fields']['order_number']['alter']['ellipsis'] = 1;
	$handler->display->display_options['fields']['order_number']['alter']['strip_tags'] = 0;
	$handler->display->display_options['fields']['order_number']['alter']['html'] = 0;
	$handler->display->display_options['fields']['order_number']['element_label_colon'] = 1;
	$handler->display->display_options['fields']['order_number']['element_default_classes'] = 1;
	$handler->display->display_options['fields']['order_number']['hide_empty'] = 0;
	$handler->display->display_options['fields']['order_number']['empty_zero'] = 0;
	$handler->display->display_options['fields']['order_number']['link_to_order'] = 'customer';
	/* Field: Commerce Order: Created date */
	$handler->display->display_options['fields']['created']['id'] = 'created';
	$handler->display->display_options['fields']['created']['table'] = 'commerce_order';
	$handler->display->display_options['fields']['created']['field'] = 'created';
	$handler->display->display_options['fields']['created']['label'] = 'Created';
	$handler->display->display_options['fields']['created']['alter']['alter_text'] = 0;
	$handler->display->display_options['fields']['created']['alter']['make_link'] = 0;
	$handler->display->display_options['fields']['created']['alter']['absolute'] = 0;
	$handler->display->display_options['fields']['created']['alter']['external'] = 0;
	$handler->display->display_options['fields']['created']['alter']['trim'] = 0;
	$handler->display->display_options['fields']['created']['alter']['nl2br'] = 0;
	$handler->display->display_options['fields']['created']['alter']['word_boundary'] = 1;
	$handler->display->display_options['fields']['created']['alter']['ellipsis'] = 1;
	$handler->display->display_options['fields']['created']['alter']['strip_tags'] = 0;
	$handler->display->display_options['fields']['created']['alter']['html'] = 0;
	$handler->display->display_options['fields']['created']['element_label_colon'] = 1;
	$handler->display->display_options['fields']['created']['element_default_classes'] = 1;
	$handler->display->display_options['fields']['created']['hide_empty'] = 0;
	$handler->display->display_options['fields']['created']['empty_zero'] = 0;
	/* Field: Commerce Order: Updated date */
	$handler->display->display_options['fields']['changed']['id'] = 'changed';
	$handler->display->display_options['fields']['changed']['table'] = 'commerce_order';
	$handler->display->display_options['fields']['changed']['field'] = 'changed';
	$handler->display->display_options['fields']['changed']['alter']['alter_text'] = 0;
	$handler->display->display_options['fields']['changed']['alter']['make_link'] = 0;
	$handler->display->display_options['fields']['changed']['alter']['absolute'] = 0;
	$handler->display->display_options['fields']['changed']['alter']['external'] = 0;
	$handler->display->display_options['fields']['changed']['alter']['trim'] = 0;
	$handler->display->display_options['fields']['changed']['alter']['nl2br'] = 0;
	$handler->display->display_options['fields']['changed']['alter']['word_boundary'] = 1;
	$handler->display->display_options['fields']['changed']['alter']['ellipsis'] = 1;
	$handler->display->display_options['fields']['changed']['alter']['strip_tags'] = 0;
	$handler->display->display_options['fields']['changed']['alter']['html'] = 0;
	$handler->display->display_options['fields']['changed']['element_label_colon'] = 1;
	$handler->display->display_options['fields']['changed']['element_default_classes'] = 1;
	$handler->display->display_options['fields']['changed']['hide_empty'] = 0;
	$handler->display->display_options['fields']['changed']['empty_zero'] = 0;
	/* Field: Order: Order total */
	$handler->display->display_options['fields']['commerce_order_total']['id'] = 'commerce_order_total';
	$handler->display->display_options['fields']['commerce_order_total']['table'] = 'field_data_commerce_order_total';
	$handler->display->display_options['fields']['commerce_order_total']['field'] = 'commerce_order_total';
	$handler->display->display_options['fields']['commerce_order_total']['label'] = 'Total';
	$handler->display->display_options['fields']['commerce_order_total']['alter']['alter_text'] = 0;
	$handler->display->display_options['fields']['commerce_order_total']['alter']['make_link'] = 0;
	$handler->display->display_options['fields']['commerce_order_total']['alter']['absolute'] = 0;
	$handler->display->display_options['fields']['commerce_order_total']['alter']['external'] = 0;
	$handler->display->display_options['fields']['commerce_order_total']['alter']['trim'] = 0;
	$handler->display->display_options['fields']['commerce_order_total']['alter']['nl2br'] = 0;
	$handler->display->display_options['fields']['commerce_order_total']['alter']['word_boundary'] = 1;
	$handler->display->display_options['fields']['commerce_order_total']['alter']['ellipsis'] = 1;
	$handler->display->display_options['fields']['commerce_order_total']['alter']['strip_tags'] = 0;
	$handler->display->display_options['fields']['commerce_order_total']['alter']['html'] = 0;
	$handler->display->display_options['fields']['commerce_order_total']['element_label_colon'] = 1;
	$handler->display->display_options['fields']['commerce_order_total']['element_default_classes'] = 1;
	$handler->display->display_options['fields']['commerce_order_total']['hide_empty'] = 0;
	$handler->display->display_options['fields']['commerce_order_total']['empty_zero'] = 0;
	$handler->display->display_options['fields']['commerce_order_total']['click_sort_column'] = 'amount';
	$handler->display->display_options['fields']['commerce_order_total']['type'] = 'commerce_price_formatted_amount';
	$handler->display->display_options['fields']['commerce_order_total']['settings'] = array(
		'calculation' => FALSE,
	);
	$handler->display->display_options['fields']['commerce_order_total']['field_api_classes'] = 0;
	/* Field: Commerce Order: Order status */
	$handler->display->display_options['fields']['status']['id'] = 'status';
	$handler->display->display_options['fields']['status']['table'] = 'commerce_order';
	$handler->display->display_options['fields']['status']['field'] = 'status';
	$handler->display->display_options['fields']['status']['alter']['alter_text'] = 0;
	$handler->display->display_options['fields']['status']['alter']['make_link'] = 0;
	$handler->display->display_options['fields']['status']['alter']['absolute'] = 0;
	$handler->display->display_options['fields']['status']['alter']['external'] = 0;
	$handler->display->display_options['fields']['status']['alter']['trim'] = 0;
	$handler->display->display_options['fields']['status']['alter']['nl2br'] = 0;
	$handler->display->display_options['fields']['status']['alter']['word_boundary'] = 1;
	$handler->display->display_options['fields']['status']['alter']['ellipsis'] = 1;
	$handler->display->display_options['fields']['status']['alter']['strip_tags'] = 0;
	$handler->display->display_options['fields']['status']['alter']['html'] = 0;
	$handler->display->display_options['fields']['status']['element_label_colon'] = 1;
	$handler->display->display_options['fields']['status']['element_default_classes'] = 1;
	$handler->display->display_options['fields']['status']['hide_empty'] = 0;
	$handler->display->display_options['fields']['status']['empty_zero'] = 0;
	/* Field: Line item: Display path */
	$handler->display->display_options['fields']['commerce_display_path']['id'] = 'commerce_display_path';
	$handler->display->display_options['fields']['commerce_display_path']['table'] = 'field_data_commerce_display_path';
	$handler->display->display_options['fields']['commerce_display_path']['field'] = 'commerce_display_path';
	$handler->display->display_options['fields']['commerce_display_path']['relationship'] = 'commerce_line_items_line_item_id';
	/* Field: Line item: Product */
	$handler->display->display_options['fields']['commerce_product']['id'] = 'commerce_product';
	$handler->display->display_options['fields']['commerce_product']['table'] = 'field_data_commerce_product';
	$handler->display->display_options['fields']['commerce_product']['field'] = 'commerce_product';
	$handler->display->display_options['fields']['commerce_product']['relationship'] = 'commerce_line_items_line_item_id';
	/* Contextual filter: Commerce Order: Uid */
	$handler->display->display_options['arguments']['uid_1']['id'] = 'uid_1';
	$handler->display->display_options['arguments']['uid_1']['table'] = 'commerce_order';
	$handler->display->display_options['arguments']['uid_1']['field'] = 'uid';
	$handler->display->display_options['arguments']['uid_1']['default_action'] = 'not found';
	$handler->display->display_options['arguments']['uid_1']['default_argument_type'] = 'fixed';
	$handler->display->display_options['arguments']['uid_1']['summary']['number_of_records'] = '0';
	$handler->display->display_options['arguments']['uid_1']['summary']['format'] = 'default_summary';
	$handler->display->display_options['arguments']['uid_1']['summary_options']['items_per_page'] = '25';
	$handler->display->display_options['arguments']['uid_1']['specify_validation'] = 1;
	$handler->display->display_options['arguments']['uid_1']['validate']['type'] = 'current_user_or_role';
	$handler->display->display_options['arguments']['uid_1']['validate_options']['restrict_roles'] = '1';
	$handler->display->display_options['arguments']['uid_1']['validate_options']['roles'] = array(
		3 => '3',
	);
	$handler->display->display_options['arguments']['uid_1']['break_phrase'] = 0;
	$handler->display->display_options['arguments']['uid_1']['not'] = 0;
	/* Filter criterion: Commerce Order: Order state */
	$handler->display->display_options['filters']['state']['id'] = 'state';
	$handler->display->display_options['filters']['state']['table'] = 'commerce_order';
	$handler->display->display_options['filters']['state']['field'] = 'state';
	$handler->display->display_options['filters']['state']['operator'] = 'not in';
	$handler->display->display_options['filters']['state']['value'] = array(
		'cart' => 'cart',
		'checkout' => 'checkout',
	);

	/* Display: User Orders */
	$handler = $view->new_display('page', 'User Orders', 'order_page');
	$handler->display->display_options['defaults']['fields'] = FALSE;
	/* Field: Commerce Order: Order number */
	$handler->display->display_options['fields']['order_number']['id'] = 'order_number';
	$handler->display->display_options['fields']['order_number']['table'] = 'commerce_order';
	$handler->display->display_options['fields']['order_number']['field'] = 'order_number';
	$handler->display->display_options['fields']['order_number']['alter']['alter_text'] = 0;
	$handler->display->display_options['fields']['order_number']['alter']['make_link'] = 0;
	$handler->display->display_options['fields']['order_number']['alter']['absolute'] = 0;
	$handler->display->display_options['fields']['order_number']['alter']['external'] = 0;
	$handler->display->display_options['fields']['order_number']['alter']['trim'] = 0;
	$handler->display->display_options['fields']['order_number']['alter']['nl2br'] = 0;
	$handler->display->display_options['fields']['order_number']['alter']['word_boundary'] = 1;
	$handler->display->display_options['fields']['order_number']['alter']['ellipsis'] = 1;
	$handler->display->display_options['fields']['order_number']['alter']['strip_tags'] = 0;
	$handler->display->display_options['fields']['order_number']['alter']['html'] = 0;
	$handler->display->display_options['fields']['order_number']['element_label_colon'] = 1;
	$handler->display->display_options['fields']['order_number']['element_default_classes'] = 1;
	$handler->display->display_options['fields']['order_number']['hide_empty'] = 0;
	$handler->display->display_options['fields']['order_number']['empty_zero'] = 0;
	$handler->display->display_options['fields']['order_number']['link_to_order'] = 'customer';
	/* Field: Commerce Order: Created date */
	$handler->display->display_options['fields']['created']['id'] = 'created';
	$handler->display->display_options['fields']['created']['table'] = 'commerce_order';
	$handler->display->display_options['fields']['created']['field'] = 'created';
	$handler->display->display_options['fields']['created']['label'] = 'Created';
	$handler->display->display_options['fields']['created']['alter']['alter_text'] = 0;
	$handler->display->display_options['fields']['created']['alter']['make_link'] = 0;
	$handler->display->display_options['fields']['created']['alter']['absolute'] = 0;
	$handler->display->display_options['fields']['created']['alter']['external'] = 0;
	$handler->display->display_options['fields']['created']['alter']['trim'] = 0;
	$handler->display->display_options['fields']['created']['alter']['nl2br'] = 0;
	$handler->display->display_options['fields']['created']['alter']['word_boundary'] = 1;
	$handler->display->display_options['fields']['created']['alter']['ellipsis'] = 1;
	$handler->display->display_options['fields']['created']['alter']['strip_tags'] = 0;
	$handler->display->display_options['fields']['created']['alter']['html'] = 0;
	$handler->display->display_options['fields']['created']['element_label_colon'] = 1;
	$handler->display->display_options['fields']['created']['element_default_classes'] = 1;
	$handler->display->display_options['fields']['created']['hide_empty'] = 0;
	$handler->display->display_options['fields']['created']['empty_zero'] = 0;
	/* Field: Commerce Order: Updated date */
	$handler->display->display_options['fields']['changed']['id'] = 'changed';
	$handler->display->display_options['fields']['changed']['table'] = 'commerce_order';
	$handler->display->display_options['fields']['changed']['field'] = 'changed';
	$handler->display->display_options['fields']['changed']['alter']['alter_text'] = 0;
	$handler->display->display_options['fields']['changed']['alter']['make_link'] = 0;
	$handler->display->display_options['fields']['changed']['alter']['absolute'] = 0;
	$handler->display->display_options['fields']['changed']['alter']['external'] = 0;
	$handler->display->display_options['fields']['changed']['alter']['trim'] = 0;
	$handler->display->display_options['fields']['changed']['alter']['nl2br'] = 0;
	$handler->display->display_options['fields']['changed']['alter']['word_boundary'] = 1;
	$handler->display->display_options['fields']['changed']['alter']['ellipsis'] = 1;
	$handler->display->display_options['fields']['changed']['alter']['strip_tags'] = 0;
	$handler->display->display_options['fields']['changed']['alter']['html'] = 0;
	$handler->display->display_options['fields']['changed']['element_label_colon'] = 1;
	$handler->display->display_options['fields']['changed']['element_default_classes'] = 1;
	$handler->display->display_options['fields']['changed']['hide_empty'] = 0;
	$handler->display->display_options['fields']['changed']['empty_zero'] = 0;
	/* Field: Order: Order total */
	$handler->display->display_options['fields']['commerce_order_total']['id'] = 'commerce_order_total';
	$handler->display->display_options['fields']['commerce_order_total']['table'] = 'field_data_commerce_order_total';
	$handler->display->display_options['fields']['commerce_order_total']['field'] = 'commerce_order_total';
	$handler->display->display_options['fields']['commerce_order_total']['label'] = 'Total';
	$handler->display->display_options['fields']['commerce_order_total']['alter']['alter_text'] = 0;
	$handler->display->display_options['fields']['commerce_order_total']['alter']['make_link'] = 0;
	$handler->display->display_options['fields']['commerce_order_total']['alter']['absolute'] = 0;
	$handler->display->display_options['fields']['commerce_order_total']['alter']['external'] = 0;
	$handler->display->display_options['fields']['commerce_order_total']['alter']['trim'] = 0;
	$handler->display->display_options['fields']['commerce_order_total']['alter']['nl2br'] = 0;
	$handler->display->display_options['fields']['commerce_order_total']['alter']['word_boundary'] = 1;
	$handler->display->display_options['fields']['commerce_order_total']['alter']['ellipsis'] = 1;
	$handler->display->display_options['fields']['commerce_order_total']['alter']['strip_tags'] = 0;
	$handler->display->display_options['fields']['commerce_order_total']['alter']['html'] = 0;
	$handler->display->display_options['fields']['commerce_order_total']['element_label_colon'] = 1;
	$handler->display->display_options['fields']['commerce_order_total']['element_default_classes'] = 1;
	$handler->display->display_options['fields']['commerce_order_total']['hide_empty'] = 0;
	$handler->display->display_options['fields']['commerce_order_total']['empty_zero'] = 0;
	$handler->display->display_options['fields']['commerce_order_total']['click_sort_column'] = 'amount';
	$handler->display->display_options['fields']['commerce_order_total']['type'] = 'commerce_price_formatted_amount';
	$handler->display->display_options['fields']['commerce_order_total']['settings'] = array(
		'calculation' => FALSE,
	);
	$handler->display->display_options['fields']['commerce_order_total']['field_api_classes'] = 0;
	/* Field: Commerce Order: Order status */
	$handler->display->display_options['fields']['status']['id'] = 'status';
	$handler->display->display_options['fields']['status']['table'] = 'commerce_order';
	$handler->display->display_options['fields']['status']['field'] = 'status';
	$handler->display->display_options['fields']['status']['alter']['alter_text'] = 0;
	$handler->display->display_options['fields']['status']['alter']['make_link'] = 0;
	$handler->display->display_options['fields']['status']['alter']['absolute'] = 0;
	$handler->display->display_options['fields']['status']['alter']['external'] = 0;
	$handler->display->display_options['fields']['status']['alter']['trim'] = 0;
	$handler->display->display_options['fields']['status']['alter']['nl2br'] = 0;
	$handler->display->display_options['fields']['status']['alter']['word_boundary'] = 1;
	$handler->display->display_options['fields']['status']['alter']['ellipsis'] = 1;
	$handler->display->display_options['fields']['status']['alter']['strip_tags'] = 0;
	$handler->display->display_options['fields']['status']['alter']['html'] = 0;
	$handler->display->display_options['fields']['status']['element_label_colon'] = 1;
	$handler->display->display_options['fields']['status']['element_default_classes'] = 1;
	$handler->display->display_options['fields']['status']['hide_empty'] = 0;
	$handler->display->display_options['fields']['status']['empty_zero'] = 0;
	$handler->display->display_options['path'] = 'user/%/orders';
	$handler->display->display_options['menu']['title'] = 'Orders';
	$handler->display->display_options['menu']['weight'] = '15';
	$handler->display->display_options['tab_options']['type'] = 'normal';
	$handler->display->display_options['tab_options']['title'] = 'Orders';
	$handler->display->display_options['tab_options']['description'] = 'User orders in the store.';
	$handler->display->display_options['tab_options']['weight'] = '';
	$handler->display->display_options['tab_options']['name'] = 'user-menu';

	/* Display: Page */
	$handler = $view->new_display('page', 'Page', 'page_1');
	$handler->display->display_options['defaults']['title'] = FALSE;
	$handler->display->display_options['title'] = 'My Subscriptions';
	$handler->display->display_options['defaults']['relationships'] = FALSE;
	/* Relationship: Order: Referenced line item */
	$handler->display->display_options['relationships']['commerce_line_items_line_item_id']['id'] = 'commerce_line_items_line_item_id';
	$handler->display->display_options['relationships']['commerce_line_items_line_item_id']['table'] = 'field_data_commerce_line_items';
	$handler->display->display_options['relationships']['commerce_line_items_line_item_id']['field'] = 'commerce_line_items_line_item_id';
	/* Relationship: Line item: Referenced product */
	$handler->display->display_options['relationships']['commerce_product_product_id']['id'] = 'commerce_product_product_id';
	$handler->display->display_options['relationships']['commerce_product_product_id']['table'] = 'field_data_commerce_product';
	$handler->display->display_options['relationships']['commerce_product_product_id']['field'] = 'commerce_product_product_id';
	$handler->display->display_options['relationships']['commerce_product_product_id']['relationship'] = 'commerce_line_items_line_item_id';
	$handler->display->display_options['relationships']['commerce_product_product_id']['required'] = 0;
	$handler->display->display_options['defaults']['fields'] = FALSE;
	/* Field: Line item: Display path */
	$handler->display->display_options['fields']['commerce_display_path']['id'] = 'commerce_display_path';
	$handler->display->display_options['fields']['commerce_display_path']['table'] = 'field_data_commerce_display_path';
	$handler->display->display_options['fields']['commerce_display_path']['field'] = 'commerce_display_path';
	$handler->display->display_options['fields']['commerce_display_path']['relationship'] = 'commerce_line_items_line_item_id';
	$handler->display->display_options['fields']['commerce_display_path']['exclude'] = TRUE;
	$handler->display->display_options['fields']['commerce_display_path']['alter']['alter_text'] = 0;
	$handler->display->display_options['fields']['commerce_display_path']['alter']['make_link'] = 0;
	$handler->display->display_options['fields']['commerce_display_path']['alter']['absolute'] = 0;
	$handler->display->display_options['fields']['commerce_display_path']['alter']['external'] = 0;
	$handler->display->display_options['fields']['commerce_display_path']['alter']['replace_spaces'] = 0;
	$handler->display->display_options['fields']['commerce_display_path']['alter']['trim'] = 0;
	$handler->display->display_options['fields']['commerce_display_path']['alter']['nl2br'] = 0;
	$handler->display->display_options['fields']['commerce_display_path']['alter']['word_boundary'] = 1;
	$handler->display->display_options['fields']['commerce_display_path']['alter']['ellipsis'] = 1;
	$handler->display->display_options['fields']['commerce_display_path']['alter']['strip_tags'] = 0;
	$handler->display->display_options['fields']['commerce_display_path']['alter']['html'] = 0;
	$handler->display->display_options['fields']['commerce_display_path']['element_label_colon'] = 1;
	$handler->display->display_options['fields']['commerce_display_path']['element_default_classes'] = 1;
	$handler->display->display_options['fields']['commerce_display_path']['hide_empty'] = 0;
	$handler->display->display_options['fields']['commerce_display_path']['empty_zero'] = 0;
	$handler->display->display_options['fields']['commerce_display_path']['field_api_classes'] = 0;
	/* Field: Line item: Product */
	$handler->display->display_options['fields']['commerce_product']['id'] = 'commerce_product';
	$handler->display->display_options['fields']['commerce_product']['table'] = 'field_data_commerce_product';
	$handler->display->display_options['fields']['commerce_product']['field'] = 'commerce_product';
	$handler->display->display_options['fields']['commerce_product']['relationship'] = 'commerce_line_items_line_item_id';
	$handler->display->display_options['fields']['commerce_product']['alter']['alter_text'] = 0;
	$handler->display->display_options['fields']['commerce_product']['alter']['make_link'] = 1;
	$handler->display->display_options['fields']['commerce_product']['alter']['path'] = '[commerce_display_path] ';
	$handler->display->display_options['fields']['commerce_product']['alter']['absolute'] = 0;
	$handler->display->display_options['fields']['commerce_product']['alter']['external'] = 0;
	$handler->display->display_options['fields']['commerce_product']['alter']['replace_spaces'] = 0;
	$handler->display->display_options['fields']['commerce_product']['alter']['trim'] = 0;
	$handler->display->display_options['fields']['commerce_product']['alter']['nl2br'] = 0;
	$handler->display->display_options['fields']['commerce_product']['alter']['word_boundary'] = 1;
	$handler->display->display_options['fields']['commerce_product']['alter']['ellipsis'] = 1;
	$handler->display->display_options['fields']['commerce_product']['alter']['strip_tags'] = 0;
	$handler->display->display_options['fields']['commerce_product']['alter']['html'] = 0;
	$handler->display->display_options['fields']['commerce_product']['element_label_colon'] = 1;
	$handler->display->display_options['fields']['commerce_product']['element_default_classes'] = 1;
	$handler->display->display_options['fields']['commerce_product']['hide_empty'] = 0;
	$handler->display->display_options['fields']['commerce_product']['empty_zero'] = 0;
	$handler->display->display_options['fields']['commerce_product']['type'] = 'commerce_product_reference_title_plain';
	$handler->display->display_options['fields']['commerce_product']['settings'] = array(
		'show_quantity' => 0,
		'default_quantity' => '1',
	);
	$handler->display->display_options['fields']['commerce_product']['field_api_classes'] = 0;
	/* Field: Commerce Order: Created date */
	$handler->display->display_options['fields']['created']['id'] = 'created';
	$handler->display->display_options['fields']['created']['table'] = 'commerce_order';
	$handler->display->display_options['fields']['created']['field'] = 'created';
	$handler->display->display_options['fields']['created']['label'] = 'Created';
	$handler->display->display_options['fields']['created']['alter']['alter_text'] = 0;
	$handler->display->display_options['fields']['created']['alter']['make_link'] = 0;
	$handler->display->display_options['fields']['created']['alter']['absolute'] = 0;
	$handler->display->display_options['fields']['created']['alter']['external'] = 0;
	$handler->display->display_options['fields']['created']['alter']['trim'] = 0;
	$handler->display->display_options['fields']['created']['alter']['nl2br'] = 0;
	$handler->display->display_options['fields']['created']['alter']['word_boundary'] = 1;
	$handler->display->display_options['fields']['created']['alter']['ellipsis'] = 1;
	$handler->display->display_options['fields']['created']['alter']['strip_tags'] = 0;
	$handler->display->display_options['fields']['created']['alter']['html'] = 0;
	$handler->display->display_options['fields']['created']['element_label_colon'] = 1;
	$handler->display->display_options['fields']['created']['element_default_classes'] = 1;
	$handler->display->display_options['fields']['created']['hide_empty'] = 0;
	$handler->display->display_options['fields']['created']['empty_zero'] = 0;
	/* Field: Commerce Product: Product ID */
	$handler->display->display_options['fields']['product_id']['id'] = 'product_id';
	$handler->display->display_options['fields']['product_id']['table'] = 'commerce_product';
	$handler->display->display_options['fields']['product_id']['field'] = 'product_id';
	$handler->display->display_options['fields']['product_id']['relationship'] = 'commerce_product_product_id';
	$handler->display->display_options['fields']['product_id']['label'] = 'Product Control Panel';
	$handler->display->display_options['fields']['product_id']['alter']['alter_text'] = 1;
	$handler->display->display_options['fields']['product_id']['alter']['text'] = 'Product Control Panel';
	$handler->display->display_options['fields']['product_id']['alter']['make_link'] = 1;
	$handler->display->display_options['fields']['product_id']['alter']['path'] = 'product_control_panel/[product_id]';
	$handler->display->display_options['fields']['product_id']['alter']['absolute'] = 0;
	$handler->display->display_options['fields']['product_id']['alter']['external'] = 0;
	$handler->display->display_options['fields']['product_id']['alter']['replace_spaces'] = 0;
	$handler->display->display_options['fields']['product_id']['alter']['trim'] = 0;
	$handler->display->display_options['fields']['product_id']['alter']['nl2br'] = 0;
	$handler->display->display_options['fields']['product_id']['alter']['word_boundary'] = 1;
	$handler->display->display_options['fields']['product_id']['alter']['ellipsis'] = 1;
	$handler->display->display_options['fields']['product_id']['alter']['strip_tags'] = 0;
	$handler->display->display_options['fields']['product_id']['alter']['html'] = 0;
	$handler->display->display_options['fields']['product_id']['element_label_colon'] = 1;
	$handler->display->display_options['fields']['product_id']['element_default_classes'] = 1;
	$handler->display->display_options['fields']['product_id']['hide_empty'] = 0;
	$handler->display->display_options['fields']['product_id']['empty_zero'] = 0;
	$handler->display->display_options['fields']['product_id']['link_to_product'] = 0;
	/* Field: Commerce Order: Delete link */
	$handler->display->display_options['fields']['delete_order']['id'] = 'delete_order';
	$handler->display->display_options['fields']['delete_order']['table'] = 'commerce_order';
	$handler->display->display_options['fields']['delete_order']['field'] = 'delete_order';
	$handler->display->display_options['fields']['delete_order']['label'] = 'Unsubscribe';
	$handler->display->display_options['fields']['delete_order']['alter']['alter_text'] = 0;
	$handler->display->display_options['fields']['delete_order']['alter']['make_link'] = 0;
	$handler->display->display_options['fields']['delete_order']['alter']['absolute'] = 0;
	$handler->display->display_options['fields']['delete_order']['alter']['external'] = 0;
	$handler->display->display_options['fields']['delete_order']['alter']['replace_spaces'] = 0;
	$handler->display->display_options['fields']['delete_order']['alter']['trim'] = 0;
	$handler->display->display_options['fields']['delete_order']['alter']['nl2br'] = 0;
	$handler->display->display_options['fields']['delete_order']['alter']['word_boundary'] = 1;
	$handler->display->display_options['fields']['delete_order']['alter']['ellipsis'] = 1;
	$handler->display->display_options['fields']['delete_order']['alter']['strip_tags'] = 0;
	$handler->display->display_options['fields']['delete_order']['alter']['html'] = 0;
	$handler->display->display_options['fields']['delete_order']['element_label_colon'] = 1;
	$handler->display->display_options['fields']['delete_order']['element_default_classes'] = 1;
	$handler->display->display_options['fields']['delete_order']['hide_empty'] = 0;
	$handler->display->display_options['fields']['delete_order']['empty_zero'] = 0;
	$handler->display->display_options['fields']['delete_order']['text'] = 'Unsubscribe';
	/* Field: Commerce Product: Type */
	$handler->display->display_options['fields']['type']['id'] = 'type';
	$handler->display->display_options['fields']['type']['table'] = 'commerce_product';
	$handler->display->display_options['fields']['type']['field'] = 'type';
	$handler->display->display_options['fields']['type']['relationship'] = 'commerce_product_product_id';
	$handler->display->display_options['fields']['type']['label'] = 'Product Category';
	$handler->display->display_options['fields']['type']['alter']['alter_text'] = 0;
	$handler->display->display_options['fields']['type']['alter']['make_link'] = 0;
	$handler->display->display_options['fields']['type']['alter']['absolute'] = 0;
	$handler->display->display_options['fields']['type']['alter']['external'] = 0;
	$handler->display->display_options['fields']['type']['alter']['replace_spaces'] = 0;
	$handler->display->display_options['fields']['type']['alter']['trim'] = 0;
	$handler->display->display_options['fields']['type']['alter']['nl2br'] = 0;
	$handler->display->display_options['fields']['type']['alter']['word_boundary'] = 1;
	$handler->display->display_options['fields']['type']['alter']['ellipsis'] = 1;
	$handler->display->display_options['fields']['type']['alter']['strip_tags'] = 0;
	$handler->display->display_options['fields']['type']['alter']['html'] = 0;
	$handler->display->display_options['fields']['type']['element_label_colon'] = 1;
	$handler->display->display_options['fields']['type']['element_default_classes'] = 1;
	$handler->display->display_options['fields']['type']['hide_empty'] = 0;
	$handler->display->display_options['fields']['type']['empty_zero'] = 0;
	$handler->display->display_options['fields']['type']['link_to_product'] = 0;
	$handler->display->display_options['defaults']['filters'] = FALSE;
	/* Filter criterion: Commerce Order: Order state */
	$handler->display->display_options['filters']['state']['id'] = 'state';
	$handler->display->display_options['filters']['state']['table'] = 'commerce_order';
	$handler->display->display_options['filters']['state']['field'] = 'state';
	$handler->display->display_options['filters']['state']['value'] = array(
		'checkout' => 'checkout',
		'pending' => 'pending',
		'completed' => 'completed',
	);
	$handler->display->display_options['path'] = 'user/%/my_subscriptions';
	$handler->display->display_options['menu']['type'] = 'tab';
	$handler->display->display_options['menu']['title'] = 'My Subscriptions';
	$handler->display->display_options['menu']['description'] = 'Products subscribed by user';
	$handler->display->display_options['menu']['weight'] = '16';
	$translatables['commerce_user_orders'] = array(
		t('Defaults'),
		t('Orders'),
		t('more'),
		t('Apply'),
		t('Reset'),
		t('Sort by'),
		t('Asc'),
		t('Desc'),
		t('Items per page'),
		t('- All -'),
		t('Offset'),
		t('You have not placed any orders with us yet.'),
		t('Line Item'),
		t('Order number'),
		t('Created'),
		t('Updated date'),
		t('Total'),
		t('Order status'),
		t('Display path'),
		t('Product'),
		t('All'),
		t('User Orders'),
		t('Page'),
		t('My Subscriptions'),
		t('[commerce_display_path] '),
		t('Product Control Panel'),
		t('product_control_panel/[product_id]'),
		t('Unsubscribe'),
		t('Product Category'),
	);

  $views[$view->name] = $view;
  
  return $views;
}
