<?php  // $Id: security_credentials.tpl.php, v 1.01 2011/05/05 13:59:44 syeole Exp $ ?>
<?php global $user; ?>
<!-- <p id="add_new_key"> -->
<?php print l('<span class="add_new_link">' .t('+') .'</span>' .t(' Create new Access Key'),'user/create_secret_access_key',array('html' => 'TRUE')); ?>
<br>
<!-- </p> -->
<p>Access Keys are used to gain access to the A12 services and to grant permissions on your index(s). You can use a single key to authenticate all the sites you manage, or create a separate key for each site for stricter access control.<br/><br/>The <strong>Key Identifier</strong> and the <strong>Secret Key</strong> are needed to authenticate your site with the Axistwelve services. Click the 'Show Secret Key' button to display your Secret Key, and enter this and the Identifier into the Subscription tab on your site.</p>
<table class="table-security-credentials">
	<tr>
		<td>		
			<table id = "access-key-details-table" class = "access-key-details">			
				<?php if(count($records) == 0) { ?>
				<tr>
					<td colspan=6 > You currently do not have any Access Keys created. </td>
				</tr>
				<?php } else { ?>
				<tr>
					<th colspan = 6 class="title">&nbsp;</th>			
				</tr>
				<tr class="col-title">
					<th class="key_user_key_name">Key Name</th>
                    <th class="key_token_id">Key Identifier</th>
					<th class="key_status">Status</th>
					<th class="key_manage">Manage</th>
					<th class="key_has_access last_cell">Has access to...</th>					
				</tr>
				<?php $index_keys = user_account_user_index_key_info(); // user index key info ?>
					<?php foreach($records as $id => $record) { ?>
					<?php $key_delete_op = l(t('Delete'), 'user/'.$record -> secret_key_id.'/delete_access_key');
                            if($record -> status == ACTIVE || $record -> status == UPDATED) {
								$key_status = 'Enabled';
								$key_status_op = l(t('Disable'),'security_credentials/ajax/'.$record -> secret_key_id.'/'.INACTIVE,array('attributes' => array('class' => array('use-ajax'))));								
								$key_operation = '(' .$key_status_op .')';
							}
							else {
								$url_del = url('security_credentials/ajax/'. $record -> secret_key_id .'/op_delete');
								$key_status = 'Disabled';
								$key_status_op = l(t('Enable'),'security_credentials/ajax/'.$record -> secret_key_id.'/'.ACTIVE,array('attributes' => array('class' => array('use-ajax'))));
								//$key_delete_op = l(t('Delete'), 'user/'.$record -> secret_key_id.'/delete_access_key');
								//$key_operation = '(' .$key_status_op . '&nbsp;|&nbsp; ' . $key_delete_op . ')';
                                $key_operation = '(' .$key_status_op .')';
							}    
					?>
				<tr <?php if($record -> status == INACTIVE) print t('class="key_disabled"');?>>
				 	<td><?php print $record -> key_name; ?></td>
                    <td><?php print $record -> token_id; ?></td>
					<td><?php print $key_status . ' <br> '. $key_operation; ?></td>
					<td><?php print l(t("Edit") , "user/".$record -> secret_key_id."/edit_access_key") . '&nbsp;|&nbsp;' . $key_delete_op; ?></td>
					<td class="last_cell">
						<?php
						// get the keys only having access for the access key
							$default_indexes_obj = db_query("	SELECT label FROM {a12_indexes}
							WHERE index_id IN	(SELECT B.index_id FROM {a12_indexes} A, {a12_map_indexes} B
							WHERE A.uid = :uid AND A.index_id = B.index_id AND B.secret_key_id = :secret_key_id
							) ORDER BY is_default DESC, label ASC
							",
							array(':uid' => $user->uid, ':secret_key_id' => $record -> secret_key_id) );
							$default_indexes = $default_indexes_obj -> fetchAll();
							
                            $accessed_index_list = "";
                            $accessed_index_list_count = 0;
                                
							foreach($default_indexes as $kid => $accessible_key) {
                                $accessed_index_list_count += 1;
                                $accessed_index_list .= $accessed_index_list_count .') ' .$accessible_key->label .'<br/>';
							}
                            if($accessed_index_list=="") $accessed_index_list = '<i>No permissions are set for this key</i>';
                            print $accessed_index_list;
						?>
						
					</td>
				</tr>
				<tr class="system_token_row <?php if($record -> status == INACTIVE) print t('key_disabled');?>">
					<td class="first_cell access-key-details-table-col"><?php print '<div class="ajax_div">'.l(t("Show Secret Key") , "a12_indexes/".$record -> secret_key_id."/show_secret_key_token", array('attributes' => array('class' => array('use-ajax', 'unite_link'), 'id' => array('change-key-button-text-'.$record -> secret_key_id)))). '</div>'; ?></td>
					<td colspan=3 class='access-key-details-table-col'><div id='show_secret_key_token_for_<?php print $record -> secret_key_id ?>' > <span>Secret Key</span> <span><input type='text' readonly='readonly' id='text_to_show_secret_key_token_for_<?php print $record -> secret_key_id ?>' class='textInput rounded' value=''></span> </div></td>
					<td class='access-key-details-table-col last_cell'><?php print '<div class="ajax_div">'.l(t("Set permissions") , "a12_indexes/".$record -> secret_key_id."/manage_access_key_settings", array('attributes' => array('class' => array('use-ajax', 'unite_link')))). '</div>'; ?></td>
				</tr>
					<?php } ?>
				<?php }?>		
			</table>
		</td>
	</tr>		
</table>

<p>

<div id="configure_access_key"></div>
