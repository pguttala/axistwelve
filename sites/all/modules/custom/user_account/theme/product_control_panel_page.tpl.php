<div>
 <div>
   <p>
    This is your product control panel, from here you can access all your Secret key, Billing info & Data metrics.
   </p>
 </div>
<?php foreach ($records as $key => $record) { ?>
    <div class="panel-info">
      <div class="panel-info-image">
        <a href="<?php print $record['click_url']; ?>">
          <img alt="<?php print $record['title']; ?>" src="<?php print $record['img']; ?>">
        </a>
      </div>
      <div>
        <div><h3><?php print $record['links']; ?></h3></div>
        <div><?php print $record['desc']; ?></div>
      </div>
    </div>
<?php } ?>
</div>