<?php

/**
* Callback for creating activity resources.
*
* @param object $data
* @return object
*/
function _a12_get_subscription_retrieve($data) {
  global $user;
  
  $data = (object)$data;

  if (!isset($data->key)) {
    return services_error('Missing key identifier', 406);
  }
  
  $result = db_query("SELECT uid,user_system_token, token_id, key_name FROM {user_system_token} WHERE status = 1 AND token_id=:id", array(":id" => $data->key))->fetchObject();
  if($result == null) {
    return (object)array('status' => 'error', 'error_code' => 'FND-603');
  } 
  else {
    $client = user_load($result->uid);
    $subscriptions = db_query("SELECT product_id, status FROM {a12_product_subscriptions_history} WHERE uid=".$client->uid . ' ORDER BY a12_product_subscriptions_history_id DESC LIMIT 1')->fetchAll();
    foreach($subscriptions as $subscription) {
        if($subscription->status == 1) {
          return (object)array(
            'uuid' => $client->field_a12_identifier['und'][0]['value'],
            'keyidentifier' => $result->token_id,
            'secretkey' => $result->user_system_token,
            'subscriptions' => $subscriptions,
            'status' => 'success',
          );
        }
    }
    return (object)array("status" => "error", "error_code" => "FND-601");
  }
}



