<?php
/**
 * @file
 * registration_flag_field.features.field.inc
 */

/**
 * Implements hook_field_default_fields().
 */
function registration_flag_field_field_default_fields() {
  $fields = array();

  // Exported field: 'profile2-registration_flag-field_registration_freetria'
  $fields['profile2-registration_flag-field_registration_freetria'] = array(
    'field_config' => array(
      'active' => '1',
      'cardinality' => '-1',
      'deleted' => '0',
      'entity_types' => array(),
      'field_name' => 'field_registration_freetria',
      'foreign keys' => array(),
      'indexes' => array(
        'value' => array(
          0 => 'value',
        ),
      ),
      'module' => 'list',
      'settings' => array(
        'allowed_values' => array(
          1 => 'This is flag set on registration from three step  free trial.',
        ),
        'allowed_values_function' => '',
        'profile2_private' => 0,
      ),
      'translatable' => '0',
      'type' => 'list_text',
    ),
    'field_instance' => array(
      'bundle' => 'registration_flag',
      'default_value' => NULL,
      'deleted' => '0',
      'description' => '',
      'display' => array(
        'default' => array(
          'label' => 'inline',
          'module' => 'list',
          'settings' => array(),
          'type' => 'list_default',
          'weight' => '0',
        ),
      ),
      'entity_type' => 'profile2',
      'field_name' => 'field_registration_freetria',
      'label' => 'Registration freetrial',
      'required' => 0,
      'settings' => array(
        'user_register_form' => FALSE,
      ),
      'widget' => array(
        'active' => 1,
        'module' => 'options',
        'settings' => array(),
        'type' => 'options_buttons',
        'weight' => '31',
      ),
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Registration freetrial');

  return $fields;
}
