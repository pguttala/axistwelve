<?php
// $Id: tax_ui.module, 2011/09/20 05:42:45 mkhan Exp $

/**
 * Implements hook_schema().
 */
function tax_ui_schema() {
  $schema = array();
  $schema['a12_commerce_tax_rate_history'] = array(
    'description' => 'Stores information about the A12 product Tax history',
    'fields' => array(
      'a12_commerce_tax_rate_history_id' => array(
        'description' => 'The primary identifier for A12 Commerce Tax Rate history, used internally only.',   
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'uid' => array(
        'description' => 'The {users}.uid who has created tax rate.',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
      'name' => array(
        'description' => 'The machine-name of this type.',
        'type' => 'varchar',
        'length' => 64,
        'not null' => TRUE,
        'default' => '',
      ),
      'title' => array(
        'description' => 'The administrative title of this type.',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
      ),
      'description' => array(
        'description' => 'A brief description of this type.',
        'type' => 'text',
        'not null' => TRUE,
        'size' => 'medium',
      ),
      'type' => array(
        'description' => 'The type of Tax (VAT, Sales...)',
        'type' => 'varchar',
        'length' => 64,
        'not null' => TRUE,
        'default' => '',
      ),
      'rate' => array(
        'description' => 'The percentage used to calculate this tax expressed as a decimal.',
        'type' => 'varchar',
        'length' => 64,
        'not null' => TRUE,
        'default' => '',
      ),
      'created' => array(
        'description' => 'The Unix timestamp when the commerce tax rate was created.',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
        'length' => 11
      ),
      'status' => array(
        'description' => 'The current status of the commerce tax rate (Active | Deleted).',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
        'size' => 'tiny',
      ),
    ),
    'primary key' => array('a12_commerce_tax_rate_history_id'),
  );
  return $schema;
}
