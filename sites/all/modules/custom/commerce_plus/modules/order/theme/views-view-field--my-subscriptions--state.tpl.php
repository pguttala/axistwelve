<?php
 /**
  * This template is used to print a single field in a view. It is not
  * actually used in default Views, as this is registered as a theme
  * function which has better performance. For single overrides, the
  * template is perfectly okay.
  *
  * Variables available:
  * - $view: The view object
  * - $field: The field handler object that can process the input
  * - $row: The raw SQL result that can be used
  * - $output: The processed output that will normally be used.
  *
  * When fetching output from the $row, this construct should be used:
  * $data = $row->{$field->field_alias}
  *
  * The above will guarantee that you'll always get the correct data,
  * regardless of any changes in the aliasing that might happen if
  * the view is modified.
  */
?>
<div id="order-status-div-<?php print $row->commerce_line_item_field_data_commerce_line_items_line_item_; ?>">
<?php
global $user;

// get subscription status from a12_product_subscriptions_history table
// show status as per the status value in database table
$qry_get_history_status = db_query("SELECT status FROM {a12_product_subscriptions_history} 
																		WHERE uid = :uid AND product_id = :product_id 
																		ORDER BY a12_product_subscriptions_history_id DESC 
																		LIMIT 1 ",
																	 array(	':uid' => $user->uid, 
																	 				':product_id' => $row->commerce_product_field_data_commerce_product_product_id
																	 			)
																	 );

if ($qry_get_history_status -> rowCount() > 0){ 
	$data_get_history_status = $qry_get_history_status -> fetchObject();
	
	switch($data_get_history_status->status) {
		
		/*********  status = OFFLINE ********/
		case 0: 
			$output = "<span>Offline </span>";
			
			$output .= "<span>(";
			$output .= l("Make Online",'my_subscriptions_order_status/ajax/'.$row->order_id.'/'.$row->commerce_line_item_field_data_commerce_line_items_line_item_.'/1' , array('attributes' => array('class' => array('use-ajax'))) );
			$output .= ")</span>";
			$output .= " <div> " . l('Cancel My Subscription' , 'my_subscriptions_order_delete_commerce_line_item_reference/'.$row->order_id.'/'.$row->commerce_line_item_field_data_commerce_line_items_line_item_, array('query' => array('op'=>'delete', 'destination' => 'user/'.$user->uid.'/my_subscriptions'))) . '</div>';
			break;
		
		/********* status = ONLINE *********/
		case 1: 
			$output = "<span>Online </span>";
			
			$output .= "<span>(";
			$output .= l("Make Offline",'my_subscriptions_order_status/ajax/'.$row->order_id.'/'.$row->commerce_line_item_field_data_commerce_line_items_line_item_.'/0' , array('attributes' => array('class' => array('use-ajax'))) );
			$output .= ")</span";
			break;
		
		/********* status = CANCELLED ********/
		case 3: 
			$output = 'Cancelled';
			break;
		
		/********* status = UPGRADED ********/
		case 4: 
			$output = 'Upgraded';
			break;
		
		/********* status = BLOCKED ********/
		case 5: 
			$output = 'Blocked';
			break;
		
		/********* status = EXPIRED ********/
		case 6: 
			$output = 'Expired';
			break;
	}
}
	// print output
	print $output;

?>
</div>
