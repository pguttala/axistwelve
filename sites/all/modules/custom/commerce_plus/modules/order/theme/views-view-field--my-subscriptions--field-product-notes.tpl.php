<?php
 /**
  * This template is used to print a single field in a view. It is not
  * actually used in default Views, as this is registered as a theme
  * function which has better performance. For single overrides, the
  * template is perfectly okay.
  *
  * Variables available:
  * - $view: The view object
  * - $field: The field handler object that can process the input
  * - $row: The raw SQL result that can be used
  * - $output: The processed output that will normally be used.
  *
  * When fetching output from the $row, this construct should be used:
  * $data = $row->{$field->field_alias}
  *
  * The above will guarantee that you'll always get the correct data,
  * regardless of any changes in the aliasing that might happen if
  * the view is modified.
  */
?>

<?php
			/*
			 * 1.	within expiry massage shows like -
			 *    Remaining days of subscription
			 * 		Use the upgrade link at any time ensure you can keep using the service. "
			 * 
			 * 2.	Expiry date is over/finished
			 * 		" Your Subscription has already expired. "
			 * 
			 * 3.	If not a trial version
			 * 		display note from 'Product Subscription Notes' field from product info.
			 * 
			 * 4. If Sunscription 'Status' is is CANCELLED, show message 'Cancelled on ..date..'
			 * 
			 * 5. If Sunscription 'Status' is is UPGRADED, show message 'Upgraded on ..date..'
			 * 
			 * 6. If Sunscription 'Status' is is BLOCKED, show message 'Blocked on ..date..'
			 * 
			 * 7. If Sunscription 'Status' is is EXPIRED, show message 'Expired on ..date..'
			 */
?>

<?php
global $user;

/***************** START: SET FLAG FOR CONTACT BUTTONS DISPLAY ****************/

  // flag variable to set display of 'Contact Us' & 'Buy Subscription' buttons
  $display_contact = 0;
  
  // check if user has any other subscription with his account
  // get if any other subscription exists in 'a12_product_subscriptions_history' table for the current user
  $qry_get_other_subscription_details = db_query("	SELECT * FROM a12_product_subscriptions_history
  																									WHERE uid = :uid AND product_id != :product_id
  																									ORDER BY a12_product_subscriptions_history_id DESC
  																									LIMIT 1",
  																								array(	':uid' => $user->uid ,
  																											 	':product_id' => $row->commerce_product_field_data_commerce_product_product_id )
  																							);

  	$data_get_other_subscription_details = $qry_get_other_subscription_details -> fetchObject();
  	
  	// if other subscription exist display contact buttons as per condtion
  	if( $qry_get_other_subscription_details -> rowCount() > 0 ) {
  		// if other subscription is either online / offline do not display contact buttons
  		if($data_get_other_subscription_details -> status == 1 || $data_get_other_subscription_details -> status == 0) {
				$display_contact = 0;
			}
			// if other subscription is either cancelled / blocked / expired display contact buttons
			else {
				// if current subscription (current subscription product in table) has upgrade version or not
				if(product_upgrade_status($row->commerce_line_item_field_data_commerce_line_items_line_item_) == 1) {
					$display_contact = 0;
				}
				else $display_contact = 1;
			}
			
		}
		// if other subscription does not exist display contact buttons
		else $display_contact = 1;
		
/***************** END: SET FLAG FOR CONTACT BUTTONS DISPLAY ****************/

// get subscription status from a12_product_subscriptions_history table
// if status is UPGRADED, show upgraded message
// else if its not either UPGRADED or EXPIRED display & if product is haivng upgrade varsion available show 'UPGRADE NOW' button
$qry_get_history_status = db_query("SELECT status, created, expiry FROM {a12_product_subscriptions_history} 
																		WHERE uid = :uid AND product_id = :product_id 
																		ORDER BY a12_product_subscriptions_history_id DESC 
																		LIMIT 1 ",
																	 array(	':uid' => $user->uid, 
																	 				':product_id' => $row->commerce_product_field_data_commerce_product_product_id
																	 			)
																	 );

	if ($qry_get_history_status -> rowCount() >0) { 
		$data_get_history_status = $qry_get_history_status -> fetchObject();
		
		switch($data_get_history_status->status) {
			case 0;
			case 1:
				if($row -> field_field_trial_version[0]['raw']['value'] == 1) {
			    $current_timestamp = time();  // current date timestamp
			    $subscription_expire_timestamp = $data_get_history_status->expiry; // expire date timestamp
			    
			    // calculate remainng days of subscription
			    $diff_total_timestamp = ($subscription_expire_timestamp - $current_timestamp);
			    $diff_days = floor( ($diff_total_timestamp) / (24*60*60) );
			    
			    if( $diff_days < 1 ) {
			      $output = 'Your Subscription has already expired';
			    }
			    else {
			      $output = "<div class='subscription_remaining_days'>". $diff_days ." days";
			      $output .= " of trial remaining";
			            
			      // display upgrade link if product can be upgrade
			      $line_item = commerce_line_item_load($row->commerce_line_item_field_data_commerce_line_items_line_item_);
			      
			      if(product_upgrade_status($row->commerce_line_item_field_data_commerce_line_items_line_item_) == 1) {
			      	//$output .= "<div class='unite_submit'>" . l('Upgrade now','product_store/'.$line_item->line_item_label.'/details',array('query' => array('order_id' => $row->order_id, 'line_item_id' => $row->commerce_line_item_field_data_commerce_line_items_line_item_))) . "</div>";
			        $output .= "<div class='unite_submit'>" . l('Upgrade now','product_store/A12S-A-01/details',array('query' => array('order_id' => $row->order_id, 'line_item_id' => $row->commerce_line_item_field_data_commerce_line_items_line_item_))) . "</div>";
			
			      }
			      
			    }
			  }
				break;
				
			case 3:
				$output = 'Cancelled on '. date('d-m-Y' , $data_get_history_status->created);
				if($display_contact == 1) {
					$output .= "<div class='unite_submit'>" . l('Contact Us', 'contact',  array('attributes' => array('target'=>'_blank'))) . "</div>";
				}				
				break;
				
			case 4:
				$output = 'Upgraded on '. date('d-m-Y' , $data_get_history_status->created);
				break;
				
			case 5:
				$output = 'Blocked on '. date('d-m-Y' , $data_get_history_status->created);
				if($display_contact == 1) {
					$output .= "<div class='unite_submit'>" . l('Contact Us', 'contact',  array('attributes' => array('target'=>'_blank'))) . "</div>";
				}				
				break;
				
			case 6:
				$output = 'Expired on '. date('d-m-Y' , $data_get_history_status->created);
				if($display_contact == 1) {
					$output .= "<div class='unite_submit'>" . l('Contact Us', 'contact',  array('attributes' => array('target'=>'_blank'))) . "</div>";
					
					// show 'Buy Subscription' button to only trial version
					if(product_upgrade_status($row->commerce_line_item_field_data_commerce_line_items_line_item_) == 1) {
						$output .= "<div class='unite_submit'>" . l('Buy Subscription','product_store/A12S-A-01/details',array('query' => array('order_id' => $row->order_id, 'line_item_id' => $row->commerce_line_item_field_data_commerce_line_items_line_item_))) . "</div>";
					}
				}				
				break;
		}
		
	}// EOF $qry_get_history_status -> rowCount() >0

  // print output content
  print $output;
?>