<?php
// $Id: commerce_plus_order.rules.inc,v 1.0 2011/06/24 12:09:44 karthik Exp $

/**
 * Implementation of hook_rules_action_info().
 */
function commerce_plus_order_rules_action_info() {
  $actions['commerce_plus_order_log_subscriptions_status'] = array(
    'label' => t('Log the subscription status'),
    'parameter' => array(
      'order' => array(
        'type' => 'commerce_order',
        'label' => t('Order'),
      ),      
    ),
    'group' => t('Product Subscriptions'),
    'callbacks' => array(
      'execute' => 'commerce_plus_order_rules_log_subscriptions_status',
    ),
  );
  return $actions;
}

/**
 * Rules action: Logs the current status of the subscriptions as well as the user, timestamp and 
 *   subscription ID.
 */
function commerce_plus_order_rules_log_subscriptions_status($order) {
  global $user;
  if($order) {
    $subscription_history = array();
    $subscription_history['uid'] = $user->uid;
    $subscription_history['created'] = time(); 
    $subscription_history['status'] = ACTIVE;   
    foreach($order->commerce_line_items['und'] as $key => $line_item) {
      $line_item = commerce_line_item_load($line_item['line_item_id']);
      $product_id = $line_item->commerce_product['und'][0]['product_id'];
      $subscription_history['product_id'] = $product_id;
      drupal_write_record('a12_product_subscriptions_history', $subscription_history);
      
      //Update expiry date for newly entered record
      // 30 days in unix timestamp 2592000;
      $new_extended_expiry_date = _get_max_expiry_date($subscription_history['uid']);
      //drupal_set_message('<pre>'.print_r($new_extended_expiry_date,1).'</pre>');
      $sql_update_expiry_status = "UPDATE {a12_product_subscriptions_history} SET expiry = :expiry  WHERE a12_product_subscriptions_history_id = :a12_product_subscriptions_history_id";
      $result_update_expiry_status = db_query($sql_update_expiry_status, array(':expiry' => $new_extended_expiry_date,':a12_product_subscriptions_history_id' => $subscription_history['a12_product_subscriptions_history_id'] ));
      
    }  
  }  
}
