<?php

/**
 * Views for the My Subscriptions.
 */

/**
 * Implements hook_views_default_views().
 */
function commerce_plus_order_views_default_views() {
  $views = array();

  // new view My Subscriptions
	$view = new view;
	$view->name = 'my_subscriptions';
	$view->description = '';
	$view->tag = 'default';
	$view->base_table = 'commerce_order';
	$view->human_name = 'My Subscriptions';
	$view->core = 7;
	$view->api_version = '3.0';
	$view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */
	
	/* Display: Master */
	$handler = $view->new_display('default', 'Master', 'default');
	$handler->display->display_options['title'] = 'Subscriptions';
	$handler->display->display_options['use_ajax'] = TRUE;
	$handler->display->display_options['access']['type'] = 'none';
	$handler->display->display_options['cache']['type'] = 'none';
	$handler->display->display_options['query']['type'] = 'views_query';
	$handler->display->display_options['query']['options']['query_comment'] = FALSE;
	$handler->display->display_options['exposed_form']['type'] = 'basic';
	$handler->display->display_options['pager']['type'] = 'full';
	$handler->display->display_options['pager']['options']['items_per_page'] = '10';
	$handler->display->display_options['style_plugin'] = 'table';
	/* Header: Global: Text area */
	$handler->display->display_options['header']['area']['id'] = 'area';
	$handler->display->display_options['header']['area']['table'] = 'views';
	$handler->display->display_options['header']['area']['field'] = 'area';
	$handler->display->display_options['header']['area']['empty'] = FALSE;
	$handler->display->display_options['header']['area']['content'] = '<?php
	global $base_url;
	$breadcrumb[] = l(\'Home\', null);
	$breadcrumb[] .= l(\'My Account\', \'user\');
	$breadcrumb[] .= l(drupal_get_title(),$_GET[\'q\']);
	drupal_set_breadcrumb($breadcrumb);
	?>';
	$handler->display->display_options['header']['area']['format'] = 'php_code';
	$handler->display->display_options['header']['area']['tokenize'] = 0;
	/* No results behavior: Global: Text area */
	$handler->display->display_options['empty']['area']['id'] = 'area';
	$handler->display->display_options['empty']['area']['table'] = 'views';
	$handler->display->display_options['empty']['area']['field'] = 'area';
	$handler->display->display_options['empty']['area']['empty'] = FALSE;
	$handler->display->display_options['empty']['area']['content'] = 'You currently have no subscriptions.
	</br>
	<a href=\'/axistwelve/product_store\'>View and Subscribe for Product</a>';
	$handler->display->display_options['empty']['area']['format'] = 'full_html';
	$handler->display->display_options['empty']['area']['tokenize'] = 0;
	/* Relationship: Commerce Order: Referenced line item */
	$handler->display->display_options['relationships']['commerce_line_items_line_item_id']['id'] = 'commerce_line_items_line_item_id';
	$handler->display->display_options['relationships']['commerce_line_items_line_item_id']['table'] = 'field_data_commerce_line_items';
	$handler->display->display_options['relationships']['commerce_line_items_line_item_id']['field'] = 'commerce_line_items_line_item_id';
	$handler->display->display_options['relationships']['commerce_line_items_line_item_id']['required'] = 0;
	/* Relationship: Commerce Line item: Referenced product */
	$handler->display->display_options['relationships']['commerce_product_product_id']['id'] = 'commerce_product_product_id';
	$handler->display->display_options['relationships']['commerce_product_product_id']['table'] = 'field_data_commerce_product';
	$handler->display->display_options['relationships']['commerce_product_product_id']['field'] = 'commerce_product_product_id';
	$handler->display->display_options['relationships']['commerce_product_product_id']['relationship'] = 'commerce_line_items_line_item_id';
	$handler->display->display_options['relationships']['commerce_product_product_id']['required'] = 0;
	/* Field: Commerce Order: Order ID */
	$handler->display->display_options['fields']['order_id']['id'] = 'order_id';
	$handler->display->display_options['fields']['order_id']['table'] = 'commerce_order';
	$handler->display->display_options['fields']['order_id']['field'] = 'order_id';
	$handler->display->display_options['fields']['order_id']['exclude'] = TRUE;
	$handler->display->display_options['fields']['order_id']['alter']['alter_text'] = 0;
	$handler->display->display_options['fields']['order_id']['alter']['make_link'] = 0;
	$handler->display->display_options['fields']['order_id']['alter']['absolute'] = 0;
	$handler->display->display_options['fields']['order_id']['alter']['external'] = 0;
	$handler->display->display_options['fields']['order_id']['alter']['replace_spaces'] = 0;
	$handler->display->display_options['fields']['order_id']['alter']['nl2br'] = 0;
	$handler->display->display_options['fields']['order_id']['alter']['word_boundary'] = 1;
	$handler->display->display_options['fields']['order_id']['alter']['ellipsis'] = 1;
	$handler->display->display_options['fields']['order_id']['alter']['strip_tags'] = 0;
	$handler->display->display_options['fields']['order_id']['alter']['trim'] = 0;
	$handler->display->display_options['fields']['order_id']['alter']['html'] = 0;
	$handler->display->display_options['fields']['order_id']['element_label_colon'] = 1;
	$handler->display->display_options['fields']['order_id']['element_default_classes'] = 1;
	$handler->display->display_options['fields']['order_id']['hide_empty'] = 0;
	$handler->display->display_options['fields']['order_id']['empty_zero'] = 0;
	/* Field: Commerce Line Item: Line item ID */
	$handler->display->display_options['fields']['line_item_id']['id'] = 'line_item_id';
	$handler->display->display_options['fields']['line_item_id']['table'] = 'commerce_line_item';
	$handler->display->display_options['fields']['line_item_id']['field'] = 'line_item_id';
	$handler->display->display_options['fields']['line_item_id']['relationship'] = 'commerce_line_items_line_item_id';
	$handler->display->display_options['fields']['line_item_id']['label'] = '';
	$handler->display->display_options['fields']['line_item_id']['exclude'] = TRUE;
	$handler->display->display_options['fields']['line_item_id']['alter']['alter_text'] = 0;
	$handler->display->display_options['fields']['line_item_id']['alter']['make_link'] = 0;
	$handler->display->display_options['fields']['line_item_id']['alter']['absolute'] = 0;
	$handler->display->display_options['fields']['line_item_id']['alter']['external'] = 0;
	$handler->display->display_options['fields']['line_item_id']['alter']['replace_spaces'] = 0;
	$handler->display->display_options['fields']['line_item_id']['alter']['nl2br'] = 0;
	$handler->display->display_options['fields']['line_item_id']['alter']['word_boundary'] = 1;
	$handler->display->display_options['fields']['line_item_id']['alter']['ellipsis'] = 1;
	$handler->display->display_options['fields']['line_item_id']['alter']['strip_tags'] = 0;
	$handler->display->display_options['fields']['line_item_id']['alter']['trim'] = 0;
	$handler->display->display_options['fields']['line_item_id']['alter']['html'] = 0;
	$handler->display->display_options['fields']['line_item_id']['element_label_colon'] = FALSE;
	$handler->display->display_options['fields']['line_item_id']['element_default_classes'] = 1;
	$handler->display->display_options['fields']['line_item_id']['hide_empty'] = 0;
	$handler->display->display_options['fields']['line_item_id']['empty_zero'] = 0;
	/* Field: Commerce Product: Type */
	$handler->display->display_options['fields']['type']['id'] = 'type';
	$handler->display->display_options['fields']['type']['table'] = 'commerce_product';
	$handler->display->display_options['fields']['type']['field'] = 'type';
	$handler->display->display_options['fields']['type']['relationship'] = 'commerce_product_product_id';
	$handler->display->display_options['fields']['type']['exclude'] = TRUE;
	$handler->display->display_options['fields']['type']['alter']['alter_text'] = 0;
	$handler->display->display_options['fields']['type']['alter']['make_link'] = 0;
	$handler->display->display_options['fields']['type']['alter']['absolute'] = 0;
	$handler->display->display_options['fields']['type']['alter']['external'] = 0;
	$handler->display->display_options['fields']['type']['alter']['replace_spaces'] = 0;
	$handler->display->display_options['fields']['type']['alter']['nl2br'] = 0;
	$handler->display->display_options['fields']['type']['alter']['word_boundary'] = 1;
	$handler->display->display_options['fields']['type']['alter']['ellipsis'] = 1;
	$handler->display->display_options['fields']['type']['alter']['strip_tags'] = 0;
	$handler->display->display_options['fields']['type']['alter']['trim'] = 0;
	$handler->display->display_options['fields']['type']['alter']['html'] = 0;
	$handler->display->display_options['fields']['type']['element_label_colon'] = 1;
	$handler->display->display_options['fields']['type']['element_default_classes'] = 1;
	$handler->display->display_options['fields']['type']['hide_empty'] = 0;
	$handler->display->display_options['fields']['type']['empty_zero'] = 0;
	$handler->display->display_options['fields']['type']['link_to_product'] = 0;
	/* Field: Commerce Product: Title */
	$handler->display->display_options['fields']['title']['id'] = 'title';
	$handler->display->display_options['fields']['title']['table'] = 'commerce_product';
	$handler->display->display_options['fields']['title']['field'] = 'title';
	$handler->display->display_options['fields']['title']['relationship'] = 'commerce_product_product_id';
	$handler->display->display_options['fields']['title']['label'] = 'Product';
	$handler->display->display_options['fields']['title']['alter']['alter_text'] = 1;
	$handler->display->display_options['fields']['title']['alter']['text'] = '[type] - [title]';
	$handler->display->display_options['fields']['title']['alter']['make_link'] = 0;
	$handler->display->display_options['fields']['title']['alter']['absolute'] = 0;
	$handler->display->display_options['fields']['title']['alter']['external'] = 0;
	$handler->display->display_options['fields']['title']['alter']['replace_spaces'] = 0;
	$handler->display->display_options['fields']['title']['alter']['nl2br'] = 0;
	$handler->display->display_options['fields']['title']['alter']['word_boundary'] = 1;
	$handler->display->display_options['fields']['title']['alter']['ellipsis'] = 1;
	$handler->display->display_options['fields']['title']['alter']['strip_tags'] = 0;
	$handler->display->display_options['fields']['title']['alter']['trim'] = 0;
	$handler->display->display_options['fields']['title']['alter']['html'] = 0;
	$handler->display->display_options['fields']['title']['element_label_colon'] = 1;
	$handler->display->display_options['fields']['title']['element_default_classes'] = 1;
	$handler->display->display_options['fields']['title']['hide_empty'] = 0;
	$handler->display->display_options['fields']['title']['empty_zero'] = 0;
	$handler->display->display_options['fields']['title']['link_to_product'] = 0;
	/* Field: Commerce Order: Order state */
	$handler->display->display_options['fields']['state']['id'] = 'state';
	$handler->display->display_options['fields']['state']['table'] = 'commerce_order';
	$handler->display->display_options['fields']['state']['field'] = 'state';
	$handler->display->display_options['fields']['state']['label'] = 'Status';
	$handler->display->display_options['fields']['state']['alter']['alter_text'] = 0;
	$handler->display->display_options['fields']['state']['alter']['make_link'] = 0;
	$handler->display->display_options['fields']['state']['alter']['absolute'] = 0;
	$handler->display->display_options['fields']['state']['alter']['external'] = 0;
	$handler->display->display_options['fields']['state']['alter']['replace_spaces'] = 0;
	$handler->display->display_options['fields']['state']['alter']['nl2br'] = 0;
	$handler->display->display_options['fields']['state']['alter']['word_boundary'] = 1;
	$handler->display->display_options['fields']['state']['alter']['ellipsis'] = 1;
	$handler->display->display_options['fields']['state']['alter']['strip_tags'] = 0;
	$handler->display->display_options['fields']['state']['alter']['trim'] = 0;
	$handler->display->display_options['fields']['state']['alter']['html'] = 0;
	$handler->display->display_options['fields']['state']['element_label_colon'] = 1;
	$handler->display->display_options['fields']['state']['element_default_classes'] = 1;
	$handler->display->display_options['fields']['state']['hide_empty'] = 0;
	$handler->display->display_options['fields']['state']['empty_zero'] = 0;
	/* Field: Commerce Order: Link */
	$handler->display->display_options['fields']['view_order']['id'] = 'view_order';
	$handler->display->display_options['fields']['view_order']['table'] = 'commerce_order';
	$handler->display->display_options['fields']['view_order']['field'] = 'view_order';
	$handler->display->display_options['fields']['view_order']['exclude'] = TRUE;
	$handler->display->display_options['fields']['view_order']['alter']['alter_text'] = 0;
	$handler->display->display_options['fields']['view_order']['alter']['make_link'] = 0;
	$handler->display->display_options['fields']['view_order']['alter']['absolute'] = 0;
	$handler->display->display_options['fields']['view_order']['alter']['external'] = 0;
	$handler->display->display_options['fields']['view_order']['alter']['replace_spaces'] = 0;
	$handler->display->display_options['fields']['view_order']['alter']['nl2br'] = 0;
	$handler->display->display_options['fields']['view_order']['alter']['word_boundary'] = 1;
	$handler->display->display_options['fields']['view_order']['alter']['ellipsis'] = 1;
	$handler->display->display_options['fields']['view_order']['alter']['strip_tags'] = 0;
	$handler->display->display_options['fields']['view_order']['alter']['trim'] = 0;
	$handler->display->display_options['fields']['view_order']['alter']['html'] = 0;
	$handler->display->display_options['fields']['view_order']['element_label_colon'] = 1;
	$handler->display->display_options['fields']['view_order']['element_default_classes'] = 1;
	$handler->display->display_options['fields']['view_order']['hide_empty'] = 0;
	$handler->display->display_options['fields']['view_order']['empty_zero'] = 0;
	$handler->display->display_options['fields']['view_order']['text'] = 'Usage';
	/* Field: Commerce Order: Link */
	$handler->display->display_options['fields']['view_order_1']['id'] = 'view_order_1';
	$handler->display->display_options['fields']['view_order_1']['table'] = 'commerce_order';
	$handler->display->display_options['fields']['view_order_1']['field'] = 'view_order';
	$handler->display->display_options['fields']['view_order_1']['exclude'] = TRUE;
	$handler->display->display_options['fields']['view_order_1']['alter']['alter_text'] = 0;
	$handler->display->display_options['fields']['view_order_1']['alter']['make_link'] = 0;
	$handler->display->display_options['fields']['view_order_1']['alter']['absolute'] = 0;
	$handler->display->display_options['fields']['view_order_1']['alter']['external'] = 0;
	$handler->display->display_options['fields']['view_order_1']['alter']['replace_spaces'] = 0;
	$handler->display->display_options['fields']['view_order_1']['alter']['nl2br'] = 0;
	$handler->display->display_options['fields']['view_order_1']['alter']['word_boundary'] = 1;
	$handler->display->display_options['fields']['view_order_1']['alter']['ellipsis'] = 1;
	$handler->display->display_options['fields']['view_order_1']['alter']['strip_tags'] = 0;
	$handler->display->display_options['fields']['view_order_1']['alter']['trim'] = 0;
	$handler->display->display_options['fields']['view_order_1']['alter']['html'] = 0;
	$handler->display->display_options['fields']['view_order_1']['element_label_colon'] = 1;
	$handler->display->display_options['fields']['view_order_1']['element_default_classes'] = 1;
	$handler->display->display_options['fields']['view_order_1']['hide_empty'] = 0;
	$handler->display->display_options['fields']['view_order_1']['empty_zero'] = 0;
	$handler->display->display_options['fields']['view_order_1']['text'] = 'Billing';
	/* Field: Commerce Product: Has Configure */
	$handler->display->display_options['fields']['field_has_configure']['id'] = 'field_has_configure';
	$handler->display->display_options['fields']['field_has_configure']['table'] = 'field_data_field_has_configure';
	$handler->display->display_options['fields']['field_has_configure']['field'] = 'field_has_configure';
	$handler->display->display_options['fields']['field_has_configure']['relationship'] = 'commerce_product_product_id';
	$handler->display->display_options['fields']['field_has_configure']['exclude'] = TRUE;
	$handler->display->display_options['fields']['field_has_configure']['alter']['alter_text'] = 0;
	$handler->display->display_options['fields']['field_has_configure']['alter']['make_link'] = 0;
	$handler->display->display_options['fields']['field_has_configure']['alter']['absolute'] = 0;
	$handler->display->display_options['fields']['field_has_configure']['alter']['external'] = 0;
	$handler->display->display_options['fields']['field_has_configure']['alter']['replace_spaces'] = 0;
	$handler->display->display_options['fields']['field_has_configure']['alter']['nl2br'] = 0;
	$handler->display->display_options['fields']['field_has_configure']['alter']['word_boundary'] = 1;
	$handler->display->display_options['fields']['field_has_configure']['alter']['ellipsis'] = 1;
	$handler->display->display_options['fields']['field_has_configure']['alter']['strip_tags'] = 0;
	$handler->display->display_options['fields']['field_has_configure']['alter']['trim'] = 0;
	$handler->display->display_options['fields']['field_has_configure']['alter']['html'] = 0;
	$handler->display->display_options['fields']['field_has_configure']['element_label_colon'] = 1;
	$handler->display->display_options['fields']['field_has_configure']['element_default_classes'] = 1;
	$handler->display->display_options['fields']['field_has_configure']['hide_empty'] = 0;
	$handler->display->display_options['fields']['field_has_configure']['empty_zero'] = 0;
	$handler->display->display_options['fields']['field_has_configure']['field_api_classes'] = 0;
	/* Field: Commerce Order: Operations links */
	$handler->display->display_options['fields']['operations']['id'] = 'operations';
	$handler->display->display_options['fields']['operations']['table'] = 'commerce_order';
	$handler->display->display_options['fields']['operations']['field'] = 'operations';
	$handler->display->display_options['fields']['operations']['label'] = 'Manage';
	$handler->display->display_options['fields']['operations']['alter']['alter_text'] = 1;
	$handler->display->display_options['fields']['operations']['alter']['text'] = '[view_order] | [view_order_1]';
	$handler->display->display_options['fields']['operations']['alter']['make_link'] = 0;
	$handler->display->display_options['fields']['operations']['alter']['absolute'] = 0;
	$handler->display->display_options['fields']['operations']['alter']['external'] = 0;
	$handler->display->display_options['fields']['operations']['alter']['replace_spaces'] = 0;
	$handler->display->display_options['fields']['operations']['alter']['nl2br'] = 0;
	$handler->display->display_options['fields']['operations']['alter']['word_boundary'] = 1;
	$handler->display->display_options['fields']['operations']['alter']['ellipsis'] = 1;
	$handler->display->display_options['fields']['operations']['alter']['strip_tags'] = 0;
	$handler->display->display_options['fields']['operations']['alter']['trim'] = 0;
	$handler->display->display_options['fields']['operations']['alter']['html'] = 0;
	$handler->display->display_options['fields']['operations']['element_label_colon'] = 1;
	$handler->display->display_options['fields']['operations']['element_default_classes'] = 1;
	$handler->display->display_options['fields']['operations']['hide_empty'] = 0;
	$handler->display->display_options['fields']['operations']['empty_zero'] = 0;
	/* Field: Commerce Product: Trial Version of Product Subscription  */
	$handler->display->display_options['fields']['field_trial_version']['id'] = 'field_trial_version';
	$handler->display->display_options['fields']['field_trial_version']['table'] = 'field_data_field_trial_version';
	$handler->display->display_options['fields']['field_trial_version']['field'] = 'field_trial_version';
	$handler->display->display_options['fields']['field_trial_version']['relationship'] = 'commerce_product_product_id';
	$handler->display->display_options['fields']['field_trial_version']['exclude'] = TRUE;
	$handler->display->display_options['fields']['field_trial_version']['alter']['alter_text'] = 0;
	$handler->display->display_options['fields']['field_trial_version']['alter']['make_link'] = 0;
	$handler->display->display_options['fields']['field_trial_version']['alter']['absolute'] = 0;
	$handler->display->display_options['fields']['field_trial_version']['alter']['external'] = 0;
	$handler->display->display_options['fields']['field_trial_version']['alter']['replace_spaces'] = 0;
	$handler->display->display_options['fields']['field_trial_version']['alter']['nl2br'] = 0;
	$handler->display->display_options['fields']['field_trial_version']['alter']['word_boundary'] = 1;
	$handler->display->display_options['fields']['field_trial_version']['alter']['ellipsis'] = 1;
	$handler->display->display_options['fields']['field_trial_version']['alter']['strip_tags'] = 0;
	$handler->display->display_options['fields']['field_trial_version']['alter']['trim'] = 0;
	$handler->display->display_options['fields']['field_trial_version']['alter']['html'] = 0;
	$handler->display->display_options['fields']['field_trial_version']['element_label_colon'] = 1;
	$handler->display->display_options['fields']['field_trial_version']['element_default_classes'] = 1;
	$handler->display->display_options['fields']['field_trial_version']['hide_empty'] = 0;
	$handler->display->display_options['fields']['field_trial_version']['empty_zero'] = 0;
	$handler->display->display_options['fields']['field_trial_version']['field_api_classes'] = 0;
	/* Field: Commerce Product: Trial Duration (in days) */
	$handler->display->display_options['fields']['field_trial_duration']['id'] = 'field_trial_duration';
	$handler->display->display_options['fields']['field_trial_duration']['table'] = 'field_data_field_trial_duration';
	$handler->display->display_options['fields']['field_trial_duration']['field'] = 'field_trial_duration';
	$handler->display->display_options['fields']['field_trial_duration']['relationship'] = 'commerce_product_product_id';
	$handler->display->display_options['fields']['field_trial_duration']['exclude'] = TRUE;
	$handler->display->display_options['fields']['field_trial_duration']['alter']['alter_text'] = 0;
	$handler->display->display_options['fields']['field_trial_duration']['alter']['make_link'] = 0;
	$handler->display->display_options['fields']['field_trial_duration']['alter']['absolute'] = 0;
	$handler->display->display_options['fields']['field_trial_duration']['alter']['external'] = 0;
	$handler->display->display_options['fields']['field_trial_duration']['alter']['replace_spaces'] = 0;
	$handler->display->display_options['fields']['field_trial_duration']['alter']['nl2br'] = 0;
	$handler->display->display_options['fields']['field_trial_duration']['alter']['word_boundary'] = 1;
	$handler->display->display_options['fields']['field_trial_duration']['alter']['ellipsis'] = 1;
	$handler->display->display_options['fields']['field_trial_duration']['alter']['strip_tags'] = 0;
	$handler->display->display_options['fields']['field_trial_duration']['alter']['trim'] = 0;
	$handler->display->display_options['fields']['field_trial_duration']['alter']['html'] = 0;
	$handler->display->display_options['fields']['field_trial_duration']['element_label_colon'] = 0;
	$handler->display->display_options['fields']['field_trial_duration']['element_default_classes'] = 1;
	$handler->display->display_options['fields']['field_trial_duration']['hide_empty'] = 0;
	$handler->display->display_options['fields']['field_trial_duration']['empty_zero'] = 0;
	$handler->display->display_options['fields']['field_trial_duration']['field_api_classes'] = 0;
	/* Field: Commerce Order: Created date */
	$handler->display->display_options['fields']['created']['id'] = 'created';
	$handler->display->display_options['fields']['created']['table'] = 'commerce_order';
	$handler->display->display_options['fields']['created']['field'] = 'created';
	$handler->display->display_options['fields']['created']['exclude'] = TRUE;
	$handler->display->display_options['fields']['created']['alter']['alter_text'] = 0;
	$handler->display->display_options['fields']['created']['alter']['make_link'] = 0;
	$handler->display->display_options['fields']['created']['alter']['absolute'] = 0;
	$handler->display->display_options['fields']['created']['alter']['external'] = 0;
	$handler->display->display_options['fields']['created']['alter']['replace_spaces'] = 0;
	$handler->display->display_options['fields']['created']['alter']['nl2br'] = 0;
	$handler->display->display_options['fields']['created']['alter']['word_boundary'] = 1;
	$handler->display->display_options['fields']['created']['alter']['ellipsis'] = 1;
	$handler->display->display_options['fields']['created']['alter']['strip_tags'] = 0;
	$handler->display->display_options['fields']['created']['alter']['trim'] = 0;
	$handler->display->display_options['fields']['created']['alter']['html'] = 0;
	$handler->display->display_options['fields']['created']['element_label_colon'] = 1;
	$handler->display->display_options['fields']['created']['element_default_classes'] = 1;
	$handler->display->display_options['fields']['created']['hide_empty'] = 0;
	$handler->display->display_options['fields']['created']['empty_zero'] = 0;
	$handler->display->display_options['fields']['created']['date_format'] = 'short';
	/* Field: Commerce Product: Product Subscription Notes */
	$handler->display->display_options['fields']['field_product_notes']['id'] = 'field_product_notes';
	$handler->display->display_options['fields']['field_product_notes']['table'] = 'field_data_field_product_notes';
	$handler->display->display_options['fields']['field_product_notes']['field'] = 'field_product_notes';
	$handler->display->display_options['fields']['field_product_notes']['relationship'] = 'commerce_product_product_id';
	$handler->display->display_options['fields']['field_product_notes']['label'] = 'Note';
	$handler->display->display_options['fields']['field_product_notes']['alter']['alter_text'] = 0;
	$handler->display->display_options['fields']['field_product_notes']['alter']['make_link'] = 0;
	$handler->display->display_options['fields']['field_product_notes']['alter']['absolute'] = 0;
	$handler->display->display_options['fields']['field_product_notes']['alter']['external'] = 0;
	$handler->display->display_options['fields']['field_product_notes']['alter']['replace_spaces'] = 0;
	$handler->display->display_options['fields']['field_product_notes']['alter']['nl2br'] = 0;
	$handler->display->display_options['fields']['field_product_notes']['alter']['word_boundary'] = 1;
	$handler->display->display_options['fields']['field_product_notes']['alter']['ellipsis'] = 1;
	$handler->display->display_options['fields']['field_product_notes']['alter']['strip_tags'] = 0;
	$handler->display->display_options['fields']['field_product_notes']['alter']['trim'] = 0;
	$handler->display->display_options['fields']['field_product_notes']['alter']['html'] = 0;
	$handler->display->display_options['fields']['field_product_notes']['element_label_colon'] = 1;
	$handler->display->display_options['fields']['field_product_notes']['element_default_classes'] = 1;
	$handler->display->display_options['fields']['field_product_notes']['hide_empty'] = 0;
	$handler->display->display_options['fields']['field_product_notes']['empty_zero'] = 0;
	$handler->display->display_options['fields']['field_product_notes']['field_api_classes'] = 0;
	/* Contextual filter: Commerce Order: Uid */
	$handler->display->display_options['arguments']['uid']['id'] = 'uid';
	$handler->display->display_options['arguments']['uid']['table'] = 'commerce_order';
	$handler->display->display_options['arguments']['uid']['field'] = 'uid';
	$handler->display->display_options['arguments']['uid']['default_action'] = 'default';
	$handler->display->display_options['arguments']['uid']['default_argument_type'] = 'current_user';
	$handler->display->display_options['arguments']['uid']['default_argument_skip_url'] = 0;
	$handler->display->display_options['arguments']['uid']['summary']['number_of_records'] = '0';
	$handler->display->display_options['arguments']['uid']['summary']['format'] = 'default_summary';
	$handler->display->display_options['arguments']['uid']['summary_options']['items_per_page'] = '25';
	$handler->display->display_options['arguments']['uid']['specify_validation'] = 1;
	$handler->display->display_options['arguments']['uid']['validate']['type'] = 'current_user_or_role';
	$handler->display->display_options['arguments']['uid']['validate_options']['restrict_roles'] = '1';
	$handler->display->display_options['arguments']['uid']['validate_options']['roles'] = array(
	  3 => '3',
	);
	$handler->display->display_options['arguments']['uid']['break_phrase'] = 0;
	$handler->display->display_options['arguments']['uid']['not'] = 0;
	/* Filter criterion: Commerce Order: Order state */
	$handler->display->display_options['filters']['state']['id'] = 'state';
	$handler->display->display_options['filters']['state']['table'] = 'commerce_order';
	$handler->display->display_options['filters']['state']['field'] = 'state';
	$handler->display->display_options['filters']['state']['value'] = array(
	  'pending' => 'pending',
	  'completed' => 'completed',
	);
	
	/* Display: Page */
	$handler = $view->new_display('page', 'Page', 'page');
	$handler->display->display_options['path'] = 'user/%/my_subscriptions';
	$handler->display->display_options['menu']['title'] = 'My Subscriptions';
	$handler->display->display_options['menu']['description'] = 'My subscription menu';
	$handler->display->display_options['menu']['weight'] = '0';
    $translatables['my_subscriptions'] = array(
    t('Master'),
    t('My Subscriptions'),
    t('more'),
    t('Apply'),
    t('Reset'),
    t('Sort by'),
    t('Asc'),
    t('Desc'),
    t('Items per page'),
    t('- All -'),
    t('Offset'),
    t('You currently have no subscriptions.
  </br>
  <a href=\'/axistwelve/product_store\'>View and Subscribe for Product</a>'),
    t('Line Item'),
    t('Product'),
    t('Order ID'),
    t('Type'),
    t('[type] - [title]'),
    t('Status'),
    t('Link'),
    t('Usage'),
    t('Billing'),
    t('Has Configure'),
    t('Manage'),
    t('[view_order] | [view_order_1]'),
    t('Trial Version of Product Subscription '),
    t('Trial Duration (in days)'),
    t('Created date'),
    t('Note'),
    t('All'),
    t('Page'),
  );


$views[$view->name] = $view;
  
  return $views;
}

