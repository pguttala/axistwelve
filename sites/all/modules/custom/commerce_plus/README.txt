
Welcome to Commerce Plus. 

	commerce plus module is designed to extend the functionality of drupal commerce module (core)

Installing Commerce Plus: 

Place the entirety of this directory in sites/all/modules/custom/commerce_plus
You must also install Drupal Commerce module (http://drupal.org/project/commerce) to use commerce plus. 

Navigate to administer >> module (admin/modules). Enable Commerce Plus and Commerce Plus Cart module.

Please update this file if changes occurs.  

