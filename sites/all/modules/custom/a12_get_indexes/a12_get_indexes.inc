<?php

/**
* Callback for creating activity resources.
*
* @param object $data
* @return object
*/
function _a12_get_indexes_retrieve($data) {
  global $user;
  $data = (object)$data;

  if (!isset($data->key)) {
    return services_error('Missing key identifier', 406);
  }
  
  $result = db_query("SELECT secret_key_id FROM {user_system_token} WHERE token_id=:key AND status=1", array(":key"=>$data->key))->fetchObject();

  if($result == null) {
    return (object)array('status' => 'error', 'error_code' => 'FND-603');
  } 
  else {
    $client = user_load($result->uid);
    $host = variable_get('A12_Find_Server', 'find.axis12.com');
    $port = variable_get('A12_Find_Port', 443);
    $mc   = variable_get('A12_Find_Multicore', 1);
    $path   = variable_get('A12_Find_Path', '/solr');
    $indexes = db_query("SELECT  *, '$host' AS host, '$port' AS port, 1 AS multicore, name AS path FROM {a12_indexes} AS a JOIN (SELECT * FROM {a12_map_indexes} WHERE secret_key_id=:key_id) AS b ON a.index_id=b.index_id WHERE status=1 ORDER BY is_default DESC, label ASC", array(":key_id" => $result->secret_key_id))->fetchAll();
    return (object)array(
      'uuid' => $client->field_a12_identifier['und'][0]['value'],
      'indexes' => $indexes,
      'status' => 'success',
    );
    
  }
  
  
  
  
}



