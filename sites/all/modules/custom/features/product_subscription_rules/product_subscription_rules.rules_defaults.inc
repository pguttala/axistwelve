<?php
/**
 * @file
 * product_subscription_rules.rules_defaults.inc
 */

/**
 * Implementation of hook_default_rules_configuration().
 */
function product_subscription_rules_default_rules_configuration() {
  $items = array();
  $items['rules_admin_order_notification_email'] = entity_import('rules_config', '{ "rules_admin_order_notification_email" : {
      "LABEL" : "Admin Send an order notification e-mail",
      "PLUGIN" : "reaction rule",
      "WEIGHT" : "4",
      "REQUIRES" : [ "rules", "commerce_checkout" ],
      "ON" : [ "commerce_checkout_complete" ],
      "DO" : [
        { "mail" : {
            "to" : [ "site:mail" ],
            "subject" : "Order [order:order-number] at [site:name]",
            "message" : "Thanks for your order [order:order-number] at [site:name].\\r\\n\\r\\nIf this is your first order with us, you will receive a separate e-mail with login instructions. You can view your order history with us at any time by logging into our website at:\\r\\n\\r\\n[site:login-url]\\r\\n\\r\\nYou can find the status of your current order at:\\r\\n\\r\\n[site:url]user\\/[order:uid]\\/orders\\/[order:order-id]\\r\\n\\r\\nPlease contact us if you have any questions about your order.",
            "from" : ""
          }
        }
      ]
    }
  }');
  $items['rules_test_rule_for_products'] = entity_import('rules_config', '{ "rules_test_rule_for_products" : {
      "LABEL" : "Recording the product subscriptions status on checkout completion",
      "PLUGIN" : "reaction rule",
      "REQUIRES" : [ "commerce_plus_order", "commerce_checkout" ],
      "ON" : [ "commerce_checkout_complete" ],
      "DO" : [
        { "commerce_plus_order_log_subscriptions_status" : { "order" : [ "order" ] } }
      ]
    }
  }');
  return $items;
}
