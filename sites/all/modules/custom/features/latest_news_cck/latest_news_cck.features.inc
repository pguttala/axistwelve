<?php
/**
 * @file
 * latest_news_cck.features.inc
 */

/**
 * Implements hook_node_info().
 */
function latest_news_cck_node_info() {
  $items = array(
    'latest_news' => array(
      'name' => t('Latest News'),
      'base' => 'node_content',
      'description' => t('Add latest news about Axis12'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  return $items;
}
