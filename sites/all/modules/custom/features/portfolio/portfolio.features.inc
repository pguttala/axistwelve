<?php
/**
 * @file
 * portfolio.features.inc
 */

/**
 * Implements hook_views_api().
 */
function portfolio_views_api() {
  list($module, $api) = func_get_args();
  if ($module == "views" && $api == "views_default") {
    return array("version" => "3.0");
  }
}

/**
 * Implements hook_image_default_styles().
 */
function portfolio_image_default_styles() {
  $styles = array();

  // Exported image style: portfolio_page_body
  $styles['portfolio_page_body'] = array(
    'name' => 'portfolio_page_body',
    'effects' => array(
      12 => array(
        'label' => 'Resize',
        'help' => 'Resizing will make images an exact set of dimensions. This may cause images to be stretched or shrunk disproportionately.',
        'effect callback' => 'image_resize_effect',
        'dimensions callback' => 'image_resize_dimensions',
        'form callback' => 'image_resize_form',
        'summary theme' => 'image_resize_summary',
        'module' => 'image',
        'name' => 'image_resize',
        'data' => array(
          'width' => '540',
          'height' => '365',
        ),
        'weight' => '-10',
      ),
      5 => array(
        'label' => 'Scale',
        'help' => 'Scaling will maintain the aspect-ratio of the original image. If only a single dimension is specified, the other dimension will be calculated.',
        'effect callback' => 'image_scale_effect',
        'dimensions callback' => 'image_scale_dimensions',
        'form callback' => 'image_scale_form',
        'summary theme' => 'image_scale_summary',
        'module' => 'image',
        'name' => 'image_scale',
        'data' => array(
          'width' => '560',
          'height' => '',
          'upscale' => 1,
        ),
        'weight' => '-9',
      ),
    ),
  );

  return $styles;
}
