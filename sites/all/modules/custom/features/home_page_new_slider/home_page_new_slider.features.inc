<?php
/**
 * @file
 * home_page_new_slider.features.inc
 */

/**
 * Implements hook_views_api().
 */
function home_page_new_slider_views_api() {
  list($module, $api) = func_get_args();
  if ($module == "views" && $api == "views_default") {
    return array("version" => "3.0");
  }
}
