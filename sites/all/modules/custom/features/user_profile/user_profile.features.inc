<?php
/**
 * @file
 * user_profile.features.inc
 */

/**
 * Implementation of hook_default_profile2_type().
 */
function user_profile_default_profile2_type() {
  $items = array();
  $items['personal_information'] = entity_import('profile2_type', '{
    "userCategory" : true,
    "userView" : true,
    "type" : "personal_information",
    "label" : "Billing Information",
    "weight" : "0",
    "data" : { "registration" : 0, "use_page" : 0 },
    "rdf_mapping" : []
  }');
  return $items;
}
