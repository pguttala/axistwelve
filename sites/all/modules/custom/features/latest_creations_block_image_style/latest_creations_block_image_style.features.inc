<?php
/**
 * @file
 * latest_creations_block_image_style.features.inc
 */

/**
 * Implements hook_image_default_styles().
 */
function latest_creations_block_image_style_image_default_styles() {
  $styles = array();

  // Exported image style: latest_creation_image
  $styles['latest_creation_image'] = array(
    'name' => 'latest_creation_image',
    'effects' => array(
      11 => array(
        'label' => 'Resize',
        'help' => 'Resizing will make images an exact set of dimensions. This may cause images to be stretched or shrunk disproportionately.',
        'effect callback' => 'image_resize_effect',
        'dimensions callback' => 'image_resize_dimensions',
        'form callback' => 'image_resize_form',
        'summary theme' => 'image_resize_summary',
        'module' => 'image',
        'name' => 'image_resize',
        'data' => array(
          'width' => '264',
          'height' => '184',
        ),
        'weight' => '1',
      ),
    ),
  );

  return $styles;
}
