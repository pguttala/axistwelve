<?php
/**
 * @file
 * feature_training_course.features.inc
 */

/**
 * Implementation of hook_node_info().
 */
function feature_training_course_node_info() {
  $items = array(
    'training_course' => array(
      'name' => t('Training Course'),
      'base' => 'node_content',
      'description' => t('This content type is basically use for adding training course information'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  return $items;
}
