<?php
/**
 * @file
 * our_latest_creations.features.inc
 */

/**
 * Implements hook_views_api().
 */
function our_latest_creations_views_api() {
  list($module, $api) = func_get_args();
  if ($module == "views" && $api == "views_default") {
    return array("version" => "3.0");
  }
}
