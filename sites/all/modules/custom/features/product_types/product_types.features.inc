<?php
/**
 * @file
 * product_types.features.inc
 */

/**
 * Implementation of hook_commerce_product_default_types().
 */
function product_types_commerce_product_default_types() {
  $items = array(
    'a12_search' => array(
      'type' => 'a12_search',
      'name' => 'A12 Search',
      'description' => 'Search service products by Axistwelve.',
      'help' => '',
      'module' => 'commerce_product_ui',
    ),
  );
  return $items;
}
