<?php 
// $Id: product_price_calculator.include.inc, v 1.01 2012/14/08 13:46:44 MKhan Exp $
/**
 * function to calculate a12 Find Price.
 */
function a12_find_price_calculator_form($form, &$form_state){
	drupal_set_title('A12 Find Price Calculator');
  // This calls only first time when page load and it is used to display the default calculated table.
  if(empty($form_state['rebuild'])) {
    a12_find_price_calculator_form_submit($form, &$form_state);
  }
  $form = array();

  $form['#redirect'] = FALSE;
  $form['price_calculator_search_title'] = array('#markup' => '<div class="price-cal-search-title">Search</div>');
  $form['no_of_daily_views'] = array(
    '#type' => 'transfer_slider',
    '#slider_title' => t('How many page views does your site receive each day?'),
    '#left_value' => 0,
    '#right_value' => 6,
    '#left' => t('Left'),
    '#right' => t('Right'),
    '#step_value' => 1,
    '#required' => TRUE,
    '#prefix' => '<div class="no-of-daily-views">',
    '#suffix' => '</div>',
  );
  $form['no_of_daily_requests'] = array(
    '#type' => 'transfer_slider',
    '#slider_title' => t('How many search requests are performed each day?'),
    '#left_value' => 0,
    '#right_value' => 6,
    '#left' => t('Join now'),
    '#right' => t('Right'),
    '#step_value' => 1,
    '#required' => TRUE,
    '#prefix' => '<div class="no-of-daily-requests">',
    '#suffix' => '</div>',
  );
  $options = array('No' => 'No', 'Yes' => 'Yes');
  $form['facets'] = array(
    '#type' => 'radios',
    '#options' => $options, //drupal_map_assoc(array(t('No'), t('Yes'))),
    '#title' => '<div class="facets-title">Will your search results includes "facets"?</br>(i.e filters to allow users to refine their search results)</div>',
    '#prefix' => '<div class="facets">',
    '#suffix' => '</div>',
  );
  $form['facets']['#default_value'] = 'No';
  $form['mlt'] = array(
    '#type' => 'radios',
    '#options' => $options, //drupal_map_assoc(array(t('No'), t('Yes'))),
    '#title' => '<div class="facets-title">Will you provide related content recommendations on your site? </br>(e.g. "More like this")</div>',
    '#prefix' => '<div class="mlt">',
    '#suffix' => '</div>',
  );
  $form['mlt']['#default_value'] = 'No';
  $form['price_calculator_indexing_title'] = array('#markup' => '<div class="price-cal-indexing-title">Indexing</div>');
  $form['no_of_daily_indexed_pages'] = array(
    '#type' => 'transfer_slider',
    '#slider_title' => t('How many pages of content will be added or changed on your site each day?'),
    '#left_value' => 0,
    '#right_value' => 6,
    '#left' => t('Left'),
    '#right' => t('Right'),
    '#step_value' => 1,
    '#required' => TRUE,
    '#prefix' => '<div class="no-of-daily-indexed-pages">',
    '#suffix' => '</div>',
  );
  $form['size_of_daily_indexed_pages'] = array(
    '#type' => 'transfer_slider',
    '#slider_title' => t('What is your average content page size (note: 2kb is roughly 2,000 characters of text)?'),
    '#left_value' => 0,
    '#right_value' => 6,
    '#left' => t('Join now'),
    '#right' => t('Right'),
    '#step_value' => 1,
    '#required' => TRUE,
    '#prefix' => '<div class="size-of-daily-indexed-pages">',
    '#suffix' => '</div>',
  );
 $form['no_of_daily_indexed_attachments'] = array(
    '#type' => 'transfer_slider',
    '#slider_title' => t('How many file attachments will be added or changed on your site each day?'),
    '#left_value' => 0,
    '#right_value' => 6,
    '#left' => t('Left'),
    '#right' => t('Right'),
    '#step_value' => 1,
    '#required' => TRUE,
    '#prefix' => '<div class="no-of-daily-indexed-attachments">',
    '#suffix' => '</div>',
  );
  $form['size_of_daily_indexed_attachments'] = array(
    '#type' => 'transfer_slider',
    '#slider_title' => t('What is your average file attachment size?'),
    '#left_value' => 0,
    '#right_value' => 6,
    '#left' => t('Join now'),
    '#right' => t('Right'),
    '#step_value' => 1,
    '#required' => TRUE,
    '#prefix' => '<div class="size-of-daily-indexed-attachments">',
    '#suffix' => '</div>',
  );
 $form['no_of_batch_per_day'] = array(
    '#type' => 'transfer_slider',
    '#slider_title' => t("How many times will you push new or updated content for re-indexing each day?"),
    '#left_value' => 0,
    '#right_value' => 6,
    '#left' => t('Left'),
    '#right' => t('Right'),
    '#step_value' => 1,
    '#required' => TRUE,
    '#prefix' => '<div class="no-of-batch-per-day">',
    '#suffix' => '</div>',
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Calculate',
  );

  // if form storage result is TRUE, display calculated table
  //if(isset($form_state['storage']['results'])) { //removed for default table on page load

    //  get rates  
    $from_ccy = 'USD';
    $to_ccy = 'GBP';
    $to_ccy2 = 'EUR';
  
    $url_ccy = 'http://finance.yahoo.com/d/quotes.csv?f=l1d1t1&s='.$from_ccy.$to_ccy.'=X';
    $handle_ccy = fopen($url_ccy, 'r');
 
    if ($handle_ccy) {
      $result = fgetcsv($handle_ccy);
      fclose($handle_ccy);
    }
 
    $USD_to_GBP = $result[0];
    $USD_to_GBP_asat = $result[1] .' at ' .$result[2];
  
    $url_ccy2 = 'http://finance.yahoo.com/d/quotes.csv?f=l1d1t1&s='.$from_ccy.$to_ccy2.'=X';
    $handle_ccy2 = fopen($url_ccy2, 'r');
 
    if ($handle_ccy2) {
      $result = fgetcsv($handle_ccy2);
      fclose($handle_ccy2);
    }
  
    $USD_to_EUR = $result[0];
    $USD_to_EUR_asat = $result[1] .' at ' .$result[2];

    // Value to display on calculated price table
    $no_of_batch_per_day = $form_state['storage']['results']['no_of_batch_per_day'];
    $size_of_daily_indexed_pages = $form_state['storage']['results']['size_of_daily_indexed_pages'];
    $cost_get_request = $form_state['storage']['results']['cost_get_request'];
    $get_request_unit_cost = $form_state['storage']['results']['get_request_unit_cost'];
    $no_of_daily_requests = $form_state['storage']['results']['no_of_daily_requests'];
    $cost_total = $form_state['storage']['results']['cost_total'];
    $cost_put_request = $form_state['storage']['results']['cost_put_request'];
    $put_request_unit_cost = $form_state['storage']['results']['put_request_unit_cost'];
    $get_data_billing_units = $form_state['storage']['results']['get_data_billing_units'];
    $get_data_unit_cost = $form_state['storage']['results']['get_data_unit_cost'];
    $cost_outbound_data_request = $form_state['storage']['results']['cost_outbound_data_request'];
    $put_data_billing_units = $form_state['storage']['results']['put_data_billing_units'];
    $cost_inbound_data_index = $form_state['storage']['results']['cost_inbound_data_index'];
    $put_data_unit_cost = $form_state['storage']['results']['put_data_unit_cost'];
    $no_of_daily_indexed_pages = $form_state['storage']['results']['no_of_daily_indexed_pages'];
    $avg_get_size = $form_state['storage']['results']['avg_get_size'];
    $put_request_billing_units_string = $form_state['storage']['results']['put_request_billing_units_string'];
    $get_request_billing_units_string = $form_state['storage']['results']['get_request_billing_units_string'];
    $no_of_daily_requests_total = $form_state['storage']['results']['no_of_daily_requests_total'];
    $no_of_daily_indexed_attachments = $form_state['storage']['results']['no_of_daily_indexed_attachments'];
    $size_of_daily_indexed_attachments = $form_state['storage']['results']['size_of_daily_indexed_attachments'];
    $display_indexing_request = $form_state['storage']['results']['display_indexing_request'];

  	$form['key_name'] = array(
      '#markup' =>'<table class="suporttable price-calculator" width="500" border="0" cellspacing="0" cellpadding="8">
                   <tbody>
                     <tr class="tablehead">
                       <th class="first firstcell"><p>Activity</p></th>
                       <th class="middlecell"><p>Calculation</p></th>
                       <th class="last lastcell"><p>Cost</p></th>
                     </tr>
		     <tr>
                       <td class="firstcell"><p>Inbound Data Transfer</p></td>
                       <td class="middlecell"><p>
(30 days x ' .$no_of_daily_indexed_pages .' x ' .bytes2English($size_of_daily_indexed_pages,true) .' pages) + 
(30 days x '.$no_of_daily_indexed_attachments .' x ' .bytes2English($size_of_daily_indexed_attachments,true).' attachments) at $' .$put_data_unit_cost .' per ' .bytes2English($put_data_billing_units,false) .'</p></td>
                       <td class="lastcell"><p>$' .$cost_inbound_data_index .' per month</p></td>
                     </tr>
		     <tr>
                       <td class="firstcell"><p>Outbound Data Transfer</p></td>
                       <td class="middlecell"><p>(30 days x ' .number_format($no_of_daily_requests_total,0,'.',',') .' search requests x ' .bytes2English($avg_get_size,true) .' result sets) at $' .$get_data_unit_cost .' per ' .bytes2English($get_data_billing_units,false) .'</p></td>
                       <td class="lastcell"><p>$' .$cost_outbound_data_request .' per month</p></td>
                     </tr>
                     <tr>
                       <td class="firstcell"><p>Put Requests</p></td>
                       <td class="middlecell"><p>(30 days x ' .$display_indexing_request .' indexing requests) at $' .$put_request_unit_cost .' per ' .$put_request_billing_units_string .'</p></td>
                       <td class="lastcell"><p>$' .$cost_put_request .' per month</p></td>
                     </tr>
                     <tr>
                       <td class="firstcell"><p>Get Requests</p></td>
                       <td class="middlecell"><p>(30 days x ' .number_format($no_of_daily_requests_total,0,'.',',') .' search requests) at $' .$get_request_unit_cost .' per ' .$get_request_billing_units_string .'</p></td>
                       <td class="lastcell"><p>$' .$cost_get_request .' per month</p></td>
                     </tr>
                     <tr>
                       <td class="firstcell"><p></p></td>
                       <td class="middlecell"><p><strong>TOTAL (USD):</strong></p></td>
                       <td class="lastcell"><p><strong>$' .$cost_total .' per month</strong></p></td>
                     </tr>
		     <tr class="bottom">
                       <td class="firstcell"><p></p></td>
                       <td class="lastcell" colspan=2><p><i>Approximate TOTAL (GBP): &pound;' .number_format(($cost_total * $USD_to_GBP),2,'.',',') .' per month<br>Approximate TOTAL (EUR): &euro;' .number_format(($cost_total * $USD_to_EUR),2,'.',',') .' per month<br><span style="font-size:.8em;">* Rates are current as of ' .$USD_to_GBP_asat .'</span></i></p></td>
                     </tr>
                   </tbody>
                 </table>
                 <p>Note: Costs shown are approximate only. Actual costs may vary depending on actual data transfer.</p>',
    );
  //}
  return $form;
}

/**
 * Implementation of hook_form_validate().
 */
function a12_find_price_calculator_form_validate($form, &$form_state) {
	$form_state['storage']['submit'] = TRUE;
}

/**
 * Implementation of hook_form_submit().
 */
function a12_find_price_calculator_form_submit($form, &$form_state) {
  $form_state['rebuild']= TRUE;

  // array value for price calculator
  $no_of_daily_views_array = array('500', '1000', '5000', '10000', '20000', '50000', '100000');
  $no_of_daily_requests_array = array('50', '100', '500', '1000', '5000', '10000', '30000');
  $no_of_daily_indexed_pages_array = array('5', '10', '50', '100', '500', '1000', '5000');
  $size_of_daily_indexed_pages_array = array('1024', '2048', '3072', '4096', '5120', '10240', '20480');
  $no_of_daily_indexed_attachments_array = array('5', '10', '50', '100', '500', '1000', '5000');
  $size_of_daily_indexed_attachments_array = array('5120', '10240', '20480', '51200', '102400', '1048576', '5242880');
  $no_of_batch_per_day_array = array('1', '2', '3', '4', '6', '12', '24');

  // key for associated array
  if (!isset($form_state['values'])){
  	$no_of_daily_views_unit = 0;
	  $no_of_daily_requests_unit = 0;
	  $has_facets = 'No';
	  $has_mlt = 'No';
	  $no_of_daily_indexed_pages_unit = 0;
	  $size_of_daily_indexed_pages_unit = 0;
	  $no_of_daily_indexed_attachments_unit = 0;
	  $size_of_daily_indexed_attachments_unit = 0;
	  $no_of_batch_per_day_unit = 0;
  }
  else {
	  $no_of_daily_views_unit = $form_state['values']['no_of_daily_views']['left'];
	  $no_of_daily_requests_unit = $form_state['values']['no_of_daily_requests']['left'];
	  $has_facets = $form_state['values']['facets'];
	  $has_mlt = $form_state['values']['mlt'];
	  $no_of_daily_indexed_pages_unit = $form_state['values']['no_of_daily_indexed_pages']['left'];
	  $size_of_daily_indexed_pages_unit = $form_state['values']['size_of_daily_indexed_pages']['left'];
	  $no_of_daily_indexed_attachments_unit = $form_state['values']['no_of_daily_indexed_attachments']['left'];
	  $size_of_daily_indexed_attachments_unit = $form_state['values']['size_of_daily_indexed_attachments']['left'];
	  $no_of_batch_per_day_unit = $form_state['values']['no_of_batch_per_day']['left'];
  }
  // retrive value depending upon key
  $no_of_daily_views = a12_find_price_calculator_get_value_by_key($no_of_daily_views_array, $no_of_daily_views_unit);
  $no_of_daily_requests = a12_find_price_calculator_get_value_by_key($no_of_daily_requests_array, $no_of_daily_requests_unit);
  $no_of_daily_indexed_pages = a12_find_price_calculator_get_value_by_key($no_of_daily_indexed_pages_array, $no_of_daily_indexed_pages_unit);
  $size_of_daily_indexed_pages = a12_find_price_calculator_get_value_by_key($size_of_daily_indexed_pages_array, $size_of_daily_indexed_pages_unit);
  $no_of_daily_indexed_attachments = a12_find_price_calculator_get_value_by_key($no_of_daily_indexed_attachments_array, $no_of_daily_indexed_attachments_unit);
  $size_of_daily_indexed_attachments = a12_find_price_calculator_get_value_by_key($size_of_daily_indexed_attachments_array,   $size_of_daily_indexed_attachments_unit);
  $no_of_batch_per_day = a12_find_price_calculator_get_value_by_key($no_of_batch_per_day_array, $no_of_batch_per_day_unit);

  $product_id = 6;
  $no_of_daily_requests_total = $no_of_daily_requests;
  $no_of_items_per_batch = 50;
  $avg_put_size = 1024;
  $avg_get_size = 1843;
  $avg_get_size_with_facets = 2048;
  $percentage_of_pages_with_mlt = 0.75;
  $decimal_precision = 2;

  // function for getting unit costs and billing units for Find.  
  $a12_find_pricing_data = a12_get_find_pricing($product_id);
  
  // data require
  
  $put_request_unit_cost = $a12_find_pricing_data['put_request_unit_cost'];
  $put_request_billing_units = $a12_find_pricing_data['put_request_billing_units'];

  $get_request_unit_cost = $a12_find_pricing_data['get_request_unit_cost'];
  $get_request_billing_units = $a12_find_pricing_data['get_request_billing_units'];

  if($put_request_billing_units==1){$put_request_billing_units_string = 'request';}
  else {$put_request_billing_units_string = $put_request_billing_units . ' requests';};

  if($get_request_billing_units==1){$get_request_billing_units_string = 'request';}
  else {$get_request_billing_units_string = $get_request_billing_units . ' requests';};
  
  $put_data_unit_cost = $a12_find_pricing_data['put_data_unit_cost'];
  $put_data_billing_units = $a12_find_pricing_data['put_data_billing_units'];
  
  $get_data_unit_cost = $a12_find_pricing_data['get_data_unit_cost'];
  $get_data_billing_units = $a12_find_pricing_data['get_data_billing_units'];
 
  // calculation part

  if ($has_facets == 'Yes') { $avg_get_size = $avg_get_size_with_facets; }
  if ($has_mlt == 'Yes') { $no_of_daily_requests_total = $no_of_daily_requests_total + ($no_of_daily_views * $percentage_of_pages_with_mlt); }

  /*START ATW-110: Corrections to price calculator 4th Point*/
  $cost_put_request_temp1 = number_format((30*$no_of_batch_per_day*$put_request_unit_cost/$put_request_billing_units),$decimal_precision,'.','');
  $cost_put_request_temp2 = number_format((30*ceil(($no_of_daily_indexed_pages+$no_of_daily_indexed_attachments)/$no_of_items_per_batch)*$put_request_unit_cost/$put_request_billing_units),$decimal_precision,'.','');
  $cost_put_request = ($cost_put_request_temp1 > $cost_put_request_temp2) ? $cost_put_request_temp1 : $cost_put_request_temp2;
  /*END*/

  $cost_inbound_data_index = number_format((
  (30*$no_of_daily_indexed_pages*$size_of_daily_indexed_pages*$put_data_unit_cost/$put_data_billing_units)
  +
  (30*$no_of_daily_indexed_attachments*$size_of_daily_indexed_attachments*$put_data_unit_cost/$put_data_billing_units)
  ),$decimal_precision,'.','');
  $cost_inbound_data_request = number_format((30*$no_of_daily_requests_total*$avg_put_size*$put_data_unit_cost/$put_data_billing_units),$decimal_precision,'.','');
  $cost_outbound_data_request = number_format((30*$no_of_daily_requests_total*$avg_get_size*$get_data_unit_cost/$get_data_billing_units),$decimal_precision,'.','');
  //$cost_put_request = number_format((30*$no_of_batch_per_day*$put_request_unit_cost/$put_request_billing_units),$decimal_precision,'.','');
  $cost_get_request = number_format((30*$no_of_daily_requests_total*$get_request_unit_cost/$get_request_billing_units),$decimal_precision,'.','');
  $cost_total = $cost_inbound_data_index + $cost_inbound_data_request + $cost_outbound_data_request + $cost_put_request + $cost_get_request;

  /*Start ATW-112: More corrections to price calculator 3rd Point*/
  $display_indexing_request_temp = ceil(($no_of_daily_indexed_pages+$no_of_daily_indexed_attachments)/$no_of_items_per_batch);
  $display_indexing_request = ($display_indexing_request_temp > $no_of_batch_per_day)? $display_indexing_request_temp : $no_of_batch_per_day;
  /*End*/

  // Build array to be use for form storage and value for calculated table.

  $form_state['storage']['results']['no_of_batch_per_day'] = $no_of_batch_per_day;
  $form_state['storage']['results']['size_of_daily_indexed_pages'] = $size_of_daily_indexed_pages;
  $form_state['storage']['results']['cost_get_request'] = $cost_get_request;
  $form_state['storage']['results']['get_request_unit_cost'] = $get_request_unit_cost;
  $form_state['storage']['results']['cost_total'] = $cost_total;
  $form_state['storage']['results']['no_of_daily_requests'] = $no_of_daily_requests;
  $form_state['storage']['results']['cost_put_request'] = $cost_put_request;
  $form_state['storage']['results']['put_request_unit_cost'] = $put_request_unit_cost;
  $form_state['storage']['results']['get_data_billing_units'] = $get_data_billing_units;
  $form_state['storage']['results']['get_data_unit_cost'] = $get_data_unit_cost;
  $form_state['storage']['results']['cost_outbound_data_request'] = $cost_outbound_data_request;
  $form_state['storage']['results']['put_data_billing_units'] = $put_data_billing_units;
  $form_state['storage']['results']['cost_inbound_data_index'] = $cost_inbound_data_index;
  $form_state['storage']['results']['put_data_unit_cost'] = $put_data_unit_cost;
  $form_state['storage']['results']['no_of_daily_indexed_pages'] = $no_of_daily_indexed_pages;
  $form_state['storage']['results']['avg_get_size'] = $avg_get_size;
  $form_state['storage']['results']['put_request_billing_units_string'] = $put_request_billing_units_string;
  $form_state['storage']['results']['get_request_billing_units_string'] = $get_request_billing_units_string;
  $form_state['storage']['results']['no_of_daily_requests_total'] = $no_of_daily_requests_total;
  $form_state['storage']['results']['no_of_daily_indexed_attachments'] = $no_of_daily_indexed_attachments;
  $form_state['storage']['results']['size_of_daily_indexed_attachments'] = $size_of_daily_indexed_attachments;
  $form_state['storage']['results']['display_indexing_request'] = $display_indexing_request;
}
/**
 * function to return value of array for associated key.
 * @param array, key
 * @return value
 */
function a12_find_price_calculator_get_value_by_key($array,$key = 0) {
  foreach($array as $k=>$each) {
    if($k==$key) {
      return $each;
    }

    if(is_array($each)) {
      if($return = get_value_by_key($each,$key)) {
        return $return;
      }
    }
  }
}
