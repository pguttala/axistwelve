<?php 
// $Id: product_price_calculator.include.inc, v 1.01 2012/14/08 13:46:44 MKhan Exp $
/**
 * function to calculate a12 Find Price.
 */
/** 
 * function to display a12 Find Pricing page.
 */
function a12_find_pricing_form($form, &$form_state){
  drupal_set_title('A12 Find Pricing');
  $product_id = 6;
  
  // function for getting unit costs and billing units for Find.
  $a12_find_pricing_data = a12_get_find_pricing($product_id);
  
  // data require
  
  $put_request_unit_cost = $a12_find_pricing_data['put_request_unit_cost'];
  $put_request_billing_units = $a12_find_pricing_data['put_request_billing_units'];
  
  $get_request_unit_cost = $a12_find_pricing_data['get_request_unit_cost'];
  $get_request_billing_units = $a12_find_pricing_data['get_request_billing_units'];
  
  if($put_request_billing_units==1){$put_request_billing_units_string = 'request';}
  else {$put_request_billing_units_string = $put_request_billing_units . ' requests';};

  if($get_request_billing_units==1){$get_request_billing_units_string = 'request';}
  else {$get_request_billing_units_string = $get_request_billing_units . ' requests';};
  
  $put_data_unit_cost = $a12_find_pricing_data['put_data_unit_cost'];
  $put_data_billing_units = $a12_find_pricing_data['put_data_billing_units'];
  
  $get_data_unit_cost = $a12_find_pricing_data['get_data_unit_cost'];
  $get_data_billing_units = $a12_find_pricing_data['get_data_billing_units'];

  //  get rates
  
  $from_ccy = 'USD';
  $to_ccy = 'GBP';
  $to_ccy2 = 'EUR';
  
  $url_ccy = 'http://finance.yahoo.com/d/quotes.csv?f=l1d1t1&s='.$from_ccy.$to_ccy.'=X';
  $handle_ccy = fopen($url_ccy, 'r');
 
  if ($handle_ccy) {
      $result = fgetcsv($handle_ccy);
      fclose($handle_ccy);
  }
 
  $USD_to_GBP = $result[0];
  $USD_to_GBP_asat = $result[1] .' at ' .$result[2];
  
  $url_ccy2 = 'http://finance.yahoo.com/d/quotes.csv?f=l1d1t1&s='.$from_ccy.$to_ccy2.'=X';
  $handle_ccy2 = fopen($url_ccy2, 'r');
 
  if ($handle_ccy2) {
      $result = fgetcsv($handle_ccy2);
      fclose($handle_ccy2);
  }
  
  $USD_to_EUR = $result[0];
  $USD_to_EUR_asat = $result[1] .' at ' .$result[2];

  //  example variables
  
  $no_of_daily_views = 20000;
  $no_of_daily_requests = 5000;
  $no_of_daily_requests_total = 5000;
  $no_of_daily_indexed_pages = 50;
  $size_of_daily_indexed_pages = 2048;
  $no_of_daily_indexed_attachments = 10;
  $size_of_daily_indexed_attachments = 51200;
  $no_of_batch_per_day = 1;
  $no_of_items_per_batch = 50;
  $avg_put_size = 1024;
  $avg_get_size = 1843;
  $avg_get_size_with_facets = 2048;
  $percentage_of_pages_with_mlt = 0.75;
  $decimal_precision = 2;
  
  /*START ATW-110: Corrections to price calculator 4th Point*/
  $cost_put_request_temp1 = number_format((30*$no_of_batch_per_day*$put_request_unit_cost/$put_request_billing_units),$decimal_precision,'.','');
  $cost_put_request_temp2 = number_format((30*ceil(($no_of_daily_indexed_pages+$no_of_daily_indexed_attachments)/$no_of_items_per_batch)*$put_request_unit_cost/$put_request_billing_units),$decimal_precision,'.','');
  $cost_put_request = ($cost_put_request_temp1 > $cost_put_request_temp2) ? $cost_put_request_temp1 : $cost_put_request_temp2;
  /*END*/

  $cost_inbound_data_index = number_format((
  (30*$no_of_daily_indexed_pages*$size_of_daily_indexed_pages*$put_data_unit_cost/$put_data_billing_units)
  +
  (30*$no_of_daily_indexed_attachments*$size_of_daily_indexed_attachments*$put_data_unit_cost/$put_data_billing_units)
  ),$decimal_precision,'.','');
  $cost_inbound_data_request = number_format((30*$no_of_daily_requests_total*$avg_put_size*$put_data_unit_cost/$put_data_billing_units),$decimal_precision,'.','');
  $cost_outbound_data_request = number_format((30*$no_of_daily_requests_total*$avg_get_size*$get_data_unit_cost/$get_data_billing_units),$decimal_precision,'.','');
  //$cost_put_request = number_format((30*$no_of_batch_per_day*$put_request_unit_cost/$put_request_billing_units),$decimal_precision,'.','');
  $cost_get_request = number_format((30*$no_of_daily_requests_total*$get_request_unit_cost/$get_request_billing_units),$decimal_precision,'.','');
  $cost_total = $cost_inbound_data_index + $cost_inbound_data_request + $cost_outbound_data_request + $cost_put_request + $cost_get_request;

  /*Start ATW-112: More corrections to price calculator 3rd Point*/
  $display_indexing_request_temp = ceil(($no_of_daily_indexed_pages+$no_of_daily_indexed_attachments)/$no_of_items_per_batch);
  $display_indexing_request = ($display_indexing_request_temp > $no_of_batch_per_day)? $display_indexing_request_temp : $no_of_batch_per_day;
  /*End*/
  
  $form = array();
        $form['key_name'] = array(
                '#markup' => '<p><h3>Pricing</h3>
                              <p>There are no set-up fees, no minimum monthly charges and no contract commitments with A12 Find. You are billed according to your monthly usage based on the amount of data transferred and the number of put/get requests made. If you don\'t use the service, your bill will be... <strong>Zero</strong>.</p>
                              <h3>Search Instances - no limits, and no costs!</h3>
                              <p>Many of our users manage multiple websites, and need a separate search index for each one. That\'s why we havn\'t imposed any limits on the number of sites that can be managed or the number of indices that can be created. We also understand the need to keep each system environment separate. That\'s why every index you create automatically comes with three separate cores - Development, Test and Production - all for <strong>Free!</strong><BR /><BR />To make things as easy as possible, we let everything be managed through a single user account, and allow granular access to be granted using multiple Access Keys. Of course if you want one Access Key to be able to access everything, this is possible as well!</p>
                              <h3>Data Transfer Pricing</h3>
                              <p>The pricing below is based on the data transferred to and from A12 Find. The INBOUND data size is determined by the amount of content sent to A12 Find for indexing. The OUTBOUND data size is determined by the size of the result sets returned by A12 Find to your users.</p>
                              <h5>Region: Europe (United Kingdom)</h5>
                              <table class="suporttable" width="500" border="0" cellspacing="0" cellpadding="8">
                              	<tbody>
                              	  <tr class="tablehead">
                              	    <th class="first" valign="top" nowrap="nowrap" width="125"><p align="center"><strong>Data transfer</strong></p></th>
                                    <th class="last" valign="top" nowrap="nowrap" width="125"><p align="center"><strong>Cost (USD)</strong></p></th>
                                  </tr>
                                  <tr>
                                    <td class="firstcell" align="center" valign="top" width="125"><p>IN</p></td>
                                    <td valign="top" width="125"><p align="center">$' .$put_data_unit_cost .' per ' .bytes2English($put_data_billing_units,false) .'</p></td>
                                  </tr>
                                  <tr class="bottom">
                                    <td class="firstcell" align="center" valign="top" width="125"><p>OUT</p></td>
                                    <td valign="top" nowrap="nowrap" width="125"><p align="center">$' .$get_data_unit_cost .' per ' .bytes2English($get_data_billing_units,false) .'</p></td>
                                  </tr>
                                </tbody>
                              </table>
                              <br>
                              <h3>Data Request Pricing</h3>
                              <p>The pricing below is based on the number of requests made to A12 Find. PUT requests are the requests made to A12 Find to index content, and GET requests are the search requests generated by your users. If your site has recommendation or facet blocks configured, these will also generate a search request.</p>
                              <h5>Region: Europe (United Kingdom)</h5>
                              <table class="suporttable" width="500" border="0" cellspacing="0" cellpadding="8">
                    		<tbody>
                    		  <tr class="tablehead">
                    		    <th class="first" valign="top" nowrap="nowrap" width="125"><p align="center"><strong>Data Requests</strong></p></th>
                                    <th class="last" valign="top" nowrap="nowrap" width="125"><p align="center"><strong>Cost (USD)</strong></p></th>
                                  </tr>
                                  <tr>
                                    <td class="firstcell" align="center" valign="top" width="125"><p>PUT</p></td>
                                    <td valign="top" width="125"><p align="center">$' .$put_request_unit_cost .' per ' .$put_request_billing_units_string .'</p></td>
                                  </tr>
                                  <tr class="bottom">
                                    <td class="firstcell" align="center" valign="top" width="125"><p>GET</p></td>
                                    <td valign="top" nowrap="nowrap" width="125"><p align="center">$' .$get_request_unit_cost .' per ' .$get_request_billing_units_string .'</p></td>
                                  </tr>
                                </tbody>
                              </table>
                              <p>You can always see how many requests you are making and how much data is being transferred through your Usage Reports and Billing Statements on your Axistwelve dashboard.</p>
                              <h3>Cost Example</h3>
                              <p>In the example below, you have approximately 25,000 documents in your search index and expect the following:</p>
                              <p><ul>
			        <li style="margin-left:30px;">' .number_format($no_of_daily_requests,0,'.',',') .' keyword search requests are made each day.</li>
                                <li style="margin-left:30px;">Each search request returns an average of ' .bytes2English($avg_get_size,true) .' of data.</li>
				<li style="margin-left:30px;">' .$no_of_daily_indexed_pages .' documents of approximately ' .bytes2English($size_of_daily_indexed_pages,false) .' in size are indexed each day.</li> 
			        <li style="margin-left:30px;">' .$no_of_daily_indexed_attachments .' attachments of approximately ' .bytes2English($size_of_daily_indexed_attachments,false) .' in size are indexed each day.</li> 
			      </ul></p>
			      <p>Use our ' .l('price calculator','content/a12-find-price-calculator') .' to see how little your costs might be.</p>
			      <table class="suporttable price-calculator" width="500" border="0" cellspacing="0" cellpadding="8">
			       <tbody>
				 <tr class="tablehead">
				   <th class="first firstcell"><p align="center"><strong>Activity</strong></p></th>
				   <th class="middlecell"><p align="center"><strong>Calculation</strong></p></th>
				   <th class="last lastcell"><p align="center"><strong>Cost</strong></p></th>
				 </tr>
				 <tr>		
				   <td class="firstcell"><p>Inbound Data Transfer</p></td>
				   <td class="middlecell"><p>
	    (30 days x ' .$no_of_daily_indexed_pages .' x ' .bytes2English($size_of_daily_indexed_pages,true) .' pages) + 
	    (30 days x '.$no_of_daily_indexed_attachments .' x ' .bytes2English($size_of_daily_indexed_attachments,true).' attachments) at $' .$put_data_unit_cost .' per ' .bytes2English($put_data_billing_units,false) .'</p></td>
				   <td class="lastcell"><p>$' .$cost_inbound_data_index .' per month</p></td>
				 </tr>
				 <tr>
				   <td class="firstcell"><p>Outbound Data Transfer</p></td>
				   <td class="middlecell"><p>(30 days x ' .number_format($no_of_daily_requests_total,0,'.',',') .' search requests x ' .bytes2English($avg_get_size,true) .' result sets) at $' .$get_data_unit_cost .' per ' .bytes2English($get_data_billing_units,false) .'</p></td>
				   <td class="lastcell"><p>$' .$cost_outbound_data_request .' per month</p></td>
				 </tr>
				 <tr>
				   <td class="firstcell"><p>Put Requests</p></td>
				   <td class="middlecell"><p>(30 days x ' .$display_indexing_request .' indexing requests) at $' .$put_request_unit_cost .' per ' .$put_request_billing_units_string .'</p></td>
				   <td class="lastcell"><p>$' .$cost_put_request .' per month</p></td>
				 </tr>
				 <tr>
				   <td class="firstcell"><p>Get Requests</p></td>
				   <td class="middlecell"><p>(30 days x ' .number_format($no_of_daily_requests_total,0,'.',',') .' search requests) at $' .$get_request_unit_cost .' per ' .$get_request_billing_units_string .'</p></td>
				   <td class="lastcell"><p>$' .$cost_get_request .' per month</p></td>
				 </tr>
				 <tr>
				   <td class="firstcell"><p></p></td>
				   <td class="middlecell"><p><strong>TOTAL (USD):</strong></p></td>
				   <td class="lastcell"><p><strong>$' .$cost_total .' per month</strong></p></td>
				 </tr>
				 <tr class="bottom">
				   <td class="firstcell"><p></p></td>
				   <td class="lastcell" colspan=2><p><i>Approximate TOTAL (GBP): &pound;' .number_format(($cost_total * $USD_to_GBP),2,'.',',') .' per month<br>Approximate TOTAL (EUR): &euro;' .number_format(($cost_total * $USD_to_EUR),2,'.',',') .' per month<br><span style="font-size:.8em;">* Rates are current as of ' .$USD_to_GBP_asat .'</span></i></p></td>
				 </tr>
			       </tbody>
			     </table>
			     <p>Note: Costs shown are approximate only. Actual costs may vary depending on actual data transfer.</p>',
        );
  return $form;
}
