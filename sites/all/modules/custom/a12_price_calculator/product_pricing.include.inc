<?php 
// $Id: product_pricing.include.inc, v 1.01 2012/20/04 10:03:44 MKhan Exp $
/**
 * function for getting unit costs and billing units for Find.
 * @return billing units and unit costs for the FIND service.
 */

function bytes2English($bytes,$show_number_if_single) {
  // returns bytes formatted as kB, MB etc (e.g. 1024 => 1kB, 2048000 => 2MB etc)
  // when $show_number_if_single is false 1kB => kB, 1MB => MB, 1GB => GB etc)
  $tb = 1024*1024*1024*1024;
  $gb = 1024*1024*1024;
  $mb = 1024*1024;
  $kb = 1024;
  if ($bytes<$kb) {
	  $returnval = number_format($bytes,1,'.',',');
	  $returntype = 'Bytes';
  }
  if ($bytes>=$kb && $bytes<$mb) {
	  $returnval = number_format($bytes/$kb,1,'.',',');
	  $returntype = 'kB';
  }
  if ($bytes>=$mb && $bytes<$gb) {
	  $returnval = number_format($bytes/$mb,1,'.',',');
	  $returntype = 'MB';
  }
  if ($bytes>=$gb && $bytes<$tb) {
	   $returnval = number_format($bytes/$gb,2,'.',',');
	  $returntype = 'GB';
  }
  if ($bytes>=$tb) {
	  $returnval = number_format($bytes/$tb,2,'.',',');
	  $returntype = 'TB';
  }
  if ($returnval==1) {
	  if($show_number_if_single==true) return $returnval .$returntype;
	  else return $returntype;
    }
  else {
	  return $returnval .$returntype;
  }
}

//FND:-164 Create a single function for getting unit costs and billing units for Find.
function a12_get_find_pricing($product_id){
	if(1 == (variable_get('a12_dbconnection_status')) && !(db_table_exists('a12_product_find_history'))) {
		$a12_database = array(
			'database' => variable_get('a12_database_name', ''),
			'username' => variable_get('a12_database_username', ''),
			'password' => variable_get('a12_database_password', ''),
			'host' => variable_get('a12_database_host', ''),
			'driver' => 'mysql',
	  );
	  Database::addConnectionInfo('a12-db-connection', 'default', $a12_database);
		db_set_active('a12-db-connection'); //A12 DB Connection closed
	}	

	$current_month_first_date = date('d-m-Y H:i:s',strtotime((date('m')).'/01/'.date('Y').' 00:59:59'));
	$current_month_first_date_timestamp = strtotime($current_month_first_date);
	$current_month_first_date_timestamp = $current_month_first_date_timestamp + 1;
 
  $sql_a12_find_all_previous_pricing = "SELECT * FROM {a12_product_find_history} WHERE product_id = :product_id AND created < :current_month_first_date_timestamp";
  $sql_a12_find_all_previous_pricing_obj= db_query($sql_a12_find_all_previous_pricing , array(':product_id' => $product_id, ':current_month_first_date_timestamp' => $current_month_first_date_timestamp));
  $sql_a12_find_all_previous_pricing_row_count = $sql_a12_find_all_previous_pricing_obj -> rowCount();
  
  $a12_pricing_result = array();
  
  $month = date('m');
  $year = date('Y');
  
  if($sql_a12_find_all_previous_pricing_row_count > 0){
  	// FND-164 1.b) get the data row where datetimestamp is MAX and is less than start of current month (i.e if now = April, look for the data row with the MAX(datetimestamp) that is less than 01.04.2012
	  $i = 1;
		while ($i > 0) {    // Execute the loop untill we don't get previous month price record.
		  $month--;         // Minus month each time
			if($month == 0){  // If current month is Jan. set month to Dec.
			  $month = 12;				  			
				$year--;        // Also set year to previous year dependent upon current month.
		  }
			$prior_month_first_date = date('d-m-Y H:i:s',strtotime($month.'/01/'.$year.' 00:00:00'));
			$prior_month_last_date = date('d-m-Y H:i:s',strtotime('-1 second',strtotime('+1 month',strtotime($month.'/01/'.$year.' 00:00:00'))));
			      
			/* Fetch prior maonth First and Last date */
			$prior_month_first_date_timestamp = strtotime($prior_month_first_date);
			$prior_month_last_date_timestamp = strtotime($prior_month_last_date);
			
			/* Fetch latest a12_product_find_id dependent upon latest update */
			$sql_a12_find_pricing_max = "SELECT a12_product_find_id, product_id, put_request_unit_cost, put_request_billing_units, get_request_unit_cost, get_request_billing_units, put_data_unit_cost, put_data_billing_units, get_data_unit_cost, get_data_billing_units   FROM {a12_product_find_history} WHERE created = (SELECT max(created) from {a12_product_find_history} WHERE product_id = :product_id AND created BETWEEN :prior_month_start_timestamp AND :prior_month_end_timestamp)";

			$sql_a12_find_pricing_max_obj= db_query($sql_a12_find_pricing_max , array(':product_id' => $product_id, ':prior_month_start_timestamp' => $prior_month_first_date_timestamp, ':prior_month_end_timestamp' => $prior_month_last_date_timestamp));
			$a12_find_pricing_data = $sql_a12_find_pricing_max_obj -> fetchAll();
      if(!(empty($a12_find_pricing_data))) {
				$a12_product_find_id = $a12_find_pricing_data[0]->a12_product_find_id;
				if(!(empty($a12_product_find_id))) {
					break;
		  	}
			}
			else{
			  $i++;
			} 	  
	  }				  	
  }
	else{
		//FND-164 1.c) if no results, get data row where datetimestamp is MINIMUM (note: this should return the earliest value from current month)
		
		//NOTE: TEMPPORARY OVERRIDE THIS TO MAX FOR TESTING PURPOSES. SET BACK TO MIN WHEN FINISHED
		
	  $sql_a12_find_current_month_min_price = "SELECT a12_product_find_id, product_id, put_request_unit_cost, put_request_billing_units, get_request_unit_cost, get_request_billing_units, put_data_unit_cost, put_data_billing_units, get_data_unit_cost, get_data_billing_units  FROM {a12_product_find_history} WHERE created = (SELECT max(created) from {a12_product_find_history} WHERE product_id = :product_id AND created > :current_month_first_date_timestamp)";
	  $sql_a12_find_current_month_min_price_obj= db_query($sql_a12_find_current_month_min_price , array(':product_id' => $product_id, ':current_month_first_date_timestamp' => $current_month_first_date_timestamp));
		$a12_find_pricing_data = $sql_a12_find_current_month_min_price_obj -> fetchAll();		
  }
	if(1 == (variable_get('a12_dbconnection_status'))) {
		db_set_active(); //A12 DB Connection closed		
  }  
  // data require
  $a12_pricing_result['a12_product_find_id'] = $a12_find_pricing_data[0]->a12_product_find_id;
  $a12_pricing_result['product_id'] = $a12_find_pricing_data[0]->product_id;
  
	$a12_pricing_result['put_request_unit_cost'] = $a12_find_pricing_data[0]->put_request_unit_cost;
	$a12_pricing_result['put_request_billing_units'] = $a12_find_pricing_data[0]->put_request_billing_units;
	
	$a12_pricing_result['get_request_unit_cost'] = $a12_find_pricing_data[0]->get_request_unit_cost;
	$a12_pricing_result['get_request_billing_units'] = $a12_find_pricing_data[0]->get_request_billing_units;

	$a12_pricing_result['put_data_unit_cost'] = $a12_find_pricing_data[0]->put_data_unit_cost;
	$a12_pricing_result['put_data_billing_units'] = $a12_find_pricing_data[0]->put_data_billing_units;
	  
	$a12_pricing_result['get_data_unit_cost'] = $a12_find_pricing_data[0]->get_data_unit_cost;
	$a12_pricing_result['get_data_billing_units'] = $a12_find_pricing_data[0]->get_data_billing_units;	  

  return $a12_pricing_result;
}
