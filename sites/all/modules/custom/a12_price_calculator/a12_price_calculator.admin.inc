<?php 
// $Id: product_pricing.include.inc, v 1.01 2012/20/04 10:03:44 MKhan Exp $
/**
 * function to input database credential to set connction with A12.
 * @param string $form, &$form_state
 */

function _a12_find_connection_settings_form($form, &$form_state) {

  $form = array();  
    
  $form['a12_dbconnection_status'] = array(
  	'#type' => 'checkbox',
    '#title' => t("Check to set A12 database connection"),
    '#default_value' => variable_get('a12_dbconnection_status', 0),
  );  
  
  $form['a12_dbconnection'] = array(
	  '#type' => 'fieldset', 
	  '#title' => t('Database connection settings'),
  	'#description' => t("Apply setting to connect with A12 database 'a12_product_find_history' table."),
  	'#states' => array(
      'invisible' => array(
       ':input[name="a12_dbconnection_status"]' => array('checked' => FALSE),
      ),
    ),
	);
	
	$form['a12_dbconnection']['a12_database_name'] = array(
		'#type' => 'textfield',
		'#title' => t('A12 database name'),
		'#default_value' => variable_get('a12_database_name', ''),
		'#required' => TRUE,
	);
	
	$form['a12_dbconnection']['a12_database_username'] = array(
		'#type' => 'textfield',
		'#title' => t('A12 database username'),
		'#default_value' => variable_get('a12_database_username', ''),
		'#required' => TRUE,
	);
	
	$form['a12_dbconnection']['a12_database_password'] = array(
		'#type' => 'password',
		'#title' => t('A12 database password'),
		'#default_value' => variable_get('a12_database_password', ''),
		'#required' => TRUE,
	);
	
	$form['a12_dbconnection']['a12_database_host'] = array(
		'#type' => 'textfield',
		'#title' => t('A12 database host'),
		'#default_value' => variable_get('a12_database_host', ''),
	  '#required' => TRUE,
	);
	  
	$form['submit'] = array(
	  '#type' => 'submit',
	  '#value' => t('Save configuration'),
	);
 	return $form;
}

/**
 * Submit handler for _a12_find_connection_settings_form().
 * @param string $form, &$form_state
 */

function _a12_find_connection_settings_form_submit($form, &$form_state) {	
  if($form_state['values']['a12_dbconnection_status']) {	
		$other_database = array(
      'database' => $form_state['values']['a12_database_name'],
      'username' => $form_state['values']['a12_database_username'],
      'password' => $form_state['values']['a12_database_password'],
      'host' => $form_state['values']['a12_database_host'],
      'driver' => 'mysql',
  	);  
  	$test_connection = _test_connection($other_database);
  	if($test_connection) {
  		variable_set('a12_database_name', $form_state['values']['a12_database_name']);
  		variable_set('a12_database_username', $form_state['values']['a12_database_username']);
  		variable_set('a12_database_password', $form_state['values']['a12_database_password']);
  		variable_set('a12_database_host', $form_state['values']['a12_database_host']);
  		variable_set('a12_dbconnection_status', $form_state['values']['a12_dbconnection_status']);
  		drupal_set_message(t('Database connection with '.$form_state['values']['a12_database_name'].' has been saved.'));	
  	}
  }
	else {
		variable_set('a12_database_name', '');
		variable_set('a12_database_username', '');
		variable_set('a12_database_password', '');
		variable_set('a12_database_host', '');
		variable_set('a12_dbconnection_status', $form_state['values']['a12_dbconnection_status']);
		drupal_set_message(t('Default database connection is in use.'));
	}	
}

/**
 * Test the connection before making your querys
 * @param string $a12_database
 */
function _test_connection($a12_database){ 
	try {
		Database::addConnectionInfo('A12-Database', 'default', $a12_database);
		db_set_active('A12-Database');	
		$db1 = Database::getConnection();
	}
	catch (Exception $e) {
		drupal_set_message(st('Failed to connect to your database server. The server reports the following message: %error.<ul><li>Is the database server running?</li><li>Does the database exist, and have you entered the correct database name?</li><li>Have you entered the correct username and password?</li><li>Have you entered the correct database hostname?</li></ul>', array('%error' => $e->getMessage())),'warning');
		db_set_active();
    return FALSE;
  }
  db_set_active();  	 
  return TRUE;
}