<?php
// $Id: a12_indexes.admin.inc,v 1.0 2011/05/07 19:59:44 karthik Exp $

/*
 *Admin interface to handle indexes for user access keys.
 *
 */
function a12_indexes_access_key_settings_form($form, &$form_state, $secret_key_id){
  global $user;
  $access_key_obj = db_query("SELECT key_name FROM {user_system_token} WHERE secret_key_id = :key_id", array(':key_id' => $secret_key_id));
  $access_key = $access_key_obj->fetchAll();
  drupal_set_title('Configure ' . $access_key[0]->key_name);

 /* $form['subtitle'] = array(*/
 /*  '#markup' => t('<div class = "a12_index_settings"><h4>Index access settings for '. $access_key[0]->key_name .' access key </h4></div>'),*/
 /*);*/

  $form['add_text'] = array(
    '#markup' => t("Select the Index(s) that you want the <strong>'". $access_key[0]->key_name ."'</strong> Access Key to have access to."),
  );

  $breadcrumb = array(
     l(t('Home'), '<front>'),
     l(t('My Account'), 'user'), 
     l(t('My Access keys'), 'user/access_keys'),
     l(t('Access Key Settings'), 'a12_indexes/'.$secret_key_id.'/access_key_settings'),  
  );  
  drupal_set_breadcrumb($breadcrumb);
  
  $indexes_obj = db_query("SELECT * FROM {a12_indexes} WHERE uid = :uid AND status = :status ", array(':uid' => $user->uid, ':status' => ACTIVE));  
  $indexes = $indexes_obj -> fetchAll();
  if($indexes) {
    foreach($indexes as $key => $index) {
       $synonym =  taxonomy_term_load($index->synonym);       
       $options[$index->index_id] = array(
                                'name' => $index->label,
                                'description' => $index->description,
                                'synonym' => $synonym->name,  
                                ); 
    } 
  }

  $form['secret_key_id'] = array(
		'#type' => 'value',
		'#value' => $secret_key_id,
	);

  $header = array(
  'name' => array('data' => t('Index Name'), 'class' => 'idx_name'),
  'description' => array('data' => t('Description'), 'class' => 'idx_desc'),
  'synonym' => array('data' => t('Synonym Library'), 'class' => 'idx_synonym'),
  );
  
  $default_indexes_obj = db_query("SELECT B.index_id FROM {a12_indexes} A, {a12_map_indexes} B WHERE A.uid = :uid AND A.index_id = B.index_id AND B.secret_key_id = :secret_key_id", array(':uid' => $user->uid, ':secret_key_id' => $secret_key_id));  
  $default_indexes = $default_indexes_obj -> fetchAll();
  $index_values = array();
  if($default_indexes) {
    foreach($default_indexes as $key => $index) {
      $index_values[$index->index_id] = $index->index_id;
    }    
  }
  $form['table'] = array(
    '#type' => 'tableselect',
    '#header' => $header,
    '#options' => $options,
    '#empty' => t('You currently do not have any indexes'),
    '#default_value' => $index_values,
  );

  $form['selected_indexes'] = array(
    '#type' => 'value',
    '#value' => $index_values,
  );

  $form['cancel'] = array(
    '#markup' => l(t('Cancel'), 'user/access_keys'),    
    '#prefix' => '<div class="manage_setting_buttons">',
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Save',
    '#submit' => array('a12_indexes_access_key_settings_form_submit'),
    '#attributes' => array('class' => array('btn')),
    '#suffix' => '</div>',
  );
  
  return $form;
}

/*
 * Funtion to redirect to manage indexes page.
 *
*/
function a12_indexes_link_manage_indexes($form, &$form_state) {
  $secret_key_id = $form_state['values']['secret_key_id'];
  $form_state['redirect'] = array('a12_indexes/manage_indexes', array('query'=> array('key' => $secret_key_id)));  
}

/*
*Submit handler to manage indexes with access keys.
*
*/
function a12_indexes_access_key_settings_form_submit($form, &$form_state) {
  $values = $form_state['values'];
  $checked_indexes = array();
  $selected_indexes = $values['selected_indexes'];  
  foreach($values['table'] as $key => $value) {
    if($value) {
      $checked_indexes[$key] = $key; 
    }
  }    
  if(is_array($selected_indexes) && !empty($selected_indexes)) {     
    db_query("DELETE FROM {a12_map_indexes} WHERE secret_key_id = :secret_key_id", array(':secret_key_id' => $values['secret_key_id']));
  }
  $indexes['secret_key_id'] = $values['secret_key_id'];
  if(is_array($checked_indexes) && !empty($checked_indexes)) {
    foreach($checked_indexes as $checked_key => $checked_value) {     
      $indexes['index_id'] = $checked_value;
      drupal_write_record('a12_map_indexes', $indexes);    
    }
  }
  drupal_set_message("Access Key permissions updated");
  $form_state['redirect'] = array('user/access_keys');
}

/*
 * Manage index keys with Access keys.
 *
 */
function a12_indexes_index_key_settings_form($form, &$form_state, $index_key_id){
  global $user;
  $access_key_obj = db_query("SELECT label FROM {a12_indexes} WHERE index_id = :key_id", array(':key_id' => $index_key_id));
  $access_key = $access_key_obj->fetchAll();
  drupal_set_title('Configure ' . $access_key[0]->label);
  
  $form['add_text'] = array(
    '#markup' => t("Select the Access Key(s) that you want to have access to the <strong>'". $access_key[0]->label ."'</strong> index."),
  );
  
  // get all active access keys generated by the current user  
  $indexes_secret_key = db_query("SELECT * FROM {user_system_token} WHERE uid=:uid AND status = 1 ORDER BY key_name ASC", array(':uid' => $user->uid));
  
  $indexes_secret_key_name = $indexes_secret_key -> fetchAll();
  
  if($indexes_secret_key_name) {
  	foreach($indexes_secret_key_name as $key => $key_details) {
  		$options[$key_details->secret_key_id] = array(
  		  'key_name' => $key_details->key_name,
  		);
  	}
  }
  
  $default_access_key_obj = db_query("SELECT  B.secret_key_id FROM {a12_indexes} A, {a12_map_indexes} B WHERE A.uid = :uid AND A.index_id = B.index_id AND B.index_id = :current_index", array(':uid' => $user->uid, ':current_index' => $index_key_id));  
  $default_access_key = $default_access_key_obj -> fetchAll();
  $key_values = array();
  if($default_access_key) {
    foreach($default_access_key as $key => $key_details) {
      $key_values[$key_details->secret_key_id] = $key_details->secret_key_id;
    }    
  }
      
  $form['index_id'] = array(
		'#type' => 'value',
		'#value' => $index_key_id,
	);

  $header = array(
  'key_name' => array('data' => t('Key Name')),
  );
  
  $form['table'] = array(
    '#type' => 'tableselect',
    '#header' => $header,
    '#options' => $options,
    '#empty' => t('You currently do not have any enabled Access Keys'),
    '#default_value' => $key_values,
  );

  $form['selected_access_keys'] = array(
    '#type' => 'value',
    '#value' => $key_values,
  );

  $form['cancel'] = array(
    '#markup' => l(t('Cancel'), 'user/access_keys'),    
    '#prefix' => '<div class="manage_setting_buttons">',
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Save',
    '#submit' => array('a12_indexes_index_key_settings_form_submit'),
    '#attributes' => array('class' => array('btn')),
    '#suffix' => '</div>',
  );
  
  return $form;
}

/*
*Submit handler to manage indexes with access keys.
*
*/
function a12_indexes_index_key_settings_form_submit($form, &$form_state) {
  $values = $form_state['values'];
  $checked_access_keys = array();
  $selected_access_keys = $values['selected_access_keys'];  
  foreach($values['table'] as $key => $value) {
    if($value) {
      $checked_access_keys[$key] = $key; 
    }
  }    
  if(is_array($selected_access_keys) && !empty($selected_access_keys)) {
    db_query("DELETE FROM {a12_map_indexes} WHERE index_id = :index_id", array(':index_id' => $values['index_id']));    
  }
  
  $access_keys['index_id'] = $values['index_id'];
  if(is_array($checked_access_keys) && !empty($checked_access_keys)) {
    foreach($checked_access_keys as $checked_key => $checked_value) {     
      $access_keys['secret_key_id'] = $checked_value;
      drupal_write_record('a12_map_indexes', $access_keys);      
    }
  }
  drupal_set_message("Index Key permissions updated");
  $form_state['redirect'] = array('a12_indexes/manage_indexes');
}
