<?php
// $Id: registration_beta_csv.inc, v 1.01 2011/25/08 17:08:06 mkhan Exp $
/**
 * Submit handler for registration_beta_user_download_csv
 * @return Downloadable CSV for Beta Register users.
 */
function registration_beta_user_download_csv($form, &$form_state) {
	// Format Date eg: DD-MM-YYYY HH:MM:SS
	$date_fmt = 'd-m-Y H:i:s';
	$registration_beta_csv = $form_state['values']['table'];
	$registration_beta_flag = FALSE;
	foreach ($registration_beta_csv as $registration_beta_csv_value) {
		if ($registration_beta_csv_value != 0) {
			$registration_beta_flag = TRUE;
		}
	}
	// Alert if no record selected
	if($registration_beta_flag == FALSE) {
		form_set_error(registared_beta_download_CSV, t("No record selected to create and download CSV"));
	}
  else {
		// Set Header for CSV
		drupal_add_http_header('Content-Type', 'application/octet-stream');
		drupal_add_http_header("Content-Disposition" ," attachment; filename = beta_user-".time().".csv");
		print("Product , Name , E-mail Id , Timestamp");
		foreach ($registration_beta_csv as $registration_beta_csv_value) {
			$registration_beta_csv_result = db_query("SELECT * FROM {a12_registration_beta} WHERE 
			a12_registration_beta_id = :a12_registration_beta_id", array(':a12_registration_beta_id' => $registration_beta_csv_value));
			
			// Content for CSV file
			foreach ($registration_beta_csv_result as $registration_beta_csv_val) {
				print("\n" . $registration_beta_csv_val->product . 
				" , " . $registration_beta_csv_val->beta_user_name . 
				" , " . $registration_beta_csv_val->beta_user_email . 
				" , " . format_date($registration_beta_csv_val->timestamp, 'custom', $date_fmt));
			}
		}
		// To remove unwanted text in CSV file (eg. page view source)
		module_invoke_all('exit');
		exit;		
	}	
}