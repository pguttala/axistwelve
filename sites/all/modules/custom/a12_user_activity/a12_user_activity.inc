<?php

/**
* Callback for creating activity resources.
*
* @param object $data
* @return object
*/
function _a12_user_activity_create($data) {
  global $user;
  
  $data = (object)$data;
  unset($data->activity_id);
  
  $data->uid = $user->uid;
  $data->request_datetime = time();

  if (!isset($data->product_id)) {
    return services_error('Missing product id', 406);
  }

  if (!isset($data->token_id)) {
    return services_error('Missing token id', 406);
  }

  if (!isset($data->request_type)) {
    return services_error('Missing request type', 406);
  }

  if (!isset($data->request_size)) {
    return services_error('Missing request size', 406);
  }

  if (!isset($data->request_records)) {
    return services_error('Missing request records', 406);
  }

  a12_user_activity_write_activity($data);
  return (object)array(
    'id' => $data->activityid,
    'uri' => services_resource_uri(array('activity', $data->id)),
  );
}



