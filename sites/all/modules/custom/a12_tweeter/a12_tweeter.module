<?php

// $Id: a12_tweeter.module,v 1.01 2012/04/01 19:59:45 tusharb Exp $

/**
 * @files
 *
 * Build module with three circle image and the display text at left part of block.
 *
 */

/**
 *
 * Implementation of hook_block_info
 *
 */

//define('CACHE_TIME', 4);

function a12_tweeter_block_info() {
  $blocks = array();
  $blocks['latest_tweeter_status'] = array(
    'info' => t('Latest tweeter status'),
  );
  return $blocks;
}

/**
 *Implementation of hook_block_view().
 * @param  $data,
 *
 */

function a12_tweeter_block_view($delta='') {
  $block = array();
  switch ($delta) {
    case 'latest_tweeter_status':
      $block['subject'] = t('');
      $block['content'] = _get_garfunkels_tweeter_json();
      break;
  }
  return $block;
}



function _get_garfunkels_tweeter_json() {
  
  $tweeter_name = variable_get('tweeter_username', 'google');
  
  if ($cache_tweeter_status = cache_get('cache_latest_tweeter_status')) {
    $cache_tweeter_status_date = cache_get('cache_latest_tweeter_date');
    $cache_tweeter_status_id = cache_get('cache_latest_tweeter_id');
    return _build_tweeter_block_content($cache_tweeter_status->data, $cache_tweeter_status_date->data, $cache_tweeter_status_id->data, $tweeter_name);
  } else {
    $status = get_status_from_tweeter();
    $cache_time = time()+(60*60*variable_get('tweet_cache', 2));
    cache_set('cache_latest_tweeter_status', $status['results'], 'cache', $cache_time);
    cache_set('cache_latest_tweeter_date', $status['date'], 'cache', $cache_time);
    cache_set('cache_latest_tweeter_id', $status['id'], 'cache', $cache_time);
    return _build_tweeter_block_content($status['results'], $status['date'], $status['id'], $tweeter_name);
  }
}


function get_status_from_tweeter() {
  $response = search();
  if (empty($response)) {
        $return['status'] = 'Empty response from Twitter.';
  } else {
      $decoded = json_decode($response, true);
      //print_r($decoded);
      if (empty($decoded)) {
        $return['status'] = 'Response from Twitter is not valid JSON data.';
      } else {
        $data = $decoded[0];
      }
      // An error from the API.
      if (!empty($data->error)) {
        $return['status'] = $data->error;
      }
      // Everything was ok.
      else {
        $return['results'] = $data->text;
        $return['date'] = $data->created_at;
        $return['id'] = $data->id_str;
      }
   }
  return $return;
}

function search() {
  
  $options['screen_name'] = variable_get('tweeter_username', 'google') ;
  $options['count'] = variable_get('tweet_count', 2);
  
  // @todo: Make this URL a configurable option.
  $url_query = 'https://api.twitter.com/1/statuses/user_timeline.json?';
  
  $url_query .= drupal_http_build_query($options);
  $ch = curl_init($url_query);

  // Applications must have a meaningful and unique User Agent.
  curl_setopt($ch, CURLOPT_USERAGENT, "Drupal Twitter Block Module");
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
  curl_setopt($ch, CURLOPT_HTTPHEADER, array('Expect:'));

  $twitter_data = curl_exec($ch);
  $http_status = curl_getinfo($ch, CURLINFO_HTTP_CODE);

  curl_close($ch);
  return $twitter_data;
}



function cleanup_tweeter_data($status_text) {
  // Linkify URLs.
  $status_text = preg_replace(
    '/(https?:\/\/\S+)/',
    '<a href="\1">\1</a>',
    $status_text
  );

  // Linkify twitter users.
  $status_text = preg_replace(
    '/(^|\s)@(\w+)/',
    '\1@<a href="http://twitter.com/\2">\2</a>',
    $status_text
  );

  // Linkify tags.
  $status_text = preg_replace(
    '/(^|\s)#([\wåäöÅÄÖ]+)/',
    '\1#<a href="http://search.twitter.com/search?q=%23\2">\2</a>',
    $status_text
  );

  return $status_text;
}


function _build_tweeter_block_content($tweeter_status = '', $tweeter_date, $tweeter_id = 0, $username) {
  
  if(!$tweeter_id || !isset($tweeter_status)) {
    return '<div id = "tweet">@<a href = "http://www.twitter.com/' . $username . '" target = "_blank">' . $username . '</a>: <em>No tweet retrieved.</em></div>';
  }
  $tweet_block  = '<div id = "tweet_date">';
  $tweet_block .= date(" j M", strtotime($tweeter_date)) . '</div>';
  $tweet_block .= '<div id = "twitter_head">Axis Twelve@Axistwelve</div>';
  $tweet_block .= '<div id = "twitter_container">';
  $tweet_block .= $tweeter_status;
  $tweet_block .= '</div>';
  return $tweet_block ;
}

function a12_tweeter_block_configure($delta = '') {
  $form = array();
  if ($delta == 'latest_tweeter_status') {
    $form['tweeter_username'] = array(
      '#type' => 'textfield',
      '#title' => t('Tweeter account User name.'),
      '#default_value' => variable_get('tweeter_username','google'),
    );
    $form['tweet_cache'] = array(
      '#type' => 'textfield',
      '#title' => t('Number of Hours to store cache for tweeter items.'),
      '#default_value' => variable_get('tweet_cache', 2),
    );
  }
  return $form;
}

function a12_tweeter_block_save($delta = '', $edit = array()) {
  if ($delta == 'latest_tweeter_status') {
    variable_set('tweeter_username', $edit['tweeter_username']);
    variable_set('tweet_cache', $edit['tweet_cache']);
  }
}
