<?php

/**
 * @file
 */


/**
 * Menu callback: display an overview of available types.
 */
function product_catalog_products_overview() {
  global $user;  
  drupal_set_title('Product store');
  $breadcrumb_value = array('product_store' => 'Product store');
  product_catalog_products_set_breadcrumb($breadcrumb_value);
  $header = array(
    array('data' => t('Name'), 'class' => 'product_name'),
    array('data' => t('Description'), 'class' => 'product_desc'),
  );

  $rows = array();
  $products = commerce_product_types();  
  // Loop through all defined product types.
  foreach (commerce_product_types() as $type => $product_type) {
    //$result = db_query("SELECT * FROM {user_product_subscriptions} WHERE uid = :uid AND product_type = :product_type", array(':product_type' => $product_type['type'], ':uid' => $user->uid));    
    //$product_subscriptions = $result->rowCount();    
    //if($product_subscriptions == 0) {
      // Add the product type's row to the table's rows array.    
      $product_type['name'] = l($product_type['name'], 'product_store/'.$product_type['type']);
      $rows[] = array(array('data' => $product_type['name']), array('data' => check_plain($product_type['description'])));    
    //}
  }

  // If no product types are defined...
  if (empty($rows)) {
    // Add a standard empty row with a link to add a new product type.
    $rows[] = array(
      array(
        'data' => t('There are no product types yet.'),
        'colspan' => 2,
      )
    );
  }
  $table_attributes = array('id' => 'product_store_table');
  return theme('table', array('header' => $header, 'rows' => $rows, 'attributes' => $table_attributes));
}

