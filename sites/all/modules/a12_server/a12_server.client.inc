<?php
class a12_server_client {

  /**
   * Defined unique identifier for connecting to A12 webservices framework
   * @var mixed
   */
  protected $client_identifier;

  /**
   * Defined unique Hash key passed by the site when transaction is done
   * @var mixed
   */
  protected $client_key;

  /**
   * Defined status of the website within A12 network
   * @var array
   */
  protected $client_subscription_data;

  /**
   * Defined timestamp when the connector was prepared to call server
   * @var timestamp
   */
  protected $client_timestamp;

  /**
   * Defined url of the site that making the call
   * @var string
   */
  protected $client_site_url;

  /**
   * Defined name of the site thats  making the call
   * @var string
   */
  protected $client_site_name;

  /**
   * Defined IP address of the site thats making the call
   * @var string
   */
  protected $client_site_ip;

  /**
   * Defined status of the subscription
   * @var bool, 
   */
  public $is_active;

  /**
   * Token that is used to authenticate each request, without a valid token no requests are ever answered
   * @var bool,
   */
  public $a12_network_token;

  /**
   * Constructor
   * @param array $params
   */
  public function __construct($identifier, $timestamp, $hmac_hash, $site_url, $ip, $site_name ){
 
    $this->client_identifier = $identifier;
    $this->client_timestamp = $timestamp;
    $this->client_hash  = $hmac_hash;
    $this->client_site_url = $site_url;
    $this->client_site_name = $site_name;
    $this->client_site_ip = $ip;
    
  }
  
  /**
   * sets values of all variables, donot use directly. Use as part of creating a new a12_connector object
   * @param array $params
   */
  protected function set_vars(array $params = array()){
    foreach($params as $a12_var_id => $val)
      variable_set($a12_var_id, $val);
  }


  public function is_active_subscription() {
    $result = db_query("SELECT uid,user_system_token, token_id, key_name FROM {user_system_token} WHERE token_id=:id", array(":id" => $this->client_identifier))->fetch();

    if($result == null) {
      return array('status' => 'error', 'error_code' => 'FND-603');
    } else {
      $subscriptions = db_query("SELECT product_id, status FROM {a12_product_subscriptions_history} WHERE uid=:uid ORDER BY a12_product_subscriptions_history_id DESC LIMIT 1", array(":uid" => $result->uid))->fetchAll();
      foreach($subscriptions as $subscription) {
        if($subscription->status == 1) {
          return array("status" => "ok");
        }
      }
      return array("status" => "error", "error_code" => "FND-601");
    }
  }

  /**
   * Register a product with A12 network
   * @param a12product $module
   */
  function register(a12product $module) {

  }

  /**
   * Validated client credentials
   */
  public function authenticate() {
    $key =  (object) a12_server_get_client_key($this->client_identifier);
    $magic_key = $key->user_system_token;

    if(isset($key->status) && $key->status == 0) {
      return array("status" => "error", "message" => "FND-603");
    }

    $salt = substr(base_convert(sha1($this->client_site_url), 16, 36), 0, 6);
    $data = $this->client_timestamp . ':' . $salt . ':' . $magic_key;
    $hash = base64_encode(hash_hmac('sha1', $data, $magic_key, TRUE));
    watchdog('A12:Server', 'Authenticate method: ' . $hash . ' client hash: ' . $this->client_hash . ' client ident' . $this->client_identifier);
    return ($hash == $this->client_hash )? array("status" => "ok"):array("status" => "error", "error_code" => "FND-601");
  }

  public static function _generate_magic_key($client_name, $user_id) {

  }
}
