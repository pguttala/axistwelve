<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

interface a12_connect_plugin {

  abstract function info();

  /**
   * Any product that wishes to use a12 connect service needs to register itself
   */
  abstract function register();

  /**
   * Build a
   */
  abstract function _send_request();

  /**
   * Returns the type of product, e.g will return "search" for A12 search.
   */
  abstract function get_type();


  /**
   * Prepares endpoint by adding custom path to A12_network_address variable
   */
  abstract function get_endpoint();
  
}
