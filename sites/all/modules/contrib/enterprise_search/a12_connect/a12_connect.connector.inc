<?php
class a12_connector {

  /**
   * Defined unique identifier for connecting to A12 webservices framework
   * @var mixed
   */
  protected $a12_identifier;

  /**
   * Defined A12 Key for this website
   * @var mixed
   */
  protected $a12_key;

  /**
   * Defined address of A12 network host service
   * @var mixed
   */
  protected $a12_network_address;

  /**
   * Defined status of the website within A12 network
   * @var array
   */
  protected $a12_subscription_data;

  /**
   * Defined timestamp when the connector was prepared to call server
   * @var timestamp
   */
  protected $a12_timestamp;

  /**
   * Defined status if the current website has a valid subscription 
   * @var bool
   */
  public $has_subscription_info;

  /**
   * Defined status of the subscription
   * @var bool, 
   */
  public $is_active;

  /**
   * Constructor
   * @param array $params
   */
  public function __construct(array $params = array(), $action = FALSE){
    if (!empty($params) && $action == A12_SUBSCRIPTION_ACTION_SAVE){
      $this->set_vars($params);
    }

    $this->a12_identifier         = key_exists('a12_identifier', $params) ? $params['a12_identifier'] : variable_get('a12_identifier', FALSE);
    $this->a12_key                = key_exists('a12_key', $params) ? $params['a12_key'] : variable_get('a12_key', FALSE) ;
    $this->a12_network_address    = variable_get('a12_network_address', A12_VALIDATE_METHOD);
    $this->a12_subscription_data  = variable_get('a12_subscription_data', FALSE);
    $this->has_subscription_info = $this->has_subscription_credentials();
    $this->is_active = $this->is_active_subscription();
    $this->timestamp = REQUEST_TIME;
   }
  
  /**
   * sets values of all variables, donot use directly. Use as part of creating a new a12_connector object
   * @param array $params
   */
  protected function set_vars(array $params = array()){
    foreach($params as $a12_var_id => $val)
      variable_set($a12_var_id, $val);
  }

  
 /**
 * returns array of A12 connect variables that will allow sites to communicate with A12 webservices
 * @return mixed
 */
  private function connect_variables($params) {
    $this->a12_identifier         = key_exists('a12_identifier', $params) ? $params['a12_identifier'] : variable_get('a12_identifier', FALSE);
    $this->a12_key                = key_exists('a12_key', $params) ? $params['a12_key'] : variable_get('a12_key', FALSE) ;
    $this->a12_network_address    = variable_get('a12_network_address', 'https://services.axis12.com');
    $this->a12_subscription_data  = variable_get('a12_subscription_data', FALSE);
  }

  /**
   * Check if required key and identifier have been set
   * @return bool
   */
  public function has_subscription_credentials() {
    return (bool)(variable_get('a12_identifier', '') && variable_get('a12_key', ''));
  }


  public function is_active_subscription() {
    $active = FALSE;
    // Subscription cannot be active if we have no credentials.
    $active = (!empty($this->has_subscription_info) && !empty($this->a12_subscription_data['active']))? TRUE : FALSE;
    return $active;
  }


  /**
   * A12 Network settings form
   * @return array
   */
  public static function display_settings_form(){
    $form = array();
    $form['a12_label'] = array(
      '#markup'         => '<h3>'.t('Connection Details').'</h3>',
    );
    $form['a12_markup'] = array(
      '#markup'         => t('Your connection details can be found by logging into your account at <a href="http://www.axistwelve.com/user" target="_blank">www.axistwelve.com</a> and selecting \'Access Keys\' from the dashboard. If you don\'t yet have an account, click <a href="http://www.axistwelve.com/free-trial" target="_blank">here</a> to start a 30 day Free trial.'),
    );
    $form['a12_identifier'] = array(
      '#type'           => 'textfield',
      '#title'          => t('Access Key Identifier'),
      '#default_value'  => variable_get('a12_identifier', FALSE),
      '#description'    => t('The Identifier of the access key you wish to use to connect your site to the Find service.'),
    );
    $form['a12_key'] = array(
      '#type'           => 'textfield',
      '#title'          => t('Secret Key'),
      '#default_value'  => variable_get('a12_key', FALSE),
      '#description'    => t('The Secret Key of the access key you wish to use to connect your site to the Find service.'),
    );
    $form['buttons']['submit'] = array(
      '#type'     => 'submit',
      '#value'    => t('Save configuration'),
      '#validate' => array('a12_connect_validate_credentials'),
      '#submit'   => array('a12_connect_settings_form_submit'),
    );
    $form['buttons']['delete'] = array(
      '#type'   => 'submit',
      '#value'  => t('Delete subscription information'),
      '#submit' => array('a12_connect_delete_submit'),
    );
    return $form;
  }
  /*
   * Register a product with A12 network
   * @param a12product $module
   */
  function register(a12product $module) {

  }

  /**
   * Delete stored settings
   * @return
   */
  public static function delete_settings(){
    variable_del('a12_key');
    variable_del('a12_identifier');
    return drupal_set_message('Settings deleted');
  }

  /**
   * Performs XMLRPC call to A12services to identify if a given user identifier and key are valid
   */
  function validate_credentials() {
    global $base_url;
    $a12_network_address = A12_WEBSERVICES_URL . "/xmlrpc.php  ";
    $data = array(
      'auth_headers'  => $this->auth_headers(array('url' => $base_url)),
      'host'          => $host = isset($_SERVER["SERVER_ADDR"]) ? $_SERVER["SERVER_ADDR"] : '',
    );
    $result = a12_connect_send_xmlrpc_request($a12_network_address, A12_VALIDATE_METHOD, $data);
    return $result;
  }

  /**
   * Prepares headers that shall be used by A12 webservices to validate
   * incoming requests
   *
   * @param aray $params
   * Any additional paramters that are to be used in preparing headers
   *
   * @return array
   */
  public function auth_headers($params = array()) {
    global $base_url;
    $a12_headers = array();
    $a12_headers['timestamp'] = $this->timestamp;
    $a12_headers['identifier'] = $this->a12_identifier;
    $a12_headers['site_url'] = $base_url;
    $a12_headers['site_name'] = $_SERVER['SERVER_NAME'];
    $a12_headers['ip'] = $_SERVER['SERVER_ADDR'];
    // Although we can always send the key to XMLRPC request as
    // $a12_headers['key'] = $this->a12_key;
    // We avoid sending key over and instead send over Hash
    $a12_headers['hash'] = a12_connect_hmac_hash($this->timestamp, $params['url'], $this->a12_key);
    return $a12_headers;
  }

  public static function displayError($code) {
    switch($code) {
      case "FND-600":
        $message = "Connection error. Please contact your system administrator [FND-600]";
        break;
      case "FND-601":
        $message = "Invalid credentials. Please confirm your connection details and retry [FND-601]";
        break;
      case "FND-602":
        $message = "Subscription error. Please ensure your Subscription is active by logging into your account at <a href=\"http://www.axistwelve.com/user\">www.axistwelve.com</a> [FND-602]";
        break;
      case "FND-603":
        $message = "Access Key error. Please ensure your Access Key is enabled and has permissions to access your selected Index [FND-603]";
        break;
      case "FND-604":
        $message = "Index error. Please ensure you have selected a valid Index for your content. If this problem persists please contact a site administrator [FND-604]";
        break;
      case "FND-605":
        $message = "There was an issue connecting to the index. Please confirm your account details are correct and you have selected a valid index. If this problem persists please contact a site administrator [FND-605]";
        break;
      case "FND-620":
        $message = "Your Subscription is offline. You can bring your Subscription back online by logging into your account at <a href=\"http://www.axistwelve.com/user\">www.axistwelve.com</a> [FND-620]";
        break;
      case "FND-623":
        $message = "Your Subscription has been cancelled. Please contact your system administrator to renew your subscription [FND-623]";
        break;
      case "FND-625":
        $message = "Your Subscription is currently blocked. Please contact your system administrator [FND-625]";
        break;
      case "FND-626":
        $message = "Your Subscription has expired. You can upgrade your Subscription by logging into your account at <a href=\"http://www.axistwelve.com/user\">www.axistwelve.com</a> [FND-626]";
        break;
      default:
        $message = "Unknown error. Please contact your system administrator [" .$code ."]";
        break;
    }

    drupal_set_message($message, "error");
  }

}

/**
 * Subscription class responsible for all subscriptions and updates.
 */
class a12_subscription extends a12_connector {
  function  __construct(array $params = array()) {
    parent::__construct($params);
  }
}

