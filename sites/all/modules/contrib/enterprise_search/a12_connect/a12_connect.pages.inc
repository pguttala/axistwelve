<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


/**
 * This function sets up A12 connection settings against Axistwelve webservices.
 */
function a12_connect_settings() {
  /**
   * @Todo We want to do a few validations of existing data before we show the form so as to set appropriate
   * status messages when the form is shown for the first time
   *
   * $status_messages variable will have the appropriate status message.
   */
  return a12_connector::display_settings_form();
}


/**
 * Validate credentials of the subscription
 * @return <type>
 */
function a12_connect_validate_credentials($form, &$form_state) {
  foreach ($form_state['values'] as $key => $value) {
    $form_state['values'][$key] = trim($value);
  };

  $connector = new a12_connector($form_state['values']);
  $result = $connector->validate_credentials();
  $result = json_decode($result);
  if($result == null) {
    drupal_set_message("A12 Services appears to be down. Please try again later.", "error");
  }
  if ($result->status == "error") {
    form_set_error('a12_identifier', $result->message);
    form_set_error('a12_key', $result->message);
    drupal_get_messages('error');
    a12_connector::displayError($result->error_code);
  } else {
    return TRUE;
  }
}

/**
 * Deletes A12 subscription settings
 * @return Boolean TRUE
 */
function a12_connect_delete_submit() {
  a12_connector::delete_settings();
}

/**
 * A12 settings form
 * @param Mixed
 * Form elements
 *
 * @param Mixed
 *
 */
function a12_connect_settings_form_submit($form, $form_state) {
  variable_set('a12_key', $form_state['values']['a12_key']);
  variable_set('a12_identifier', $form_state['values']['a12_identifier']);
  drupal_set_message(t('Subscription details saved. You can now choose an index.'));
  //$status = a12_connect_validate_credentials();
  //@Todo, Do a check of user subscription status via XMLRPC
  cache_clear_all();
}



/**
 * returns a12 connctor
 * @staticvar boolean $a12_connector
 * @return a12_connector
 */

function a12_connect_get_connector($params = array()) {
  static $a12_connector = FALSE;

  if (!$a12_connector){
    $a12_connector = new a12_connector($params);
  }
  return $a12_connector;
}
