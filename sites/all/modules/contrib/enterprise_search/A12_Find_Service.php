<?php
/**
 * @file
 *   Extends the Drupal_Apache_Solr_Service to make it compatible with
 *   the A12 Find service.
 * @author Axis12 Ltd. <techincal@axistwelve.com>
 */
module_load_include('php', 'apachesolr', 'Drupal_Apache_Solr_Service');

class A12_Find_Service extends DrupalApacheSolrService {

  /**
   * Servlet mappings
   */
  const DELETE_SERVLET = 'delete';
  const DELETE_QUERY_SERVLET = 'delete/query';

  /**
   * Override the constructor
   * Append our index and core to the URL
   */
  public function __construct($url, $env_id = NULL) {
    parent::__construct($url, $env_id);
    $index = variable_get("enterprise_search_apachesolr_index", "default");
    $core = variable_get("enterprise_search_apachesolr_core", "DEV");


    $this->setUrl($this->getUrl() . '/'.$index.'/'.$core);
  }

  /**
   * Override _makeHttpRequest
   * Attach authentication headers and check the response for errors
   */
  protected function _makeHttpRequest($url, $options = array()) {
    $options['headers'] = enterprise_search_auth_headers($url);
    $options['User-Agent'] = 'a12_find/'. A12_FIND_VERSION;
    $response = parent::_makeHttpRequest($url, $options);
    if(isset($response->status) && $response->status == "error") {
      $result = json_decode($response->data);
      a12_connector::displayError($result->error_code);
      if(user_access('administer search')) {
        drupal_goto('admin/config/search/apachesolr/subscription');
      } else {
        $search_page = apachesolr_search_page_load(apachesolr_search_default_search_page());
        if($_GET['q'] != $search_page->search_path) {
          drupal_goto($search_page->search_path);
        }
      }
    }
    return $response;
  }
  /**
   * Override the of the addDocument 
   * @param type $documents
   * @param type $overwrite
   * @param type $commitWithin 
   * @todo Extend to include all schema type and cast as appropriate
   */
  public function addDocuments($documents, $overwrite = NULL, $commitWithin = NULL) {
    $namespace = "druA12";

    $records = array();
    foreach($documents as $document) {
      $field = array();
      $xml = simplexml_load_string(ApacheSolrDocument::documentToXml($document));
      foreach($xml->children() as $element) {
        $attributes = $element->attributes();
        $name = $attributes['name'];
        $key = $namespace . '$' . (string)$name;
        $value = (string)$element;
        $solr_type = explode('_', $name);
        switch($solr_type[0]) {
          case "bs":
            $field[$key] = (bool)$value;
            break;
          case 'bm':
            $value = (bool) $value;
          case 'access':
          case 'dm':
          case 'ddm':
          case 'fm':
          case 'ftm':
          case 'fsm':
          case 'ghm':
          case 'hsm':
          case 'htm':
          case 'im':
          case 'ism':
          case 'itm':
          case 'pm':
          case "ptm":
          case "psm":            
          case 'sm':
          case 'taxonomy':
          case 'tid':
          case 'tem':
          case 'tom':
          case 'tm':
          case 'tum':
          case 'twm':
          case 'xm':
            if(is_numeric($value) && $key != 'druA12$comment_count') {
              $field[$key][] = intval($value);
            }
            else {
              $field[$key][] = $value;
            }
            break;
          default:
            if(is_numeric($value) && $key != 'druA12$comment_count') {
              $field[$key] = intval($value);
            }
            else {
              $field[$key] = $value;
            }
        }

        if($name == 'entity_id') {
          $field[$key] = (string)$element;
          $entity_id = intval((string)$element);
        }
      }

      $record = array();
      $record['fields'] = (object) $field;

      if(isset($entity_id)) {
        $doc_data = new stdClass();
        $doc_data->nid = $document->entity_id;
        $doc_data->language = $document->language;
        $record['id'] = enterprise_search_generate_document_id($doc_data);
      }
      $records[] = $record;

    }

    $rawPost = array();
    $rawPost['namespaces'] = array(
      'com.axistwelve.drupalschema' => $namespace,
    );
    $rawPost['records'] = $records;
    $this->update(json_encode($rawPost));
  }

  /**
   * Override setStatus
   * Send and receive data through JSON
   */
  protected function setStats() {
    $data = $this->getLuke();
    // Only try to get stats if we have connected to the index.
    if (empty($this->stats) && isset($data->index->numDocs)) {
      $url = $this->_constructUrl(self::STATS_SERVLET);
      if ($this->env_id) {
        $this->stats_cid = $this->env_id . ":stats:" . drupal_hash_base64($url);
        $cache = cache_get($this->stats_cid, 'cache_apachesolr');
        if (isset($cache->data)) {
          $this->stats = json_decode($cache->data);
        }
      }
      // Second pass to populate the cache if necessary.
      if (empty($this->stats)) {
        $response = $this->_sendRawGet($url);
        $this->stats = json_decode($response->data);
        if ($this->env_id) {
          cache_set($this->stats_cid, $response->data, 'cache_apachesolr');
        }
      }
    }
  }

  public function getStatsSummary() {
    $stats = $this->getStats();
    return (array) $stats;
  }

  /**
   * Raw update Method. Takes a raw post body and sends it to the update service. Post body
   * should be a complete and well formed xml document.
   *
   * @param string $rawPut
   * @param float $timeout Maximum expected duration (in seconds)
   *
   * @return response object
   *
   * @throws Exception If an error occurs during the service call
   */
  public function update($rawPut, $timeout = FALSE) {
    // @todo: throw exception if updates are disabled.
    if (empty($this->update_url)) {
      // Store the URL in an instance variable since many updates may be sent
      // via a single instance of this class.
      $this->update_url = $this->_constructUrl(self::UPDATE_SERVLET, array('wt' => 'json'));
    }
    $options['method'] = 'PUT';
    $options['data'] = $rawPut;
    if ($timeout) {
      $options['timeout'] = $timeout;
    }
    return $this->_makeHttpRequest($this->update_url, $options);
  }


  /**
   * Create a delete document based on document ID
   *
   * @param string $id Expected to be utf-8 encoded
   * @param float $timeout Maximum expected duration of the delete operation on the server (otherwise, will throw a communication exception)
   *
   * @return response object
   *
   * @throws Exception If an error occurs during the service call
   */
  public function deleteById($id, $timeout = 3600) {
    return $this->deleteByMultipleIds(array($id), $timeout);
  }

  /**
   * Create and post a delete document based on multiple document IDs.
   *
   * @param array $ids Expected to be utf-8 encoded strings
   * @param float $timeout Maximum expected duration of the delete operation on the server (otherwise, will throw a communication exception)
   *
   * @return response object
   *
   * @throws Exception If an error occurs during the service call
   */
  public function deleteByMultipleIds($ids, $timeout = 3600) {
    $record_ids = array();
    foreach($ids as $id) {
      $node = new stdClass();
      $node->id = $id;
      $record_ids[] = enterprise_search_generate_document_id($node);
    }
    $options['method'] = 'DELETE';
    $options['data'] = json_encode($record_ids);
    if ($timeout) {
      $options['timeout'] = $timeout;
    }
    $url = $this->_constructUrl(self::DELETE_SERVLET);
    return $this->_makeHttpRequest($url, $options);
  }

  /**
   * Create a delete document based on a query and submit it
   *
   * @param string $rawQuery Expected to be utf-8 encoded
   * @param float $timeout Maximum expected duration of the delete operation on the server (otherwise, will throw a communication exception)
   * @return stdClass response object
   *
   * @throws Exception If an error occurs during the service call
   */
  public function deleteByQuery($rawQuery, $timeout = 3600) {
    $query_parts = explode(' ', $rawQuery);
    foreach ($query_parts as $pos => $part) {
      if (strstr($part, 'sm_parent_document_id:')) {
        $nd = new stdClass();
        $nd->id = str_replace('sm_parent_document_id:', '', $part);
        $query_parts[$pos] = sprintf('%s%s', 'sm_parent_document_id:', enterprise_search_generate_document_id($nd));
      }
      elseif (strstr($part, 'id:')) {
        $nd = new stdClass();
        $nd->id = str_replace('id:', '', $part);
        $query_parts[$pos] = sprintf('%s%s', 'id:', enterprise_search_generate_document_id($nd));
      }
    }
    $rawQuery = implode(' ', $query_parts);
    $options['method'] = 'DELETE';
    $options['data'] = json_encode($rawQuery);
    if ($timeout) {
      $options['timeout'] = $timeout;
    }
    $url = $this->_constructUrl(self::DELETE_QUERY_SERVLET);
    return $this->_makeHttpRequest($url, $options);
  }

  /**
   * Override of Simple Search interface
   *
   * @param string $query The raw query string
   * @param array $params key / value pairs for other query parameters (see Solr documentation), use arrays for parameter keys used more than once (e.g. facet.field)
   *
   * @return response object
   *
   * @throws Exception If an error occurs during the service call
   */
  public function search($query = '', $params = array(), $method = 'GET') {
    if (isset($params['qt']) && $params['qt'] == 'mlt') {
      $node = new stdClass();
      $node->id = str_replace('id:', '', $params['q']);
      $params['q'] = sprintf('id:%s', enterprise_search_generate_document_id($node));
    }
    if (!is_array($params)) {
      $params = array();
    }
    // Always use JSON. See http://code.google.com/p/solr-php-client/issues/detail?id=6#c1 for reasoning
    $params['wt'] = 'json';
    // Additional default params.
    $params += array(
      'json.nl' => self::NAMED_LIST_FORMAT,
    );
    if (!empty($query) && @$params['qt'] != 'mlt') {
      // Swapping this back as there is no good reason for it
      $query = str_replace('&#039;', "'", $query);
      $params['q'] = $query;
    }
    // PHP's built in http_build_query() doesn't give us the format Solr wants.
    $queryString = $this->httpBuildQuery($params);
    // Check string length of the query string, change method to POST
    // if longer than 4000 characters (typical server handles 4096 max).
    // @todo - make this a per-server setting.
    if (strlen($queryString) > variable_get('apachesolr_search_post_threshold', 4000)) {
      $method = 'POST';
    }

    if ($method == 'GET') {
      $searchUrl = $this->_constructUrl(self::SEARCH_SERVLET, array(), $queryString);
      return $this->_sendRawGet($searchUrl);
    }
    else if ($method == 'POST') {
      $searchUrl = $this->_constructUrl(self::SEARCH_SERVLET);
      $options['data'] = $queryString;
      $options['headers']['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';
      return $this->_sendRawPost($searchUrl, $options);
    }
    else {
      throw new Exception("Unsupported method '$method' for search(), use GET or POST");
    }
  }
}
