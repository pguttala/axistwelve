<?php
 
/**
 * @file
 *   Enterprise search with a cloud based Solr service.
 * @author Axis12 Ltd <technical@axitwelve.com>
 * 
 * @todo - Check that security is being added to all requests to the service!!!!
 *       - Remember to add the option to support scheme in a12_connect
 */

define('A12_FIND_VERSION', "6.x-1.0001");
define('A12_FIND_DRUPAL_NS', 'druA12');
define('A12_FIND_SERVER_NS', 'A12:Find');
define('A12_FIND_SERVER', 'find.axis12.com');

define('A12_TMP_DEFAULT_SCHEME', 'https://');


/**
 * Implements hook_menu()
 */
function enterprise_search_menu() {
  $items = array();
  $items['admin/config/search/apachesolr/indicies'] = array(
    'title' => '2. Choose Index',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('enterprise_search_index_form'),
    'access arguments' => array('administer search'),
    'type' => MENU_LOCAL_TASK,
    'weight' => 2,
  );
  return $items;
}

/**
 * Implementation of hook_menu_alter().
 * Reorganise the Apache Solr search modules menu items to make room for
 * Enterprise Search menu items.
 */
function enterprise_search_menu_alter(&$menu) {
  if (isset($menu['admin/config/search/apachesolr'])) {
    $menu['admin/config/search/apachesolr']['title'] = 'Enterprise Search';
    $menu['admin/config/search/apachesolr']['description'] = 'Configure enterprise search settings, indexing options and blocks, and manage search environments.';
  }
  if (isset($menu['admin/config/search/apachesolr/settings'])) {
    $menu['admin/config/search/apachesolr/settings']['title'] = '5. Advanced Settings';
    $menu['admin/config/search/apachesolr/settings']['weight'] = 10;
  }
  if (isset($menu['admin/config/search/apachesolr/index'])) {
    $menu['admin/config/search/apachesolr/index']['title'] = '3. Manage Index';
    $menu['admin/config/search/apachesolr/index']['weight'] = 3;
    $menu['admin/config/search/apachesolr/index']['type'] = MENU_LOCAL_TASK;
  }

  if (isset($menu['admin/config/search/apachesolr/search-pages'])) {
    $menu['admin/config/search/apachesolr/search-pages']['title'] = '4. Manage Pages/Blocks';
    $menu['admin/config/search/apachesolr/search-pages']['weight'] = 4;
  }

  if (isset($menu['admin/reports/apachesolr/conf'])) {
    $menu['admin/reports/apachesolr/conf']['page callback'] = 'enterprise_search_solr_conf_page';
  }
}

/**
 * Display an error message if the configuration file page is accessed
 */
function enterprise_search_solr_conf_page() {
  return t("The solr server is provided by Axis12 Find so the configuration files are not currently accessible.");
}

/**
 * Use indexed URL
 */
function enterprise_search_preprocess_search_result(&$variables) {
  $variables['url']  = $variables['result']['fields']['url'];
}

/**
 * Implements hook_form_alter
 */
function enterprise_search_form_alter(&$form, &$form_state, $form_id) {

  switch ($form_id) {
    //If stats can not be retreived then the index can not be contacted
    case "apachesolr_index_action_form":
      $solr = (apachesolr_get_solr());
      if(($solr->getStats() == "")) {
        drupal_set_message("Your index could not be connected to. Please check you have selected an index.", "error");
        drupal_goto("admin/config/search/apachesolr/indicies");
      }
      // ref: FND-257: Remove the 'Delete Index' button and label
      // remove button 'Delete the Search & Solr index' & its description.
      unset($form['action']['delete']);
      unset($form['action']['delete_description']);
    break;

    case "apachesolr_index_config_form":
      global $base_url;

      $form['config']['base_url_title'] = array(
        '#type' => 'markup',
        '#markup' => 'Set the URL to be indexed with your content. If you are indexing content from a private facing site (e.g. http://authoring.abc.com) you will want to populate this field with your public facing URL (e.g. http://abc.com).',
        '#weight' => -2,
      );

      $form['config']['base_url'] = array(
        '#type' => 'textfield',
        '#title' => 'Base URL',
        '#default_value' => variable_get('enterprise_search_base_url', $base_url),
        '#weight' => -1,
      );
      
      $form['config']['enterprise_search_index_attachments'] = array(
        '#type' => 'checkbox',
        '#title' => 'Index Attachments',
        '#default_value' => variable_get('enterprise_search_index_attachments', 0),
      );
      
      $form['#submit'][] = 'base_url_config_form_submit';
      $form['#validate'][] = 'base_url_config_form_validate';
    break;

 }
}

/**
 * Validate the base URL
 */
function base_url_config_form_validate($form, $form_state) {
  $url = $form_state['values']['base_url'];
  $url_check_pattern = "@(http|https)://([a-zA-Z0-9.]|%[0-9A-Za-z]|/|:[0-9]?)*@";
  if(!preg_match($url_check_pattern, $url)) {
    form_set_error('base_url', 'The base URL must be fully qualified');
    return FALSE;
  }
  return TRUE;
}

/**
 * Save the base URL
 */
function base_url_config_form_submit($form, $form_state) {
  $url = $form_state['values']['base_url'];
  $last = substr($url, -1);
  if($last == "/") {
    $url = substr($url, 0, -1);
  }
  variable_set('enterprise_search_base_url', $url);
}

/**
 * enterprise_search_index_form submit handler
 * Saves the current index and core the user is using. 
 */
function enterprise_search_index_form_submit($form, &$form_state) {
  variable_set('enterprise_search_apachesolr_index', strtolower($form_state['values']['name']));
  variable_set('enterprise_search_apachesolr_core', $form_state['values']['cores']);
  $solr = (apachesolr_get_solr());
  if(($solr->getStats() == "")) {
    a12_connector::displayError('FND-605');
    form_set_error('name','');
  } else {
    drupal_set_message("Index selected.");
  }

}

function enterprise_search_index_form_validate($form, &$form_state) {
  if(empty($form_state['values']['name'])) {
    form_set_error('name','Please select an index');
  }
  if(empty($form_state['values']['cores'])) {
    form_set_error('cores','Please select an environment');
  }
}

/**
 * Index form
 * Lets the user choose an index and core to use
 */
function enterprise_search_index_form() {
  $indicies = enterprise_search_check_indicies();
  if(!$indicies) {
    return array( "message" => 
      array(
        "#type" => "markup",
        "#markup" => "No indicies were found. This could be due to one of the following reasons:<br /><br /><ul><li>Your subscription has expired or is currently set to 'offline'</li><li>The Access Key you are using is currently 'disabled' or has been deleted</li><li> the Access Key you are using does not have permissions to see any of your indices</li><li>You are not using the most recent version of the <a href='http://www.http://drupal.org/project/apachesolr'>ApacheSolr</a> module</li></ul><br />Please review your settings through your account at <a href='http://www.axistwelve.com/user'>www.axistwelve.com</a> and try again.",
      ),
    );
  }
  $options = array();

  $form = array();
  $form['a12_label'] = array(    	
    '#markup' => '<h3>Search Server</h3>' .t('Create and manage your index(s) through your account at ').l(t('www.axistwelve.com'),'http://www.axistwelve.com/user', array('attributes' => array('target' =>'_blank'))).t('. If you don\'t see an index you\'re expecting to, it may be that the Access Key you are using does not have permission to view it. You can amend this through your account.<br /><br />If you are using \'Enterprise Search\' on more than one website, remember that you can either create a separate index for each site, or configure all sites to use a single common index for multi-site searching.'),
  );

  $options[0] = 'None';
  foreach ($indicies as $environment_id => $data) {
    $options[strtolower($data['name'])] = $data['label'] . " - " . $data['description'];
  }

  $default = variable_get('enterprise_search_apachesolr_index',0);
  if(!isset($options[$default])) {
    $default = 0;
  }

  $form['name'] = array(
    '#type' => 'radios',
    '#title' => "Index",
    '#options' => $options,
    '#default_value' => $default,
    '#required' => TRUE,
  );

  $rows = array();

  // Environment settings
  $id = apachesolr_default_environment();
  $environments = apachesolr_load_all_environments();
  $default_environment = apachesolr_default_environment();

  // Reserve a row for the default one
  $rows[$default_environment] = array();

  $default_value = variable_get("enterprise_search_apachesolr_core","0");
  if($default == 0) $default_value = "0"; // if index unidentified then set core to none (i.e. override current core variable as it irelates to a previous index)
  foreach($indicies as $index) {
    $environments[$index['index_id']]['description'] = $index['description'];
    $form['cores'] = array(
      '#type' => 'radios',
      '#title' => 'Environment',    
      '#options' => array("0"=>"None","DEV"=>"Development", "TEST"=>"Test", "LIVE"=>"Production"),
      '#default_value' =>  $default_value,
      '#required' => TRUE,
    );
  }

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Set search server',
  );

  return $form;
}

/**
 * Reset solr settings to defaults
 */
function enterprise_search_refresh_solr_settings() {
  $environments = apachesolr_load_all_environments();
  foreach($environments as $environment) {
    if($environment['env_id'] == "find") {
      apachesolr_environment_delete($environment['env_id']);
    }
  }

  $values = array(
    'env_id' => 'find',
    'name' => 'Enterprise Search',
    'url' =>  A12_TMP_DEFAULT_SCHEME.A12_FIND_SERVER."/",
    'service_class' => 'A12_Find_Service',
  );

  apachesolr_environment_save($values);
  variable_set('apachesolr_default_environment', 'find');
}

/**
 * This method will return all of the available indicies for the clients key
 */
function enterprise_search_check_indicies() {
  $a12_servs = array();
  $solr = new DrupalApacheSolrService(A12_TMP_DEFAULT_SCHEME . A12_FIND_SERVER);
  $options['headers'] = enterprise_search_auth_headers(A12_TMP_DEFAULT_SCHEME . A12_FIND_SERVER . '/admin/servers?wt=json&json.nl=map');
  $options['User-Agent'] = 'a12_find/'. A12_FIND_VERSION;
  $resp = $solr->makeServletRequest('admin/servers', array(), $options);
  $data = json_decode($resp->data, true);
  if(isset($data['indexes'])) {
    return $data['indexes'];
  } 
  else {
    a12_connector::displayError($resp->error_code);
    return null;
  }
}

/**
 * Implements hook_apachesolr_index_document_build_ENTITY_TYPE
 */
function enterprise_search_apachesolr_index_document_build_node(ApacheSolrDocument $document, $entity, $env_id) {
  global $base_url;
  $document->id = enterprise_search_generate_document_id($entity);
  $document->url = variable_get('enterprise_search_base_url', $base_url) . "/" . $document->path;
}

/**
 * Modify a solr base url and construct a hmac authenticator headers.
 *
 * @param $url
 *  The solr url beng requested - passed by reference and may be altered.
 * @param $string
 *  A string - the data to be authenticated, or empty to just use the path
 *  and query from the url to build the authenticator.
 * @param $derived_key
 *  Optional string to supply the derived key.
 * @param $schem
 *  Scheme to be used during the request
 *
 * @return
 *  An array containing the string to be added as the content of the
 *  Cookie header to the request and the nonce.
 *
 */
function enterprise_search_auth_headers($url, $string = '', $scheme = NULL) {
  $uri = parse_url($url);
  if(is_null($scheme)) {
    // If no scheme is defined, default to ssl.
    if (in_array('ssl', stream_get_transports(), TRUE)) {
      $scheme = 'https://';
      $port = '';
    }
    else {
      $scheme = 'http://';
      $port = (isset($uri['port']) && $uri['port'] != 80) ? ':'. $uri['port'] : '';
    }
  }
  else {
    $port = ':' . $uri['port'];
  }

  $path = isset($uri['path']) ? $uri['path'] : '/';
  $query = isset($uri['query']) ? '?'. $uri['query'] : '';
  //$url = $scheme . $uri['host'] . $port . $path . $query;
  $params = array(
    'url' => $url,
    'timestamp' => time(),
  );
  $a12_conn = new a12_connector();
  $auth_header = $a12_conn->auth_headers($params);
  return $auth_header;
}

/**
 * Generate a unique document id for the find back end
 */
function enterprise_search_generate_document_id($node) {
  $vars = array();
  $name = variable_get('enterprise_search_apachesolr_index', '');
  $core = variable_get('enterprise_search_apachesolr_core', '');
  !empty($node->language)?$vars[]='language='.$node->language:$vars[]='language=und';
  !empty($name)?$vars[]="name=$name":$vars[]='name=wonderfulindex';
  !empty($core)?$vars[]="type=$core":$vars[]='type=DEV';
  

  if (isset($node->id)) {
    return sprintf('%s.%s', str_ireplace('/', '-', $node->id), implode(',', $vars));
  }

  return sprintf('%s.%s', str_ireplace('/', '-', apachesolr_document_id($node->nid)), implode(',', $vars));
}
